Running
=======

The repository contain sample working directories for all current core types.
For copyright reasons we aren't allowed to provide all files needed to run the game server.

Additional files
----------------

The game server need the following files:

* atlasinfo.txt - use the one from the client you are using (locale/[lang]/atlasinfo.txt)
* item_proto - use the item_proto from your client (locale/[lang]/item_proto)
* data/pc & data/pc2 - copy the folders d:/ymir work/pc and d:/ymir work/pc2, remove the common folder in
  both folders, you can also remove all *.gr2 and *.dds files

Database
--------

Currently we don't have any automatic migration system in place (even it is planned).
In the main repo you can find a dump of the current state of the database.
The database contains some test users, all passwords are `test123`.

Cache server
------------

QuantumCore utilize Redis as a cache server for fast changing and temporary data.
We recommend to use a current version of redis.

**Windows:**
There is no official redis built for windows. We recommend using Windows Subsystem for Linux,
or run redis as a Docker container. 
There is an inofficial build by microsoft which seems to work fine, but it is not officially supported.
https://github.com/microsoftarchive/redis/releases

Signals
-------

QuantumCore handles the signals `SIGINT` and `SIGTERM` to initiate a graceful shutdown of the core.
(So you can shutdown the core with `CTRL+C`, or by sending the signals with system commands)