Building
========

This project is tested using vcpkg as a library manager. Only 64bit
builds are tested.

The cmake project contains a special target called `compile_packets`, this target
will generate the source files for all packets defined, it has to been run before
compiling the core. It might be necessary to reload the cmake project afterwards.

VCPKG
-----

The following packages are required:

* boost-convert
* boost-log
* boost-system
* boost-filesystem
* boost-program-options
* libmariadb
* lzo
* cryptopp

The following packages are optional:

* angelscript

We always building and testing against the newest available versions of
the libraries.

It is **not** recommended to use old outdated libraries from your
operating system package manager.

CMake
-----

The project uses cmake for build configuration. This allow you to
generate Makefiles and project files for many platforms and IDEs.

CMake options
-------------

To customize which features and how the core is compiled we have provided some cmake options:

+--------------+---------------------------------------------------------------------------+---------------+--------------------------+
| CMake option | Description                                                               | Default Value | Value in official builds |
+==============+===========================================================================+===============+==========================+
| BUILD_TESTS  | Should the test project be generated?                                     | OFF           | ON                       |
+--------------+---------------------------------------------------------------------------+---------------+--------------------------+
| SPLIT_CORES  | If this option is enabled each core type will generate an own executable. | OFF           | OFF                      |
+--------------+---------------------------------------------------------------------------+---------------+--------------------------+
| PROFILING    | If this option is enabled the core will write profiling information.      | OFF           | OFF                      |
+--------------+---------------------------------------------------------------------------+---------------+--------------------------+

Tested platforms
----------------

The project is regular tested against the following operating systems
and compilers:

* Windows - MSVC 14.24 (Visual Studio 2019 Version 16.4)
* Debian - Clang 11
* macOS - Apple Clang 10