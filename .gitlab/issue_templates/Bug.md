By reporting this issue, you acknowledge that you have tested your issue
with the latest commit of Quantum Core in the master branch and the issue
still persists.

**NOTE:** We do not cover issues by third party plugins or modified
clients. (ie: clients with Lycan, Sash, ...)


**Platform where the server is hosted:** 


**Brief description of the issue:**


**Steps to reproduce the issue:**


**Server logs: (make sure to cover any private info):**


**Screenshots: (if possible)**


**Additional information:**

