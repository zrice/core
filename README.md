# QuantumCore
[![pipeline status](https://gitlab.com/m2reloaded/core/badges/master/pipeline.svg)](https://gitlab.com/m2reloaded/core/commits/master)
[![coverage report](https://gitlab.com/m2reloaded/core/badges/master/coverage.svg)](https://gitlab.com/m2reloaded/core/commits/master)

**QuantumCore** is a reinvented open source implementation of the Metin2 server.

## Maintainers
This project is currently maintained by:
- NoFr1ends
- Arves100

## Community and contributing
To get started contributing to the project, see the [contributing guide](CONTRIBUTING.md).

To get in touch with the developers, the best way is to join our [Discord server](https://discord.gg/6VhbYxX).

## Documentation
The official documentation is available [here](https://docs.quantum-core.io). It is maintained inside this repository 
inside the [docs/](docs) folder.