-- MariaDB dump 10.17  Distrib 10.4.11-MariaDB, for osx10.15 (x86_64)
--
-- Host: 127.0.0.1    Database: account
-- ------------------------------------------------------
-- Server version	10.4.11-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `account`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `account` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `account`;

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` tinytext DEFAULT NULL,
  `username` varchar(40) NOT NULL,
  `password` varchar(60) NOT NULL,
  `email` varchar(100) NOT NULL,
  `registration_date` datetime NOT NULL DEFAULT current_timestamp(),
  `last_modified` datetime NOT NULL DEFAULT current_timestamp(),
  `empire` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts`
--

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
INSERT INTO `accounts` VALUES (1,'OK','test','$2y$12$aUxFstNjhTzlEWX28G8xTu1.3.by5RzJc1RLiqN86H6Q4qHBflsDG','test@test.tld','2020-02-08 17:58:19','2020-02-08 17:58:23',1),(2,'OK','test2','$2y$12$aUxFstNjhTzlEWX28G8xTu1.3.by5RzJc1RLiqN86H6Q4qHBflsDG','test@test.tld','2020-03-31 01:16:28','2020-03-31 01:16:28',2);
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('M20181013_234200_CreateAccounts'),('M20181027_214100_CreatePlayers'),('M20181027_224500_CreatePlayerData'),('M20181027_225700_CreatePlayerEquipment'),('M20181027_225900_CreatePlayerSkills'),('M20181027_230100_CreatePlayerQuickSlots');
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `game`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `game` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `game`;

--
-- Table structure for table `item_bonuses`
--

DROP TABLE IF EXISTS `item_bonuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_bonuses` (
  `item_id` bigint(20) DEFAULT NULL,
  `position` tinyint(4) DEFAULT NULL,
  `bonus_id` smallint(6) DEFAULT NULL,
  `bonus_value` int(11) DEFAULT NULL,
  KEY `item_bonuses_items_id_fk` (`item_id`),
  CONSTRAINT `item_bonuses_items_id_fk` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_bonuses`
--

LOCK TABLES `item_bonuses` WRITE;
/*!40000 ALTER TABLE `item_bonuses` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_bonuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_sockets`
--

DROP TABLE IF EXISTS `item_sockets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_sockets` (
  `item_id` bigint(20) DEFAULT NULL,
  `position` tinyint(4) DEFAULT NULL,
  `socket` int(11) DEFAULT NULL,
  KEY `item_sockets_items_id_fk` (`item_id`),
  CONSTRAINT `item_sockets_items_id_fk` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_sockets`
--

LOCK TABLES `item_sockets` WRITE;
/*!40000 ALTER TABLE `item_sockets` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_sockets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items` (
  `id` bigint(20) NOT NULL,
  `player_id` int(11) DEFAULT NULL,
  `vnum` int(11) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `window` tinyint(2) DEFAULT NULL,
  `position` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (1,1,19,1,1,0),(2,1,299,1,2,4),(3,1,11469,1,2,0),(4,1,11459,1,1,4);
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `player_data`
--

DROP TABLE IF EXISTS `player_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `player_data` (
  `player_id` int(11) NOT NULL,
  `x` int(8) NOT NULL,
  `y` int(8) NOT NULL,
  `hp` int(6) NOT NULL,
  `mp` int(6) NOT NULL,
  `stamina` smallint(4) NOT NULL,
  `st` smallint(3) NOT NULL,
  `ht` smallint(3) NOT NULL,
  `dx` smallint(3) NOT NULL,
  `iq` smallint(3) NOT NULL,
  `level` smallint(3) NOT NULL,
  `exp` smallint(3) NOT NULL DEFAULT 0,
  `gold` bigint(20) NOT NULL,
  `rank_points` int(6) NOT NULL DEFAULT 0,
  `playtime` int(6) NOT NULL DEFAULT 0,
  `last_play` datetime NOT NULL,
  `stat_point` smallint(4) NOT NULL DEFAULT 0,
  `skill_point` smallint(3) NOT NULL DEFAULT 0,
  PRIMARY KEY (`player_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `player_data`
--

LOCK TABLES `player_data` WRITE;
/*!40000 ALTER TABLE `player_data` DISABLE KEYS */;
INSERT INTO `player_data` VALUES (1,332414,756489,100,100,100,90,90,90,90,99,0,1000000,20000,100,'2020-02-15 01:58:42',0,0),(2,332414,756489,100,100,100,90,90,90,90,99,0,1000000,20000,100,'2020-02-15 01:58:42',0,0),(5,332414,756489,100,100,100,99,99,99,99,99,0,300000,0,0,'2020-03-31 02:28:25',0,0);
/*!40000 ALTER TABLE `player_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `player_equipment`
--

DROP TABLE IF EXISTS `player_equipment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `player_equipment` (
  `player_id` int(11) NOT NULL,
  `position` smallint(3) NOT NULL,
  `item_id` int(11) NOT NULL,
  PRIMARY KEY (`player_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `player_equipment`
--

LOCK TABLES `player_equipment` WRITE;
/*!40000 ALTER TABLE `player_equipment` DISABLE KEYS */;
/*!40000 ALTER TABLE `player_equipment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `player_quick_slot`
--

DROP TABLE IF EXISTS `player_quick_slot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `player_quick_slot` (
  `player_id` int(11) NOT NULL,
  `slot_id` smallint(4) NOT NULL,
  `type` tinyint(3) NOT NULL,
  `position` smallint(4) NOT NULL,
  PRIMARY KEY (`player_id`,`slot_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `player_quick_slot`
--

LOCK TABLES `player_quick_slot` WRITE;
/*!40000 ALTER TABLE `player_quick_slot` DISABLE KEYS */;
/*!40000 ALTER TABLE `player_quick_slot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `player_skills`
--

DROP TABLE IF EXISTS `player_skills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `player_skills` (
  `player_id` int(11) NOT NULL,
  `skill_id` smallint(3) NOT NULL,
  `level` tinyint(2) NOT NULL,
  PRIMARY KEY (`player_id`,`skill_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `player_skills`
--

LOCK TABLES `player_skills` WRITE;
/*!40000 ALTER TABLE `player_skills` DISABLE KEYS */;
/*!40000 ALTER TABLE `player_skills` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `players`
--

DROP TABLE IF EXISTS `players`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `players` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `name` varchar(24) NOT NULL,
  `class` tinyint(2) NOT NULL,
  `skill_group` tinyint(2) NOT NULL DEFAULT 0,
  `appearance` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `players`
--

LOCK TABLES `players` WRITE;
/*!40000 ALTER TABLE `players` DISABLE KEYS */;
INSERT INTO `players` VALUES (1,1,'NoFr1ends',1,0,0),(2,1,'NoFr1ends2',3,0,0),(5,2,'ted53.',8,0,0);
/*!40000 ALTER TABLE `players` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-22 19:37:01
