#include <iostream>
#include <memory>

#include "../core/application.hpp"
#include "application.hpp"

#ifdef SPLIT_CORES
int main() {
    auto app = std::make_shared<core::Application>();
    auto gameApp = std::make_shared<game::Application>(app);
    app->Start(gameApp);

#ifdef _DEBUG
    // Wait for key to exit process when debug executable is compiled
    std::cout << "Press any key to exit the application." << std::endl;
    getchar();
#endif

    return 0;
}
#endif