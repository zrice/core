#include "animation_manager.hpp"

#include <boost/filesystem.hpp>

#include "../../core/logger.hpp"
#include "../../core/profiler.hpp"
#include "structured_file_reader.hpp"

namespace fs = boost::filesystem;

namespace game::formats {
    AnimationManager::AnimationManager() : _animations() {}

    AnimationManager::~AnimationManager() {
        for (auto entry : _animations) {
            for (auto animation : entry.second) {
                delete animation.second;
            }
        }
    }

    void AnimationManager::Load() {
        PROFILE_FUNCTION();
        fs::path data{"data/"};

        for (const auto &directory : fs::directory_iterator(data)) {
            auto path = directory.path();
            if (!fs::is_directory(path)) continue;

            // Depending on which folder we are processing we have to
            // "sort" the animations differently
            // pc and pc2 are player animations
            if (path.filename() == "pc") {
                LoadPlayerAnimations(path, false);
            }
            if (path.filename() == "pc2") {
                LoadPlayerAnimations(path, true);
            }
        }
    }

    void AnimationManager::LoadPlayerAnimations(const fs::path &dir,
                                                bool isPC2) {
        for (const auto &directory : fs::directory_iterator(dir)) {
            auto path = directory.path();
            if (!fs::is_directory(path)) continue;

            // Convert folder name to id
            uint32_t id = 0;
            auto dirname = path.filename();
            if (dirname == "warrior") {
                id = 0;
            } else if (dirname == "assassin") {
                id = 1;
            } else if (dirname == "sura") {
                id = 2;
            } else if (dirname == "shaman") {
                id = 3;
            }
            if (isPC2) {
                id += 4;
            }

            auto walkPath = fs::path(path).append("general").append("walk.msa");
            auto runPath = fs::path(path).append("general").append("run.msa");

            PutAnimation(id, AnimationType::WALK, AnimationSubType::GENERAL,
                         LoadAnimation(walkPath));
            PutAnimation(id, AnimationType::RUN, AnimationSubType::GENERAL,
                         LoadAnimation(runPath));
        }
    }

    Animation *AnimationManager::LoadAnimation(
        const boost::filesystem::path &msa) {
        if (!fs::exists(msa)) {
            CORE_LOGGING(error) << "File " << msa << " doesn't exists";
            return nullptr;
        }

        CORE_LOGGING(trace) << "Load " << msa;

        auto file = StructuredFileReader::Read(msa.string());
        if (file.GetValue("ScriptType") != "MotionData") {
            CORE_LOGGING(error)
                << "Failed to load " << msa << " invalid ScriptType";
            return nullptr;
        }

        auto duration = file.GetValue<float>("MotionDuration");
        auto x = file.GetValue<float>("Accumulation", 0);
        auto y = file.GetValue<float>("Accumulation", 1);
        auto z = file.GetValue<float>("Accumulation", 2);

        auto ret = new Animation();
        ret->motionDuration = duration;
        ret->accumulationX = x;
        ret->accumulationY = y;
        ret->accumulationZ = z;
        return ret;
    }

    void AnimationManager::PutAnimation(uint32_t id, AnimationType type,
                                        AnimationSubType subType,
                                        Animation *animation) {
        uint32_t animationId = type | (subType << 24);

        CORE_LOGGING(trace) << "Add animation " << animationId << " for " << id;
        if (_animations.count(id) == 0) {
            _animations[id] = std::map<uint32_t, Animation *>();
        }
        _animations[id][animationId] = animation;
    }

    const Animation *AnimationManager::GetAnimation(
        uint32_t id, AnimationType type, AnimationSubType subType) const {
        if (_animations.count(id) == 0) return nullptr;
        uint32_t animationId = type | (subType << 24);

        auto animations = _animations.at(id);
        if (animations.count(animationId) == 0) return nullptr;

        return animations.at(animationId);
    }
}  // namespace game::formats