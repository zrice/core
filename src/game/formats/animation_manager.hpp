#pragma once

#include <array>
#include <boost/filesystem.hpp>
#include <map>
#include <string>

namespace game::formats {
    enum AnimationType { WALK, RUN, MAX };

    enum AnimationSubType {
        GENERAL,
        BOW,
        BELL,
        ONEHAND_SWORD,
        DUALHAND_SWORD,
        TWOHAND_SWORD,
        FAN
    };

    struct Animation {
        float motionDuration;
        float accumulationX, accumulationY, accumulationZ;
    };

    class AnimationManager {
       public:
        AnimationManager();
        virtual ~AnimationManager();

        void Load();

        const Animation *GetAnimation(uint32_t id, AnimationType type,
                                      AnimationSubType subType) const;

       private:
        void LoadPlayerAnimations(const boost::filesystem::path &dir,
                                  bool isPC2);
        Animation *LoadAnimation(const boost::filesystem::path &msa);

        void PutAnimation(uint32_t id, AnimationType type,
                          AnimationSubType subType, Animation *animation);

       private:
        std::map<uint32_t, std::map<uint32_t, Animation *>> _animations;
    };
}  // namespace game::formats