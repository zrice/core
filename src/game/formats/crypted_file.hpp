#pragma once

#include <string>
#include <vector>

namespace game::formats {
    class CryptedFile {
       public:
        explicit CryptedFile(const std::vector<uint8_t> &data,
                             unsigned int offset = 0);
        explicit CryptedFile(const std::string &file, unsigned int offset = 0);
        virtual ~CryptedFile();

        const std::vector<uint8_t> &GetData();

       private:
        std::vector<uint8_t> _data;

        std::string _fourCC;
        unsigned int _size;
        unsigned int _compressSize;
        unsigned int _realSize;
    };
}  // namespace game::formats
