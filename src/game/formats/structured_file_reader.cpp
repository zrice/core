#include "structured_file_reader.hpp"

#include <boost/algorithm/string.hpp>
#include <fstream>

#include "../../core/logger.hpp"

namespace game::formats {
    std::string StructuredFile::GetValue(const std::string &name,
                                         unsigned int index) {
        auto value = values[name];
        std::vector<std::string> res;
        boost::split(res, value, boost::is_any_of("\t"),
                     boost::token_compress_on);

        if (res.size() <= index) return "";
        return res[index];
    }

    StructuredFile StructuredFileReader::Read(const std::string &path) {
        StructuredFile ret{};

        std::fstream stream(path);
        std::string line;

        while (std::getline(stream, line)) {
            boost::trim(line);
            if (line.empty()) continue;

            std::vector<std::string> values;
            boost::split(values, line, boost::is_any_of(" "),
                         boost::token_compress_on);

            std::string name = values[0];
            values.erase(values.begin());

            std::string value = boost::join(values, " ");

            ret.values[name] = value;
        }

        return ret;
    }
}  // namespace game::formats