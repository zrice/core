#include "item_proto.hpp"

#include <cassert>

#include "../../core/logger.hpp"
#include "../../core/utils/file.hpp"
#include "crypted_file.hpp"

namespace game::formats {
    ItemProto::ItemProto() {}

    ItemProto::~ItemProto() {}

    bool ItemProto::Load(const std::string &file,
                         std::map<uint32_t, Item> &items) {
        auto data = core::utils::ReadFile("item_proto");
        const char *fourCC = reinterpret_cast<const char *>(&data[0]);
        uint32_t version = *reinterpret_cast<uint32_t *>(&data[4]);
        uint32_t stride = *reinterpret_cast<uint32_t *>(&data[8]);
        uint32_t elements = *reinterpret_cast<uint32_t *>(&data[12]);
        uint32_t size = *reinterpret_cast<uint32_t *>(&data[16]);

        // Check FourCC
        if (strncmp(fourCC, "MIPX", 4) != 0) {
            CORE_LOGGING(error) << "Failed to load item_proto invalid header";
            return false;
        }

        // Decrypt data
        CryptedFile proto(data, 20);
        auto protoData = proto.GetData();
        // Ensure that our decrypted size matches the expected data from
        // elements * stride
        assert(stride * elements == protoData.size());

        // Iterate over all items and parse each of it
        for (auto i = 0; i < elements; i++) {
            Item item;
            item.Read(&protoData[i * stride]);
            items[item.id] = item;
        }

        return true;
    }

    void Item::Read(uint8_t *ptr) {
        id = *reinterpret_cast<uint32_t *>(ptr);
        unknown = *reinterpret_cast<uint32_t *>(ptr + 4);
        name = std::string(reinterpret_cast<const char *>(ptr + 8), 25);
        translatedName =
            std::string(reinterpret_cast<const char *>(ptr + 33), 25);
        type = *(ptr + 58);
        subtype = *(ptr + 59);
        unknown2 = *(ptr + 60);
        size = *(ptr + 61);
        antiFlags = *reinterpret_cast<uint32_t *>(ptr + 62);
        flags = *reinterpret_cast<uint32_t *>(ptr + 66);
        wearFlags = *reinterpret_cast<uint32_t *>(ptr + 70);
        immuneFlags = *reinterpret_cast<uint32_t *>(ptr + 74);
        buyPrice = *reinterpret_cast<uint32_t *>(ptr + 78);
        sellPrice = *reinterpret_cast<uint32_t *>(ptr + 82);
        for (auto i = 0, position = 86; i < 2; i++, position += 5) {
            limits[i].type = *(ptr + position);
            limits[i].value = *reinterpret_cast<int32_t *>(ptr + position + 1);
        }
        for (auto i = 0, position = 96; i < 3; i++, position += 5) {
            applies[i].type = *(ptr + position);
            applies[i].value = *reinterpret_cast<int32_t *>(ptr + position + 1);
        }
        for (auto i = 0, position = 111; i < 6; i++, position += 4) {
            values[i] = *reinterpret_cast<int32_t *>(ptr + position);
        }
        for (auto i = 0, position = 135; i < 3; i++, position += 4) {
            sockets[i] = *reinterpret_cast<int32_t *>(ptr + position);
        }
        upgradeId = *reinterpret_cast<int32_t *>(ptr + 147);
        upgradeSet = *reinterpret_cast<int16_t *>(ptr + 151);
        magicItemPercentage = *(ptr + 153);
        specular = *(ptr + 154);
        socketPercentage = *(ptr + 155);
    }
}  // namespace game::formats
