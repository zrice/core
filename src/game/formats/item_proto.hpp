#pragma once

#include <map>
#include <string>

namespace game::formats {
    struct ItemLimit {
        uint8_t type;
        int32_t value;
    };

    struct ItemApply {
        uint8_t type;
        int32_t value;
    };

    struct Item {
        uint32_t id;
        uint32_t unknown;
        std::string name;
        std::string translatedName;
        uint8_t type;
        uint8_t subtype;
        uint8_t unknown2;
        uint8_t size;
        uint32_t antiFlags;
        uint32_t flags;
        uint32_t wearFlags;
        uint32_t immuneFlags;
        uint32_t buyPrice;
        uint32_t sellPrice;
        ItemLimit limits[2];
        ItemApply applies[3];
        int32_t values[6];
        int32_t sockets[3];
        uint32_t upgradeId;
        uint16_t upgradeSet;
        uint8_t magicItemPercentage;
        uint8_t specular;
        uint8_t socketPercentage;

        void Read(uint8_t *ptr);
    };

    class ItemProto {
       public:
        ItemProto();
        virtual ~ItemProto();

        bool Load(const std::string &file, std::map<uint32_t, Item> &items);
    };
}  // namespace game::formats
