#pragma once

#include <memory>

#include "../core/application.hpp"
#include "../core/application_abstract.hpp"
#include "../core/cache/account_cache.hpp"
#include "../core/cache/player_cache.hpp"
#include "../core/networking/client.hpp"
#include "../core/networking/server.hpp"
#include "environment/world.hpp"
#include "formats/animation_manager.hpp"
#include "item/item_manager.hpp"

namespace game {
    /**
     * The "game" application.
     * The player will connect to this server after authorization.
     *
     * This application handle the main game logic.
     */
    class Application : public core::ApplicationAbstract,
                        public std::enable_shared_from_this<Application> {
       public:
        Application(const std::shared_ptr<core::Application> &core_application);
        virtual ~Application();

        void Start() override;
        void Update(uint32_t elapsedTime) override;

        std::shared_ptr<core::cache::PlayerCache> GetPlayerCache() const {
            return _playerCache;
        }
        std::shared_ptr<item::ItemManager> GetItemManager() const {
            return _itemManager;
        }
        std::shared_ptr<core::Application> GetCoreApplication() const {
            return _coreApplication;
        }
        std::shared_ptr<formats::AnimationManager> GetAnimationManager() const {
            return _animationManager;
        }

        static std::shared_ptr<Application> GetInstance();

       private:
        void RegisterPackets();

        void OnNewConnection(
            std::shared_ptr<core::networking::Connection> connection);
        void OnDisconnect(
            std::shared_ptr<core::networking::Connection> connection);
        void LoginRequest(
            std::shared_ptr<core::networking::Connection> connection,
            std::shared_ptr<core::networking::Packet> packet);
        void CharacterSelect(
            std::shared_ptr<core::networking::Connection> connection,
            std::shared_ptr<core::networking::Packet> packet);
        void EnterGame(std::shared_ptr<core::networking::Connection> connection,
                       std::shared_ptr<core::networking::Packet> packet);
        void SelectEmpire(
            std::shared_ptr<core::networking::Connection> connection,
            std::shared_ptr<core::networking::Packet> packet);
        void CreateCharacter(
            std::shared_ptr<core::networking::Connection> connection,
            std::shared_ptr<core::networking::Packet> packet);

       private:
        static std::shared_ptr<Application> _instance;

        std::shared_ptr<core::Application> _coreApplication;
        std::shared_ptr<core::networking::Server> _server;

        std::shared_ptr<core::cache::PlayerCache> _playerCache;
        std::shared_ptr<core::cache::AccountCache> _accountCache;
        std::shared_ptr<environment::World> _world;

        uint16_t _port;
        std::shared_ptr<item::ItemManager> _itemManager;
        std::shared_ptr<formats::AnimationManager> _animationManager;
    };
}  // namespace game