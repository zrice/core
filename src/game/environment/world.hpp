#pragma once

#include <map>
#include <memory>

#include "../../core/networking/connection.hpp"
#include "../../core/utils/grid.hpp"
#include "map.hpp"
#include "object.hpp"
#include "player.hpp"

namespace game::environment {
    class World {
       public:
        World();
        virtual ~World();

        void Load();
        void Update(uint32_t elapsedTime);

        std::shared_ptr<Map> GetMapAtLocation(uint64_t x, uint64_t y);
        std::shared_ptr<Map> GetMapByName(const std::string &name);

        std::shared_ptr<Player> CreatePlayer(
            uint32_t characterId,
            std::shared_ptr<core::networking::Connection> connection);
        void SpawnObject(const std::shared_ptr<Object> &object);

       private:
        std::mutex _vidMutex;
        uint32_t _vidCounter;

        std::map<std::string, std::shared_ptr<Map>> _maps;
        core::utils::Grid<std::shared_ptr<Map>> _grid;
    };
}  // namespace game::environment