#pragma once

#include <cstdint>
#include <memory>
#include <mutex>
#include <vector>

#include "object.hpp"

#define DEFAULT_QUAD_TREE_CAPACITY 20

namespace game::environment {
    struct Rectangle {
        int64_t x;
        int64_t y;
        int64_t width;
        int64_t height;

        bool Contains(int64_t x, int64_t y) const;
        bool Intersects(const Rectangle &rect);
    };

    struct Circle {
        int64_t x;
        int64_t y;
        int64_t radius;

        bool Contains(int64_t x, int64_t y) const;
        bool Intersects(const Rectangle &rect);
    };

    class QuadTree {
       public:
        QuadTree(int64_t x, int64_t y, int64_t width, int64_t height,
                 uint16_t capacity = DEFAULT_QUAD_TREE_CAPACITY);
        virtual ~QuadTree();

        bool Insert(const std::shared_ptr<Object> &object);
        bool Remove(const std::shared_ptr<Object> &object);

        void QueryObjectsAround(std::vector<std::shared_ptr<Object>> &out,
                                int64_t x, int64_t y, int32_t radius);

        void Print();

       private:
        void Subdivide();

       private:
        std::mutex _mutex;

        Rectangle _bounds;
        int64_t _x;
        int64_t _y;
        int64_t _width;
        int64_t _height;
        uint16_t _capacity;

        std::vector<std::shared_ptr<Object>> _objects;
        bool _subdivided;
        std::unique_ptr<QuadTree> _nw;
        std::unique_ptr<QuadTree> _ne;
        std::unique_ptr<QuadTree> _sw;
        std::unique_ptr<QuadTree> _se;
    };
}  // namespace game::environment
