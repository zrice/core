#include "map.hpp"

#include <utility>

#include "../../core/logger.hpp"
#include "../../core/networking/server.hpp"
#include "../../core/profiler.hpp"
#include "player.hpp"

namespace game::environment {
    Map::Map(std::string name, uint64_t x, uint64_t y, uint8_t width,
             uint8_t height)
        : _name(std::move(name)),
          _x(x),
          _y(y),
          _width(width),
          _height(height),
          _objects(),
          _quadTree(x, y, width * UNIT_SIZE, height * UNIT_SIZE) {
        CORE_LOGGING(trace)
            << "Creating map " << _name << " at " << x << "," << y << " ("
            << (int)width << "x" << (int)height << ")";
    }
    Map::~Map() = default;

    void Map::Update(uint32_t elapsedTime) {
        {
            std::lock_guard<std::mutex> lock(_spawnMutex);

            while (!_spawnQueue.empty()) {
                auto &object = _spawnQueue.front();

                if (!_quadTree.Insert(object)) {
                    CORE_LOGGING(error)
                        << "Failed to insert object into root quad tree!";
                    continue;
                }

                _objects[object->GetVID()] = object;

                _spawnQueue.pop();
            }
        }

        {
            std::lock_guard<std::mutex> lock(_despawnMutex);

            while (!_despawnQueue.empty()) {
                auto &object = _despawnQueue.front();

                _quadTree.Remove(object);

                std::vector<std::shared_ptr<Object>> objects;
                _quadTree.QueryObjectsAround(objects, object->GetPositionX(),
                                             object->GetPositionY(), 10000);

                for (auto &qobject : objects) {
                    // todo: this is an hack because we do not have monsters yet
                    std::shared_ptr<Player> player =
                        std::dynamic_pointer_cast<Player>(qobject);
                    object->Remove(player->GetConnection());
                }

                _objects.erase(object->GetVID());

                _despawnQueue.pop();
            }
        }

        for (const auto &object : _objects) {
            object.second->Update(elapsedTime);

            if (object.second->GetPositionModified()) {
                _quadTree.Remove(object.second);
                _quadTree.Insert(object.second);
            }
        }
    }

    void Map::SpawnObject(const std::shared_ptr<Object> &object) {
        std::lock_guard<std::mutex> lock(_spawnMutex);

        _spawnQueue.push(object);
    }

    void Map::DespawnObject(const std::shared_ptr<Object> &object) {
        std::lock_guard<std::mutex> lock(_despawnMutex);

        _despawnQueue.push(object);
    }

    void Map::QueryObjectsAround(std::vector<std::shared_ptr<Object>> &out,
                                 uint64_t x, uint64_t y, uint32_t radius) {
        _quadTree.QueryObjectsAround(out, x, y, radius);
    }
}  // namespace game::environment