#include "world.hpp"

#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <fstream>

#include "../../core/logger.hpp"
#include "../../core/profiler.hpp"

namespace game::environment {
    World::World() : _grid(0, 0), _vidCounter(0) {}

    World::~World() = default;

    void World::Load() {
        PROFILE_FUNCTION();
        CORE_LOGGING(trace) << "Initializing game world";

        // Keep track of upper bound for grid
        uint64_t maxX = 0;
        uint64_t maxY = 0;

        // Load atlasinfo
        std::ifstream fileStream("atlasinfo.txt");
        assert(fileStream.is_open());

        if (!fileStream.is_open()) {
            CORE_LOGGING(error) << "Failed to open atlasinfo.txt";
            throw std::runtime_error("Failed to open atlasinfo.txt");
        }

        std::vector<std::string> details;
        for (std::string line; std::getline(fileStream, line);) {
            if (line.empty()) continue;  // skip empty lines

            boost::split(details, line, boost::is_any_of("\t "));
            details.erase(std::remove_if(details.begin(), details.end(),
                                         [](const std::string &str) {
                                             return str.empty();
                                         }),
                          details.end());
            assert(details.size() == 5);

            std::string name = details[0];
            uint64_t x = std::stoul(details[1]);
            uint64_t y = std::stoul(details[2]);
            auto width = static_cast<uint8_t>(std::stoul(details[3]));
            auto height = static_cast<uint8_t>(std::stoul(details[4]));

            if (x + width * UNIT_SIZE > maxX) maxX = x + width * UNIT_SIZE;
            if (y + height * UNIT_SIZE > maxY) maxY = y + height * UNIT_SIZE;

            auto map = std::make_shared<Map>(name, x, y, width, height);
            _maps[name] = map;

            details.clear();
        }

        _grid.Resize(maxX / UNIT_SIZE, maxY / UNIT_SIZE);

        for (const auto &map : _maps) {
            for (auto x = map.second->GetUnitX();
                 x < map.second->GetUnitX() + map.second->GetWidth(); x++) {
                for (auto y = map.second->GetUnitY();
                     y < map.second->GetUnitY() + map.second->GetHeight();
                     y++) {
#ifdef NDEBUG
                    assert(_grid.Get(x, y) == nullptr);
#endif
                    _grid.Set(x, y, map.second);
                }
            }
        }
    }

    void World::Update(uint32_t elapsedTime) {
        for (const auto &map : _maps) {
            map.second->Update(elapsedTime);
        }
    }

    std::shared_ptr<Map> World::GetMapAtLocation(uint64_t x, uint64_t y) {
        auto gridX = x / UNIT_SIZE;
        auto gridY = y / UNIT_SIZE;
        if (gridX > _grid.GetWidth()) return nullptr;
        if (gridY > _grid.GetHeight()) return nullptr;

        return _grid.Get(gridX, gridY);
    }

    std::shared_ptr<Map> World::GetMapByName(const std::string &name) {
        if (_maps.find(name) == _maps.end()) return nullptr;
        return _maps[name];
    }

    std::shared_ptr<Player> World::CreatePlayer(
        uint32_t characterId,
        std::shared_ptr<core::networking::Connection> connection) {
        std::lock_guard<std::mutex> lock(_vidMutex);

        auto vid = ++_vidCounter;
        return std::make_shared<Player>(characterId, vid, connection);
    }

    void World::SpawnObject(const std::shared_ptr<Object> &object) {
        auto map =
            GetMapAtLocation(object->GetPositionX(), object->GetPositionY());
        if (!map) {
            CORE_LOGGING(error) << "Failed to spawn object at position "
                                << object->GetPositionX() << ","
                                << object->GetPositionY() << " no map found!";
            return;
        }

        CORE_LOGGING(trace) << "Spawning object on map " << map->GetName();
        map->SpawnObject(object);
        object->SetMap(map);
    }
}  // namespace game::environment
