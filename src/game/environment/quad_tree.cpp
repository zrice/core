#include "quad_tree.hpp"

#include "../../core/logger.hpp"
#include "../../core/profiler.hpp"

namespace game::environment {
    QuadTree::QuadTree(int64_t x, int64_t y, int64_t width, int64_t height,
                       uint16_t capacity)
        : _bounds{x, y, width, height},
          _x(x),
          _y(y),
          _width(width),
          _height(height),
          _capacity(capacity),
          _objects(),
          _subdivided(false) {}

    QuadTree::~QuadTree() = default;

    bool QuadTree::Insert(const std::shared_ptr<Object> &object) {
        PROFILE_FUNCTION();

        std::lock_guard<std::mutex> lock(_mutex);

        // Check if point is in boundary
        if (!_bounds.Contains(object->GetPositionX(), object->GetPositionY())) {
            return false;
        }

        // Check if our QuadTree still has space
        if (_objects.size() < _capacity) {
            _objects.push_back(object);
            return true;
        }

        // We don't have any space, subdivide if not yet happen
        if (!_subdivided) {
            Subdivide();
        }

        // Insert our object to one of the subdivisions
        return _nw->Insert(object) || _ne->Insert(object) ||
               _sw->Insert(object) || _se->Insert(object);
    }

    bool QuadTree::Remove(const std::shared_ptr<Object> &object) {
        PROFILE_FUNCTION();

        std::lock_guard<std::mutex> lock(_mutex);

        // Check if point is in boundary
        if (!_bounds.Contains(object->GetPositionX(), object->GetPositionY())) {
            return false;
        }

        // Try to remove it from our local elements
        const auto size = _objects.size();
        _objects.erase(std::remove(_objects.begin(), _objects.end(), object),
                       _objects.end());
        if (size != _objects.size()) return true;

        // If we are divided and the object wasn't in our list ask our children
        if (_subdivided) {
            return _nw->Remove(object) || _ne->Remove(object) ||
                   _sw->Remove(object) || _se->Remove(object);
        }

        return false;
    }

    void QuadTree::Subdivide() {
        auto halfWidth = _width / 2;
        auto halfHeight = _height / 2;

        _nw = std::make_unique<QuadTree>(_x, _y, halfWidth, halfHeight,
                                         _capacity);
        _sw = std::make_unique<QuadTree>(_x, _y + halfHeight, halfWidth,
                                         halfHeight, _capacity);
        _ne = std::make_unique<QuadTree>(_x + halfWidth, _y, halfWidth,
                                         halfHeight, _capacity);
        _se = std::make_unique<QuadTree>(_x + halfWidth, _y + halfHeight,
                                         halfWidth, halfHeight, _capacity);
        _subdivided = true;
    }

    void QuadTree::Print() {
        CORE_LOGGING(trace)
            << _x << "," << _y << " " << _width << "x" << _height;
        CORE_LOGGING(trace) << "Children: " << _objects.size();
        CORE_LOGGING(trace) << "Divided: " << (_subdivided ? "Yes" : "No");
    }
    void QuadTree::QueryObjectsAround(std::vector<std::shared_ptr<Object>> &out,
                                      int64_t x, int64_t y, int32_t radius) {
        PROFILE_FUNCTION();

        std::lock_guard<std::mutex> lock(_mutex);

        auto circle = Circle{x, y, radius};
        if (!circle.Intersects(_bounds)) {
            return;
        }

        for (auto &obj : _objects) {
            if (circle.Contains(obj->GetPositionX(), obj->GetPositionY())) {
                out.push_back(obj);
            }
        }

        if (_subdivided) {
            _nw->QueryObjectsAround(out, x, y, radius);
            _sw->QueryObjectsAround(out, x, y, radius);
            _ne->QueryObjectsAround(out, x, y, radius);
            _se->QueryObjectsAround(out, x, y, radius);
        }
    }

    bool Rectangle::Contains(int64_t x, int64_t y) const {
        return x >= this->x && x <= this->x + this->width && y >= this->y &&
               y <= this->y + this->height;
    }

    bool Rectangle::Intersects(const Rectangle &rect) {
        return this->x < rect.x + rect.width &&
               this->x + this->width > rect.x &&
               this->y > rect.y + rect.height && this->y + this->width < rect.y;
    }

    bool Circle::Contains(int64_t x, int64_t y) const {
        return pow(x - this->x, 2) + pow(y - this->y, 2) <=
               pow(this->radius, 2);
    }

    bool Circle::Intersects(const Rectangle &rect) {
        auto halfWidth = rect.width / 2;
        auto halfHeight = rect.height / 2;
        auto centerX = rect.x + halfWidth;
        auto centerY = rect.y + halfHeight;

        int64_t xDist = std::abs(static_cast<int64_t>(centerX - x));
        int64_t yDist = std::abs(static_cast<int64_t>(centerY - y));

        auto edges = pow(xDist - halfWidth, 2) + pow(yDist - halfHeight, 2);
        if (xDist > radius + halfWidth || yDist > radius + halfHeight) {
            return false;
        }
        if (xDist <= halfWidth || yDist <= halfHeight) {
            return true;
        }

        return edges <= pow(radius, 2);
    }
}  // namespace game::environment