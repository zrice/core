#include "player.hpp"

#include <utility>

#include "../../core/logger.hpp"
#include "../../core/profiler.hpp"
#include "../../core/utils/math.hpp"
#include "../application.hpp"
#include "../commands/command_manager.hpp"

namespace game::environment {
    Player::Player(uint32_t id, uint32_t vid,
                   std::shared_ptr<core::networking::Connection> connection)
        : Object(vid),
          _viewObjects(),
          _persistTimer(0),
          _id(id),
          _targetX(0),
          _targetY(0),
          _state(PlayerState::IDLING),
          _connection(std::move(connection)),
          _inventory(5, 9, 2),
          _name(),
          _playerClass(),
          _skillGroup(),
          _playtime(),
          _level(),
          _exp(),
          _gold(),
          _st(),
          _ht(),
          _dx(),
          _iq(),
          _hp(),
          _mp(),
          _stamina() {}

    Player::~Player() {
        Persist();
        CORE_LOGGING(trace)
            << "Destroy player " << _name << " (" << _vid << ")";
    }

    void Player::Load() {
        auto core = game::Application::GetInstance();

        auto data = core->GetPlayerCache()->GetPlayer(_id);
        _name = data.name;
        _playerClass = data.playerClass;
        _skillGroup = data.skillGroup;
        _playtime = data.playtime;
        _level = data.level;
        _exp = data.exp;
        _gold = data.gold;
        _st = data.st;
        _ht = data.ht;
        _dx = data.dx;
        _iq = data.iq;
        _posX = data.posX;
        _posY = data.posY;
        _hp = data.hp;
        _mp = data.mp;
        _stamina = data.stamina;

        _inventory.Load(_id, 1, core->GetItemManager());

        auto items = core->GetItemManager()->QueryItems(_id, 2);
        for (const auto &item : items) {
            SetItem(item->GetWindow(), item->GetPosition(), item, false);
        }
    }

    void Player::SendBasicData() {
        auto character =
            _connection->GetServer()->GetPacketManager()->CreatePacket(
                0x71, core::networking::Outgoing);
        character->SetField<uint32_t>("vid", _vid);
        character->SetString("name", _name);
        character->SetField<uint16_t>("class", _playerClass);
        character->SetField<int32_t>("posX", _posX);
        character->SetField<int32_t>("posY", _posY);
        character->SetField<uint8_t>("empire", 3);
        _connection->Send(character);
    }

    void Player::SendPoints() {
        auto points =
            _connection->GetServer()->GetPacketManager()->CreatePacket(
                0x10, core::networking::Outgoing);
        points->SetRepeatedField<uint32_t>("points", 1, 99);
        points->SetRepeatedField<uint32_t>("points", 17, _attackSpeed);
        points->SetRepeatedField<uint32_t>("points", 19, _moveSpeed);
        _connection->Send(points);
    }

    void Player::SendInventory() {
        for (auto &item : _inventory.GetItems()) {
            item.second->Send(_connection);
        }

        if (_body) _body->Send(_connection);
        if (_head) _body->Send(_connection);
        if (_shoes) _body->Send(_connection);
        if (_bracelet) _body->Send(_connection);
        if (_weapon) _weapon->Send(_connection);
        if (_necklace) _necklace->Send(_connection);
        if (_earrings) _earrings->Send(_connection);
    }

    void Player::SendCharacter(
        const std::shared_ptr<core::networking::Connection> &connection) {
        auto character =
            connection->GetServer()->GetPacketManager()->CreatePacket(
                0x01, core::networking::Outgoing);
        character->SetField<uint32_t>("vid", _vid);
        character->SetField<uint8_t>("characterType", 6);
        character->SetField<int32_t>("x", _posX);
        character->SetField<int32_t>("y", _posY);
        character->SetField<uint16_t>("class", _playerClass);
        character->SetField<uint8_t>("moveSpeed", _moveSpeed);
        character->SetField<uint8_t>("attackSpeed", _attackSpeed);
        connection->Send(character);
    }

    void Player::SendCharacterAdditional(
        const std::shared_ptr<core::networking::Connection> &connection) {
        CORE_LOGGING(trace)
            << "Send additional for " << _name << " (" << _vid << ")";

        auto additional =
            connection->GetServer()->GetPacketManager()->CreatePacket(
                0x88, core::networking::Outgoing);
        additional->SetField<uint32_t>("vid", _vid);
        additional->SetString("name", _name);
        additional->SetField<uint8_t>("empire", 3);
        additional->SetField<uint32_t>("level", _level);
        additional->SetRepeatedField<uint16_t>(
            "parts", 0, _body ? _body->GetProto().id : 0);
        additional->SetRepeatedField<uint16_t>(
            "parts", 1, _weapon ? _weapon->GetProto().id : 0);
        additional->SetRepeatedField<uint16_t>("parts", 2, 0);
        additional->SetRepeatedField<uint16_t>("parts", 3, 0);
        connection->Send(additional);
    }

    void Player::SendCharacterUpdate(
        const std::shared_ptr<core::networking::Connection> &connection) {
        auto update = connection->GetServer()->GetPacketManager()->CreatePacket(
            0x13, core::networking::Outgoing);
        update->SetField<uint32_t>("vid", _vid);
        update->SetRepeatedField<uint16_t>("parts", 0,
                                           _body ? _body->GetProto().id : 0);
        update->SetRepeatedField<uint16_t>(
            "parts", 1, _weapon ? _weapon->GetProto().id : 0);
        update->SetRepeatedField<uint16_t>("parts", 2, 0);
        update->SetRepeatedField<uint16_t>("parts", 3, 0);
        update->SetField<uint8_t>("moveSpeed", _moveSpeed);
        update->SetField<uint8_t>("attackSpeed", _attackSpeed);
        connection->Send(update);
    }

    void Player::Show(
        const std::shared_ptr<core::networking::Connection> &connection) {
        SendCharacter(connection);
        SendCharacterAdditional(connection);
    }

    void Player::Remove(
        const std::shared_ptr<core::networking::Connection> &connection) {
        auto packet = connection->GetServer()->GetPacketManager()->CreatePacket(
            0x02, core::networking::Direction::Outgoing);
        packet->SetField("vid", GetVID());
        connection->Send(packet);
    }

    std::shared_ptr<item::Item> Player::GetItem(uint8_t window,
                                                uint16_t position) {
        switch (window) {
            case 1:
                return _inventory.Get(position);
            case 2:
                switch (position) {
                    case 0:
                        return _body;
                    case 1:
                        return _head;
                    case 2:
                        return _shoes;
                    case 3:
                        return _bracelet;
                    case 4:
                        return _weapon;
                    case 5:
                        return _necklace;
                    case 6:
                        return _earrings;
                    default:
                        return nullptr;
                }
            default:
                return nullptr;
        }
    }

    bool Player::HasSpace(uint8_t window, uint16_t position, uint8_t size) {
        switch (window) {
            case 1:
                return _inventory.HasSpace(position, size);
            case 2:
                switch (position) {
                    case 0:
                        return _body == nullptr;
                    case 1:
                        return _head == nullptr;
                    case 2:
                        return _shoes == nullptr;
                    case 3:
                        return _bracelet == nullptr;
                    case 4:
                        return _weapon == nullptr;
                    case 5:
                        return _necklace == nullptr;
                    case 6:
                        return _earrings == nullptr;
                    default:
                        return false;
                }
            default:
                CORE_LOGGING(error) << "Player::HasSpace Unknown window "
                                    << static_cast<uint16_t>(window);
                return false;
        }
    }

    bool Player::RemoveItem(const std::shared_ptr<item::Item> &item,
                            bool send) {
        if (!item) return false;
        auto window = item->GetWindow();
        auto position = item->GetPosition();

        switch (window) {
            case 1:
                _inventory.Clear(position);
                item->SendRemove(_connection);
                item->SetWindow(0);
                item->SetPosition(0);
                return true;
            case 2:
                switch (position) {
                    case 0:
                        _body = nullptr;
                        break;
                    case 1:
                        _head = nullptr;
                        break;
                    case 2:
                        _shoes = nullptr;
                        break;
                    case 3:
                        _bracelet = nullptr;
                        break;
                    case 4:
                        _weapon = nullptr;
                        break;
                    case 5:
                        _necklace = nullptr;
                        break;
                    case 6:
                        _earrings = nullptr;
                        break;
                    default:
                        return false;
                }

                item->SendRemove(_connection);
                item->SetWindow(0);
                item->SetPosition(0);
                SendCharacterUpdate(_connection);  // todo send to view
                return true;
            default:
                return false;
        }
    }

    bool Player::SetItem(uint8_t window, uint16_t position,
                         const std::shared_ptr<item::Item> &item, bool send) {
        if (!item) return false;

        switch (window) {
            case 1:
                _inventory.Set(position, item);
                item->SetWindow(window);
                item->SetPosition(position);
                if (send) item->Send(_connection);
                return true;
            case 2:
                switch (position) {
                    case 0:
                        _body = item;
                        break;
                    case 1:
                        _head = item;
                        break;
                    case 2:
                        _shoes = item;
                        break;
                    case 3:
                        _bracelet = item;
                        break;
                    case 4:
                        _weapon = item;
                        break;
                    case 5:
                        _necklace = item;
                        break;
                    case 6:
                        _earrings = item;
                        break;
                    default:
                        return false;
                }

                item->SetWindow(window);
                item->SetPosition(position);
                if (send) item->Send(_connection);
                SendCharacterUpdate(_connection);  // todo send to view
                return true;
            default:
                return false;
        }
    }

    bool Player::GiveItem(const std::shared_ptr<item::Item> &item) {
        auto size = item->GetProto().size;
        for (auto i = 0; i < 90; i++) {
            if (HasSpace(1, i, size)) {
                SetItem(1, i, item);
                item->Persist();
                game::Application::GetInstance()->GetItemManager()->AddToWindow(
                    1, _id, item->GetId());
                return true;
            }
        }
        return false;
    }

    bool Player::HasSpace(uint8_t size) {
        for (auto i = 0; i < 90; i++) {
            if (HasSpace(1, i, size)) {
                return true;
            }
        }
        return false;
    }

    void Player::SendChatMessage(uint8_t messageType,
                                 const std::string &message) {
        auto packet =
            _connection->GetServer()->GetPacketManager()->CreatePacket(
                0x04, core::networking::Direction::Outgoing);
        packet->SetField<uint8_t>("messageType", messageType);
        packet->SetDynamicString(message);
        _connection->Send(packet);
    }

    void Player::SendPacketAround(
        const std::shared_ptr<core::networking::Packet> &packet) {
        std::lock_guard<std::mutex> lock(_viewObjectsMutex);

        for (const auto &objectptr : _viewObjects) {
            auto object = objectptr.lock();
            if (!object) continue;
            if (object.get() == this) continue;

            const auto &player = std::static_pointer_cast<Player>(object);
            player->GetConnection()->Send(packet);
        }
    }

    void Player::Move(int32_t x, int32_t y) {
        SetPositionX(x);
        SetPositionY(y);
    }

    void Player::Goto(int32_t x, int32_t y) {
        if (GetPositionX() == x && GetPositionY() == y)
            return;  // we are already at our target
        if (_targetX == x && _targetY == y)
            return;  // we already have this position as our target

        // Get animation data
        auto animation =
            game::Application::GetInstance()
                ->GetAnimationManager()
                ->GetAnimation(_playerClass, formats::AnimationType::RUN,
                               formats::AnimationSubType::GENERAL);
        if (!animation) {
            CORE_LOGGING(error)
                << "Failed to find animation run for " << _playerClass;
            return;
        }

        _state = PlayerState::MOVING;
        _targetX = x;
        _targetY = y;
        _startX = GetPositionX();
        _startY = GetPositionY();
        _movementStart =
            _connection->GetServer()->GetApplication()->GetCoreTime();

        float distance =
            core::utils::Math::Distance(_startX, _startY, _targetX, _targetY);
        float animationSpeed =
            -animation->accumulationY / animation->motionDuration;

        int i = 100 - _moveSpeed;
        if (i > 0) {
            i = 100 + i;
        } else if (i < 0) {
            i = 10000 / (100 - i);
        } else {
            i = 100;
        }
        int duration =
            static_cast<int>((distance / animationSpeed) * 1000) * i / 100;
        _movementDuration = duration;

        CORE_LOGGING(trace) << "Movement duration: " << _movementDuration;
    }

    void Player::Update(uint32_t elapsedTime) {
        PROFILE_FUNCTION();

        if (!_map) return;  // We aren't fully spawned yet!

        // Increment persist timer and persist the player every second
        _persistTimer += elapsedTime;
        if (_persistTimer >= 1000) {
            Persist();
            _persistTimer -= 1000;
        }

        auto time = _connection->GetServer()->GetApplication()->GetCoreTime();

        if (_state == PlayerState::MOVING) {
            auto elapsed = time - _movementStart;
            auto rate = elapsed / (float)_movementDuration;
            if (rate > 1) rate = 1;

            auto x = (int32_t)((float)(_targetX - _startX) * rate + _startX);
            auto y = (int32_t)((float)(_targetY - _startY) * rate + _startY);

            Move(x, y);

            if (rate >= 1) {
                _state = PlayerState::IDLING;
                CORE_LOGGING(trace) << "Movement of " << _name << " done";
            }
        }

        // Remove all entities which aren't in view anymore
        {
            std::lock_guard<std::mutex> lock(_viewObjectsMutex);

            _viewObjects.erase(
                std::remove_if(_viewObjects.begin(), _viewObjects.end(),
                               [this](const std::weak_ptr<Object> &objptr) {
                                   auto obj = objptr.lock();
                                   if (!obj) return true;

                                   auto distance = core::utils::Math::Distance(
                                       obj->GetPositionX(), obj->GetPositionY(),
                                       GetPositionX(), GetPositionY());
                                   if (distance > 10000) {
                                       CORE_LOGGING(trace)
                                           << "Removing object from view list";
                                       obj->Remove(_connection);
                                       return true;
                                   }

                                   return false;
                               }),
                _viewObjects.end());

            // Look for new objects in view area
            std::vector<std::shared_ptr<Object>> objects;
            _map->QueryObjectsAround(objects, GetPositionX(), GetPositionY(),
                                     10000);
            for (const auto &obj : objects) {
                if (obj.get() == this) continue;
                if (std::find_if(_viewObjects.begin(), _viewObjects.end(),
                                 [obj](const std::weak_ptr<Object> &w) -> bool {
                                     return obj == w.lock();
                                 }) != _viewObjects.end())
                    continue;

                obj->Show(_connection);
                _viewObjects.push_back(obj);
            }
        }
    }

    void Player::Persist() {
        PROFILE_FUNCTION();

        auto core = game::Application::GetInstance();
        core->GetPlayerCache()->SavePlayer(
            {_id, _name, _playerClass, _skillGroup, _playtime, _level, _exp,
             _gold, _st, _ht, _dx, _iq, GetPositionX(), GetPositionY(), _hp,
             _mp, _stamina});
    }

    void Player::DebugView() {
        std::lock_guard<std::mutex> lock(_viewObjectsMutex);

        SendChatMessage(1, "Objects in view:");
        for (const auto &objptr : _viewObjects) {
            auto obj = objptr.lock();
            if (!obj) continue;
            auto player = std::static_pointer_cast<Player>(obj);
            auto distance = core::utils::Math::Distance(
                obj->GetPositionX(), obj->GetPositionY(), GetPositionX(),
                GetPositionY());
            SendChatMessage(1, "[" + std::to_string(obj->GetVID()) + "]" +
                                   player->GetName() +
                                   " - Distance: " + std::to_string(distance));
        }
        SendChatMessage(1, "-------------------------");
    }
}  // namespace game::environment