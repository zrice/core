#pragma once

#include <memory>

namespace core::networking {
    class Connection;
}

namespace game::environment {
    class Map;

    class Object {
       public:
        explicit Object(uint32_t vid)
            : _vid(vid),
              _posX(0),
              _posY(0),
              _rotation(0),
              _positionModified(false) {}
        virtual ~Object() {}

        virtual void Show(const std::shared_ptr<core::networking::Connection>
                              &connection) = 0;
        virtual void Remove(const std::shared_ptr<core::networking::Connection>
                                &connection) = 0;
        virtual void Update(uint32_t elapsedTime) = 0;

        [[nodiscard]] uint32_t GetVID() const { return _vid; }

        [[nodiscard]] int32_t GetPositionX() const { return _posX; }
        void SetPositionX(int32_t posX) {
            if (_posX != posX) _positionModified = true;
            _posX = posX;
        }

        [[nodiscard]] int32_t GetPositionY() const { return _posY; }
        void SetPositionY(int32_t posY) {
            if (_posY != posY) _positionModified = true;
            _posY = posY;
        }

        [[nodiscard]] float GetRotation() const { return _rotation; }
        void SetRotation(float rotation) { _rotation = rotation; }

        [[nodiscard]] std::shared_ptr<Map> GetMap() const { return _map; }
        void SetMap(std::shared_ptr<Map> map) { _map = map; }

        [[nodiscard]] bool GetPositionModified() const {
            return _positionModified;
        }
        void SetPositionModified(bool positionModified) {
            _positionModified = positionModified;
        }

       protected:
        uint32_t _vid;
        int32_t _posX;
        int32_t _posY;
        bool _positionModified;
        float _rotation;
        std::shared_ptr<Map> _map;
    };
}  // namespace game::environment