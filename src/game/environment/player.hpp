#pragma once

#include <memory>
#include <string>

#include "../../core/networking/connection.hpp"
#include "../item/inventory.hpp"
#include "object.hpp"

namespace game::environment {
    enum ChatTypes {
        CHAT_NORMAL = 0,
        CHAT_INFO = 1,
        CHAT_GROUP = 3,
        CHAT_GUILD = 4,
        CHAT_COMMAND = 5,
        CHAT_SHOUT = 6
    };

    enum MovementTypes {
        WAIT = 0,
        MOVE = 1,
        ATTACK = 2,
        COMBO = 3,
        MOB_SKILL = 4,
        MAX,
        SKILL = 0x80  // why?!
    };

    enum PlayerState { IDLING, MOVING };

    class Player : public Object, public std::enable_shared_from_this<Player> {
       public:
        Player(uint32_t id, uint32_t vid,
               std::shared_ptr<core::networking::Connection> connection);
        virtual ~Player();

        [[nodiscard]] std::shared_ptr<core::networking::Connection>
        GetConnection() {
            return _connection;
        }
        void SetConnection(
            std::shared_ptr<core::networking::Connection> &&connection) {
            _connection = std::move(connection);
        }

        [[nodiscard]] uint32_t GetID() { return _id; }

        void Load();
        void Show(const std::shared_ptr<core::networking::Connection>
                      &connection) override;
        void Remove(const std::shared_ptr<core::networking::Connection>
                        &connection) override;
        void Update(uint32_t elapsedTime) override;

        void Persist();

        void Move(int32_t x, int32_t y);
        void Goto(int32_t x, int32_t y);

        std::shared_ptr<item::Item> GetItem(uint8_t window, uint16_t position);
        bool HasSpace(uint8_t window, uint16_t position, uint8_t size);
        bool HasSpace(uint8_t size);
        bool RemoveItem(const std::shared_ptr<item::Item> &item,
                        bool send = true);
        bool SetItem(uint8_t window, uint16_t position,
                     const std::shared_ptr<item::Item> &item, bool send = true);
        bool GiveItem(const std::shared_ptr<item::Item> &item);

        void SendBasicData();
        void SendPoints();
        void SendInventory();
        void SendCharacter(
            const std::shared_ptr<core::networking::Connection> &connection);
        void SendCharacterAdditional(
            const std::shared_ptr<core::networking::Connection> &connection);
        void SendCharacterUpdate(
            const std::shared_ptr<core::networking::Connection> &connection);
        void SendChatMessage(uint8_t messageType, const std::string &message);

        void SendPacketAround(
            const std::shared_ptr<core::networking::Packet> &packet);

        void OnItemMove(
            const std::shared_ptr<core::networking::Packet> &packet);
        void OnChat(const std::shared_ptr<core::networking::Packet> &packet);
        void OnMove(const std::shared_ptr<core::networking::Packet> &packet);

        void DebugView();

       public:
        [[nodiscard]] const std::string &GetName() const { return _name; }

        [[nodiscard]] uint8_t GetLevel() const { return _level; }

        void SetLevel(uint8_t level) { _level = level; }

       private:
        std::vector<std::weak_ptr<Object>> _viewObjects;
        std::mutex _viewObjectsMutex;
        uint32_t _persistTimer;

        uint32_t _id;
        std::shared_ptr<core::networking::Connection> _connection;

        item::Inventory _inventory;
        std::shared_ptr<item::Item> _body;
        std::shared_ptr<item::Item> _head;
        std::shared_ptr<item::Item> _shoes;
        std::shared_ptr<item::Item> _bracelet;
        std::shared_ptr<item::Item> _weapon;
        std::shared_ptr<item::Item> _necklace;
        std::shared_ptr<item::Item> _earrings;

        PlayerState _state;
        int32_t _startX;
        int32_t _startY;
        int32_t _targetX;
        int32_t _targetY;
        uint32_t _movementDuration;
        uint32_t _movementStart;

        uint8_t _moveSpeed = 150;
        uint8_t _attackSpeed = 140;

        std::string _name;
        uint8_t _playerClass;
        uint8_t _skillGroup;
        uint32_t _playtime;
        uint8_t _level;
        uint32_t _exp;
        uint32_t _gold;
        uint8_t _st;
        uint8_t _ht;
        uint8_t _dx;
        uint8_t _iq;
        uint32_t _hp;
        uint32_t _mp;
        uint32_t _stamina;
    };
}  // namespace game::environment