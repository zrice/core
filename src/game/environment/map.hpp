#pragma once

#include <map>
#include <memory>
#include <mutex>
#include <queue>
#include <string>

#include "object.hpp"
#include "quad_tree.hpp"

#define UNIT_SIZE 25600

namespace game::environment {
    class Map {
       public:
        Map(std::string name, uint64_t x, uint64_t y, uint8_t width,
            uint8_t height);
        virtual ~Map();

        void Update(uint32_t elapsedTime);

        [[nodiscard]] std::string GetName() const { return _name; }
        [[nodiscard]] uint64_t GetX() const { return _x; }
        [[nodiscard]] uint64_t GetUnitX() const { return _x / UNIT_SIZE; }
        [[nodiscard]] uint64_t GetY() const { return _y; }
        [[nodiscard]] uint64_t GetUnitY() const { return _y / UNIT_SIZE; }
        [[nodiscard]] uint8_t GetWidth() const { return _width; }
        [[nodiscard]] uint8_t GetHeight() const { return _height; }

        void SpawnObject(const std::shared_ptr<Object> &object);
        void DespawnObject(const std::shared_ptr<Object> &object);
        void QueryObjectsAround(std::vector<std::shared_ptr<Object>> &out,
                                uint64_t x, uint64_t y, uint32_t radius);

       private:
        std::string _name;
        uint64_t _x;
        uint64_t _y;
        uint8_t _width;
        uint8_t _height;

        std::map<uint32_t, std::shared_ptr<Object>> _objects;
        QuadTree _quadTree;

        std::queue<std::shared_ptr<Object>> _despawnQueue;
        std::mutex _despawnMutex;
        std::queue<std::shared_ptr<Object>> _spawnQueue;
        std::mutex _spawnMutex;
    };
}  // namespace game::environment