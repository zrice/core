#pragma once

#include <functional>
#include <memory>
#include <string>
#include <vector>

namespace game::environment {
    class Player;
}

namespace game::commands {
    typedef std::function<void(const std::shared_ptr<environment::Player> &,
                               const std::vector<std::string> &)>
        CommandFunction;

    struct Command {
        std::string name;
        CommandFunction func;
    };

    class CommandManager {
       public:
        static void RegisterDefaultCommands();

        static void RegisterCommand(const std::string &name,
                                    CommandFunction &&func);
        static void Execute(const std::shared_ptr<environment::Player> &player,
                            const std::string &command);

       private:
        static std::vector<Command> _commands;
    };
}  // namespace game::commands