#include "command_manager.hpp"

#include <sstream>
#include <utility>

#include "../../core/logger.hpp"
#include "../../core/networking/reserved_headers.hpp"
#include "../application.hpp"
#include "../environment/player.hpp"

namespace game::commands {
    std::vector<Command> CommandManager::_commands;

    void CommandManager::RegisterCommand(const std::string &name,
                                         CommandFunction &&func) {
        Command command{name, std::move(func)};
        _commands.push_back(command);
    }

    void CommandManager::Execute(
        const std::shared_ptr<environment::Player> &player,
        const std::string &command) {
        if (!player) return;

        std::stringstream stream(command.substr(1));
        std::string segment;
        std::vector<std::string> arguments;

        while (std::getline(stream, segment, ' ')) {
            arguments.push_back(segment);
        }

        auto commandName = arguments.front();
        for (const auto &command : _commands) {
            if (command.name == commandName) {
                command.func(player, arguments);
                return;
            }
        }

        player->SendChatMessage(game::environment::CHAT_INFO,
                                "Command not found.");
    }

    void CommandManager::RegisterDefaultCommands() {
        RegisterCommand(
            "item", [](const std::shared_ptr<environment::Player> &player,
                       const std::vector<std::string> &arguments) {
                if (arguments.size() != 2) {
                    player->SendChatMessage(game::environment::CHAT_INFO,
                                            "/item [vnum]");
                    return;
                }

                try {
                    auto str = arguments[1];
                    auto vnum = boost::lexical_cast<uint32_t>(arguments[1]);
                    auto itemManager =
                        game::Application::GetInstance()->GetItemManager();
                    if (!itemManager->HasProto(vnum)) {
                        player->SendChatMessage(game::environment::CHAT_INFO,
                                                "Item not found.");
                        return;
                    }

                    auto proto = itemManager->GetProto(vnum);
                    if (!player->HasSpace(proto.size)) {
                        player->SendChatMessage(game::environment::CHAT_INFO,
                                                "No space in inventory.");
                        return;
                    }

                    auto item = itemManager->CreateItem(vnum, player->GetID());
                    player->GiveItem(item);
                } catch (boost::bad_lexical_cast &e) {
                    CORE_LOGGING(trace) << "'" << arguments[1] << "'";
                    player->SendChatMessage(game::environment::CHAT_INFO,
                                            "Invalid vnum.");
                }
            });

        RegisterCommand(
            "debug", [](const std::shared_ptr<environment::Player> &player,
                        const std::vector<std::string> &arguments) {
                if (arguments.size() < 2) {
                    player->SendChatMessage(game::environment::CHAT_INFO,
                                            "/debug [view]");
                    return;
                }

                if (arguments[1] == "view") {
                    player->DebugView();
                }
            });

        RegisterCommand("quit",
                        [](const std::shared_ptr<environment::Player> &player,
                           const std::vector<std::string> &arguments) {
                            player->SendChatMessage(
                                game::environment::CHAT_COMMAND, "quit");
                            player->GetConnection()->Close();
                        });

        RegisterCommand("logout",
                        [](const std::shared_ptr<environment::Player> &player,
                           const std::vector<std::string> &arguments) {
                            player->GetConnection()->Close();
                        });

        RegisterCommand(
            "phase_select",
            [](const std::shared_ptr<environment::Player> &player,
               const std::vector<std::string> &arguments) {
                auto connection = player->GetConnection();

                connection->SetPlayer(nullptr);

                player->SetConnection(nullptr);
                auto map = player->GetMap();
                if (map) {
                    map->DespawnObject(player);
                }

                connection->SetPhase(core::networking::Phases::PHASE_SELECT);
            });
    }
}  // namespace game::commands