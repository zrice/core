#include "item.hpp"

#include <utility>

#include "../../core/mysql/migrations/_defaults.hpp"
#include "../../core/networking/server.hpp"

namespace game::item {
    Item::Item(std::shared_ptr<ItemManager> manager, formats::Item proto,
               uint32_t owner, uint64_t id, uint8_t count)
        : _manager(std::move(manager)),
          _proto(std::move(proto)),
          _owner(owner),
          _id(id),
          _count(count),
          _window(0),
          _position(0) {}

    Item::~Item() = default;

    void Item::Send(
        const std::shared_ptr<core::networking::Connection> &connection) const {
        auto packet = connection->GetServer()->GetPacketManager()->CreatePacket(
            0x15, core::networking::Direction::Outgoing);
        // todo make position adjustment variable based on the player inventory
        // size
        packet->SetField<uint8_t>("window", _window == 2 ? 1 : _window);
        packet->SetField<uint16_t>("position",
                                   _window == 2 ? _position + 90 : _position);
        packet->SetField<uint32_t>("vnum", _proto.id);
        packet->SetField<uint8_t>("count", _count);
        packet->SetField<uint32_t>("flags", _proto.flags);
        packet->SetField<uint32_t>("antiFlags", _proto.antiFlags);
        connection->Send(packet);
    }

    void Item::SendRemove(
        const std::shared_ptr<core::networking::Connection> &connection) const {
        Item::SendRemove(_window, _position, connection);
    }

    void Item::SendRemove(
        uint8_t window, uint16_t position,
        const std::shared_ptr<core::networking::Connection> &connection) {
        auto packet = connection->GetServer()->GetPacketManager()->CreatePacket(
            0x15, core::networking::Direction::Outgoing);
        // todo make position adjustment variable based on the player inventory
        // size
        packet->SetField<uint8_t>("window", window == 2 ? 1 : window);
        packet->SetField<uint16_t>("position",
                                   window == 2 ? position + 90 : position);
        packet->SetField<uint32_t>("vnum", 0);
        packet->SetField<uint8_t>("count", 0);
        packet->SetField<uint32_t>("flags", 0);
        packet->SetField<uint32_t>("antiFlags", 0);
        connection->Send(packet);
    }

    uint8_t Item::Add(uint8_t count) {
        if (static_cast<uint16_t>(_count) + count > 255) {
            _count = 255;
        } else {
            _count += count;
        }

        return _count;
    }

    uint8_t Item::Remove(uint8_t count) {
        if (static_cast<int16_t>(_count) - count <= 0) {
            _count = 0;
            Destroy();
        } else {
            _count -= count;
        }

        return _count;
    }

    void Item::Destroy() {}

    void Item::Persist(bool create) {
        assert(_id > 0);
        auto key = "item:" + std::to_string(_id);

        _manager->_redis->Execute(bredis::command_container_t{
            bredis::single_command_t{"hset", key, "owner",
                                     std::to_string(_owner)},
            bredis::single_command_t{"hset", key, "vnum",
                                     std::to_string(_proto.id)},
            bredis::single_command_t{"hset", key, "count",
                                     std::to_string(_count)},
            bredis::single_command_t{"hset", key, "position",
                                     std::to_string(_position)},
            bredis::single_command_t{"hset", key, "window",
                                     std::to_string(_window)},
        });
    }
    std::shared_ptr<Item> Item::Load(std::shared_ptr<ItemManager> manager,
                                     uint64_t id) {
        auto key = "item:" + std::to_string(id);

        CORE_LOGGING(trace) << "Load item " << id;
        auto exists = manager->_redis->Execute<int64_t>(
            bredis::single_command_t{"exists", key});
        if (exists == 1) {
            CORE_LOGGING(trace) << "Item found in the cache";
            // Item is cached use the cache
            auto result = manager->_redis->ExecuteArray(
                bredis::single_command_t{"hgetall", key});

            formats::Item proto;
            uint32_t owner;
            uint8_t count;
            uint16_t position;
            uint16_t window;

            for (auto i = 0; i < result.elements.size(); i += 2) {
                auto fieldName =
                    boost::get<core::cache::string_t>(result.elements[i]);
                auto value =
                    boost::get<core::cache::string_t>(result.elements[i + 1]);

                if (fieldName.str == "owner") {
                    owner = boost::lexical_cast<uint32_t>(value.str);
                } else if (fieldName.str == "vnum") {
                    proto = manager->GetProto(
                        boost::lexical_cast<uint32_t>(value.str));
                } else if (fieldName.str == "count") {
                    count = static_cast<uint8_t>(
                        boost::lexical_cast<uint16_t>(value.str));
                } else if (fieldName.str == "position") {
                    position = boost::lexical_cast<uint16_t>(value.str);
                } else if (fieldName.str == "window") {
                    window = boost::lexical_cast<uint16_t>(value.str);
                }
            }

            auto item = std::make_shared<Item>(manager, proto, id, count);
            item->_position = position;
            item->_window = window;

            return item;
        }

        CORE_LOGGING(trace) << "Item not cached yet";
        auto stmt = manager->_database->CreateStatement(
            "SELECT * FROM `" GAME_DATABASE "`.`items` WHERE id=%1%");
        stmt << id;

        auto result = stmt();
        if (!result->Next()) {
            CORE_LOGGING(error) << "Failed to find item with id " << id;
            return nullptr;
        }

        auto proto = manager->GetProto(result->GetUnsigned32("vnum"));
        auto player = result->GetUnsigned32("player_id");
        auto count = result->GetUnsigned8("count");
        auto position = result->GetUnsigned16("position");
        auto window = result->GetUnsigned16("window");

        manager->_redis->Execute(bredis::command_container_t{
            bredis::single_command_t{"hset", key, "owner",
                                     std::to_string(player)},
            bredis::single_command_t{"hset", key, "vnum",
                                     std::to_string(proto.id)},
            bredis::single_command_t{"hset", key, "count",
                                     std::to_string(count)},
            bredis::single_command_t{"hset", key, "position",
                                     std::to_string(position)},
            bredis::single_command_t{"hset", key, "window",
                                     std::to_string(window)}});

        auto item = std::make_shared<Item>(manager, proto, id, count);
        item->_position = position;
        item->_window = window;

        return item;
    }
}  // namespace game::item
