#include "inventory.hpp"

#include "../../core/logger.hpp"

namespace game::item {
    Inventory::Inventory(uint16_t width, uint16_t height, uint8_t pages)
        : _width(width),
          _height(height),
          _pageSize(width * height),
          _pages(pages),
          _grids() {
        assert(pages > 0);

        for (auto i = 0; i < pages; i++) {
            auto grid = std::make_unique<InventoryGrid>(width, height);
            _grids.push_back(std::move(grid));
        }
    }

    Inventory::~Inventory() {}

    void Inventory::Load(uint32_t playerId, uint16_t window,
                         const std::shared_ptr<ItemManager> &manager) {
        auto items = manager->QueryItems(playerId, window);
        for (const auto &item : items) {
            Set(item->GetPosition(), item);
        }
    }

    const std::map<uint32_t, std::shared_ptr<Item>> &Inventory::GetItems()
        const {
        return _items;
    }

    bool Inventory::Set(uint32_t position, const std::shared_ptr<Item> &item) {
        uint8_t page;
        uint16_t x, y;
        if (!CalculatePosition(position, page, x, y)) {
            return false;
        }

        _items[position] = item;

        CORE_LOGGING(trace) << "Place item at " << x << "," << y << " ("
                            << item->GetId() << ")";

        // todo maybe verify if item is overlapping with something
        for (auto i = 0; i < item->GetProto().size; i++) {
            _grids[page]->Set(x, y + i, item);
        }

        return true;
    }

    void Inventory::Clear(uint32_t position) {
        if (_items.count(position) == 0) {
            return;
        }

        auto item = _items[position];

        uint8_t page;
        uint16_t x, y;
        if (!CalculatePosition(position, page, x, y)) {
            CORE_LOGGING(error) << "Failed to clear inventory position because "
                                   "it is out of bounds! (item id "
                                << item->GetId() << ")";
            return;
        }

        for (auto i = 0; i < item->GetProto().size; i++) {
            _grids[page]->Set(x, y + i, nullptr);
        }
        _items.erase(_items.find(position));
    }

    bool Inventory::CalculatePosition(uint32_t position, uint8_t &page,
                                      uint16_t &x, uint16_t &y) const {
        page = position / _pageSize;
        if (page >= _pages) {
            return false;
        }

        position -= _pageSize * page;

        y = position / _width;
        x = position - y * _width;

        if (y >= _height) return false;

        return true;
    }

    std::shared_ptr<Item> Inventory::Get(uint32_t position) {
        uint8_t page;
        uint16_t x, y;
        if (!CalculatePosition(position, page, x, y)) {
            return nullptr;
        }

        return Get(page, x, y);
    }

    std::shared_ptr<Item> Inventory::Get(uint8_t page, uint16_t x, uint16_t y) {
        return _grids[page]->Get(x, y);
    }

    bool Inventory::HasSpace(uint32_t position, uint8_t size) {
        uint8_t page;
        uint16_t x, y;
        if (!CalculatePosition(position, page, x, y)) {
            return false;
        }

        for (auto i = 0; i < size; i++) {
            if (y + i >= _height) return false;  // we are out of bounds!
            if (Get(position + i * _width) != nullptr)
                return false;  // position is blocked!
        }

        return true;
    }
}  // namespace game::item