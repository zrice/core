#pragma once

#include <map>
#include <memory>

#include "../../core/cache/redis.hpp"
#include "../../core/mysql/mysql.hpp"
#include "../formats/item_proto.hpp"

namespace game::item {
    class Item;

    class ItemManager : public std::enable_shared_from_this<ItemManager> {
        friend Item;

       public:
        ItemManager(std::shared_ptr<core::cache::Redis> redis,
                    std::shared_ptr<core::mysql::MySQL> database);
        virtual ~ItemManager();

        std::vector<std::shared_ptr<Item>> QueryItems(uint32_t playerId,
                                                      uint16_t window);
        void RemoveFromWindow(uint16_t window, uint32_t playerId, uint64_t id);
        void AddToWindow(uint16_t window, uint32_t playerId, uint64_t id);

        std::shared_ptr<Item> CreateItem(uint32_t vnum, uint32_t owner);
        std::shared_ptr<Item> GetItemById(uint64_t id);

        void Load();

        bool HasProto(uint32_t id) const;
        const formats::Item &GetProto(uint32_t id) const;

       private:
        std::shared_ptr<core::cache::Redis> _redis;
        std::shared_ptr<core::mysql::MySQL> _database;
        std::map<uint32_t, formats::Item> _protos;
    };
}  // namespace game::item
