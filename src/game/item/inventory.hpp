#pragma once

#include <cstdint>
#include <memory>
#include <vector>

#include "../../core/utils/grid.hpp"
#include "item.hpp"
#include "item_manager.hpp"

namespace game::item {
    typedef core::utils::Grid<std::shared_ptr<Item>> InventoryGrid;

    class Inventory {
       public:
        Inventory(uint16_t width, uint16_t height, uint8_t pages = 1);
        virtual ~Inventory();

        void Load(uint32_t playerId, uint16_t window,
                  const std::shared_ptr<ItemManager> &manager);
        std::shared_ptr<Item> Get(uint32_t position);
        std::shared_ptr<Item> Get(uint8_t page, uint16_t x, uint16_t y);
        bool Set(uint32_t position, const std::shared_ptr<Item> &item);
        void Clear(uint32_t position);

        bool HasSpace(uint32_t position, uint8_t size);

        const std::map<uint32_t, std::shared_ptr<Item>> &GetItems() const;

       private:
        bool CalculatePosition(uint32_t position, uint8_t &page, uint16_t &x,
                               uint16_t &y) const;

       private:
        uint16_t _width;
        uint16_t _height;
        uint8_t _pages;

        uint32_t _pageSize;

        std::map<uint32_t, std::shared_ptr<Item>> _items;
        std::vector<std::unique_ptr<InventoryGrid>> _grids;
    };
}  // namespace game::item
