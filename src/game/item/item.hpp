#pragma once

#include <memory>

#include "../../core/networking/connection.hpp"
#include "../formats/item_proto.hpp"
#include "item_manager.hpp"

namespace game::item {
    class Item {
        friend ItemManager;

       public:
        Item(std::shared_ptr<ItemManager> manager, formats::Item proto,
             uint32_t owner, uint64_t id, uint8_t count = 1);
        virtual ~Item();

        /// Send this item as a set command to the client
        void Send(const std::shared_ptr<core::networking::Connection>
                      &connection) const;
        void SendRemove(const std::shared_ptr<core::networking::Connection>
                            &connection) const;
        static void SendRemove(
            uint8_t window, uint16_t position,
            const std::shared_ptr<core::networking::Connection> &connection);

        /// Increase the count and returns the new count.
        uint8_t Add(uint8_t count);

        /// Decrease the count and returns the new count. If the returned count
        /// is zero the item got removed
        uint8_t Remove(uint8_t count);

        /// Removes the item
        void Destroy();

        /// Saves the item to the redis cache
        /// \param create Currently unused but should only be used by
        /// ItemManager!
        void Persist(bool create = false);

        [[nodiscard]] uint64_t GetId() const { return _id; }
        [[nodiscard]] const formats::Item &GetProto() const { return _proto; }
        [[nodiscard]] uint16_t GetPosition() const { return _position; }
        void SetPosition(uint16_t position) { _position = position; }
        [[nodiscard]] uint16_t GetWindow() const { return _window; }
        void SetWindow(uint16_t window) { _window = window; }

       private:
        static std::shared_ptr<Item> Load(std::shared_ptr<ItemManager> manager,
                                          uint64_t id);

       private:
        std::shared_ptr<ItemManager> _manager;
        formats::Item _proto;

        uint32_t _owner;
        uint64_t _id;
        uint8_t _count;
        uint16_t _window{};
        uint16_t _position{};
    };
}  // namespace game::item
