#pragma once

#ifdef ENABLE_ANGELSCRIPT
#include "../../../core/scripting/angelscript/angelscript.hpp"

namespace game::scripting::angelscript {
    class Interface {
       public:
        static void RegisterInterface(
            std::shared_ptr<core::scripting::angelscript::AngelScript> as);
    };
}  // namespace game::scripting::angelscript
#endif