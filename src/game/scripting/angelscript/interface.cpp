#include "interface.hpp"

#include "../../../core/scripting/angelscript/proxy/mysql.hpp"
#include "../../../core/scripting/angelscript/scriptarray.hpp"
#include "../../application.hpp"
#include "../../commands/command_manager.hpp"
#include "proxy/player.hpp"

#ifdef ENABLE_ANGELSCRIPT
namespace game::scripting::angelscript {
    core::scripting::angelscript::proxy::MySQL *GetMySQL() {
        return new core::scripting::angelscript::proxy::MySQL(
            game::Application::GetInstance()->GetCoreApplication()->GetMySQL());
    }

    void RegisterCommand(std::string name, asIScriptFunction *function) {
        CORE_LOGGING(trace)
            << "Register command " << name << " to " << function->GetName();
        commands::CommandManager::RegisterCommand(
            name, [function](const std::shared_ptr<environment::Player> &player,
                             const std::vector<std::string> &arguments) {
                CORE_LOGGING(trace) << "Call AngelScript callback";

                auto engine = std::static_pointer_cast<
                    core::scripting::angelscript::AngelScript>(
                    game::Application::GetInstance()
                        ->GetCoreApplication()
                        ->GetScriptEngine("AngelScript"));
                auto asEngine = engine->GetEngine();
                auto arrayType = asEngine->GetTypeInfoByDecl("array<string>");

                auto array = CScriptArray::Create(arrayType, arguments.size());
                for (auto i = 0; i < array->GetSize(); i++) {
                    auto arg = arguments[i];
                    array->SetValue(i, &arg);
                }

                auto ctx = engine->GetContext();
                ctx->Prepare(function);
                ctx->SetArgAddress(0, new proxy::Player(player));
                ctx->SetArgObject(1, array);
                auto result = ctx->Execute();
                if (result != asEXECUTION_FINISHED) {
                    CORE_LOGGING(error)
                        << "Failed to call callback for command";
                }
                engine->ReturnContext(ctx);
            });
    }

#define REGISTER_REF_CLASS(name)                                             \
    r = r = engine->RegisterObjectType(#name, 0, asOBJ_REF);                 \
    assert(r >= 0);                                                          \
    r = engine->RegisterObjectBehaviour(#name, asBEHAVE_ADDREF, "void f()",  \
                                        asMETHOD(proxy::name, AddRef),       \
                                        asCALL_THISCALL);                    \
    assert(r >= 0);                                                          \
    r = engine->RegisterObjectBehaviour(#name, asBEHAVE_RELEASE, "void f()", \
                                        asMETHOD(proxy::name, ReleaseRef),   \
                                        asCALL_THISCALL);                    \
    assert(r >= 0)

    void Interface::RegisterInterface(
        std::shared_ptr<core::scripting::angelscript::AngelScript> as) {
        auto engine = as->GetEngine();
        int r;

        r = engine->SetDefaultNamespace("game");
        assert(r >= 0);
        REGISTER_REF_CLASS(Player);
        r = engine->RegisterObjectMethod(
            "Player", "void SendChatMessage(uint8, string &in)",
            asMETHOD(proxy::Player, SendChatMessage), asCALL_THISCALL);
        assert(r >= 0);
        r = engine->RegisterObjectMethod("Player", "string get_name() property",
                                         asMETHOD(proxy::Player, GetName),
                                         asCALL_THISCALL);
        assert(r >= 0);
        r = engine->RegisterObjectMethod("Player", "uint8 get_level() property",
                                         asMETHOD(proxy::Player, GetLevel),
                                         asCALL_THISCALL);
        assert(r >= 0);
        r = engine->RegisterObjectMethod(
            "Player", "void set_level(uint8) property",
            asMETHOD(proxy::Player, SetLevel), asCALL_THISCALL);
        assert(r >= 0);

        r = engine->SetDefaultNamespace("core");
        assert(r >= 0);
        r = engine->RegisterGlobalFunction("MySQL@ GetMySQL()",
                                           asFUNCTION(GetMySQL), asCALL_CDECL);
        assert(r >= 0);
        r = engine->SetDefaultNamespace("command");
        assert(r >= 0);
        r = engine->RegisterFuncdef(
            "void CommandCallback(game::Player@, string[])");
        assert(r >= 0);
        r = engine->RegisterGlobalFunction(
            "void RegisterCommand(string, CommandCallback @cb)",
            asFUNCTION(RegisterCommand), asCALL_CDECL);
        assert(r >= 0);
    }
}  // namespace game::scripting::angelscript
#endif