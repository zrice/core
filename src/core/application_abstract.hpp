#pragma once

namespace core {
    class ApplicationAbstract {
       public:
        virtual void Start() = 0;
        virtual void Update(uint32_t elapsedTime) = 0;
    };
}  // namespace core
