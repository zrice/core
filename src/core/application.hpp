#pragma once

#include <boost/asio.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/property_tree/ptree.hpp>

#include "application_abstract.hpp"
#include "cache/redis.hpp"
#include "mysql/mysql.hpp"
#include "scripting/scripting.hpp"

namespace core {
    /**
     * This is the main application which is created by the executable.
     * It will load the selected server task, connect to the database
     * and connect to the cache.
     */
    class Application : public std::enable_shared_from_this<Application> {
       public:
        /**
         * Initialize all required components for the core.
         */
        Application();

        /**
         * Will start the given application type and start the io context.
         */
        void Start(std::shared_ptr<ApplicationAbstract> application);

        /**
         * @return The configuration of the core.
         */
        const boost::property_tree::ptree &GetConfiguration() const;

        /**
         * @return The configured database connection.
         */
        std::shared_ptr<mysql::MySQL> GetMySQL() const;

        /**
         * @return The network io context, used for handling async processing.
         */
        boost::asio::io_context &GetContext();

        /**
         * @return The connection to redis. It is recommended to always use the
         * prepared cache objects.
         */
        std::shared_ptr<cache::Redis> GetRedis() const;

        /**
         * @return Past milliseconds since the application was started.
         */
        uint32_t GetCoreTime();

        /**
         * @return The application type configured in the configuration
         */
        const std::string &GetType();

        /**
         * Search for a script engine given by it's name
         * @param name name of script engine
         * @return shared pointer to the script engine or a nullptr if the
         * engine is not available
         */
        std::shared_ptr<scripting::ScriptingEngineAbstract> GetScriptEngine(
            const std::string &name);

       private:
        void UpdateLoop();
        void StartNetworkThread();

       private:
        void InitializeLogging();
        void LoadConfiguration();
        void InitializeDatabase();
        void InitializeCache();
        void InitializeScripting();

        bool VerifyConfiguration();

        std::thread _networkThread;

        boost::property_tree::ptree _configuration;
        std::string _type;
        std::shared_ptr<ApplicationAbstract> _application;
        std::shared_ptr<scripting::Scripting> _scripting;
        std::shared_ptr<mysql::MySQL> _mysql;
        std::shared_ptr<cache::Redis> _redis;

        boost::asio::io_context _context;
        boost::posix_time::ptime _bootTime;
    };
}  // namespace core
