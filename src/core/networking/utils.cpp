#include "utils.hpp"

#include <boost/asio.hpp>

#include "../logger.hpp"

namespace core::networking {
    std::string GetLocalAddress() {
        boost::asio::io_service service;
        boost::asio::ip::tcp::resolver resolver(service);

        std::string hostName = boost::asio::ip::host_name();

        auto endpoints = resolver.resolve({hostName, ""});
        for (auto &endpoint : endpoints) {
            if (endpoint.endpoint().protocol() != boost::asio::ip::tcp::v4())
                continue;  // the client currently only supports IPv4!
            return endpoint.endpoint().address().to_string();
        }

        CORE_LOGGING(error) << "Failed to get local address of core";
        return "127.0.0.1";
    }

    int32_t ConvertIP(const std::string &ip) {
        std::istringstream stream(ip);
        int firstOctal, secondOctal, thirdOctal, fourthOctal;

        stream >> firstOctal;
        stream.ignore(1);
        stream >> secondOctal;
        stream.ignore(1);
        stream >> thirdOctal;
        stream.ignore(1);
        stream >> fourthOctal;

        int32_t ret = 0;
        ret |= (fourthOctal & 0xff) << 24;
        ret |= (thirdOctal & 0xff) << 16;
        ret |= (secondOctal & 0xff) << 8;
        ret |= (firstOctal & 0xff) << 0;

        CORE_LOGGING(trace)
            << ip << " -> " << firstOctal << " " << secondOctal << " "
            << thirdOctal << " " << fourthOctal << " -> " << ret;

        return ret;
    }
}  // namespace core::networking