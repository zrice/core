#include "server.hpp"

#include <boost/bind.hpp>
#include <utility>

#include "../application.hpp"
#include "packets/default_packets.hpp"
#include "reserved_headers.hpp"

namespace ip = boost::asio::ip;

namespace core::networking {
    Server::Server(std::shared_ptr<Application> application, std::string host,
                   unsigned short port, uint8_t securityLevel,
                   const uint32_t defaultKeys[])
        : _application(std::move(application)),
          _host(host),
          _port(port),
          _acceptor(_application->GetContext(),
                    ip::tcp::endpoint(ip::tcp::v4(), port)),
          _securityLevel(securityLevel),
          _connections(),
          _connectionsMutex(),
          _nextConnectionId(1) {
        CORE_LOGGING(trace) << "Creating server on " << host << ":" << port;

        _packetManager = std::make_shared<PacketManager>();

        memcpy(_defaultKeys, defaultKeys, sizeof(_defaultKeys));

        if (_securityLevel > SECURITY_LEVEL_NONE) RegisterDefault();
    }

    Server::~Server() { CORE_LOGGING(trace) << "Stopping server"; }

    void Server::RegisterDefault() {
        // Packets
        packets::RegisterDefault_packets(_packetManager);

        // Handlers
        RegisterHandler(ReservedIncomingHeaders::HEADER_INC_HANDSHAKE,
                        [](std::shared_ptr<Connection> connection,
                           std::shared_ptr<Packet> packet) {
                            connection->HandleHandshake(packet);
                        });

        if (_securityLevel > SECURITY_LEVEL_XTEA) {
            RegisterHandler(ReservedIncomingHeaders::HEADER_INC_KEY_AGREEMENT,
                            [](std::shared_ptr<Connection> connection,
                               std::shared_ptr<Packet> packet) {
                                connection->HandleKeyAgreement(packet);
                            });
        }
    }

    void Server::Run() { StartAccept(); }

    std::shared_ptr<PacketManager> Server::GetPacketManager() {
        return _packetManager;
    }

    void Server::SetNewConnectionHandler(NewConnectionHandler handler) {
        _newConnectionHandler = handler;
    }

    void Server::SetDisconnectHandler(Server::DisconnectHandler handler) {
        _disconnectHandler = handler;
    }

    void Server::RegisterHandler(uint8_t header, PacketHandler handler) {
        CORE_LOGGING(trace)
            << "Register handler for " << std::hex << (int)header;
        _packetHandlers[header] = handler;
    }

    void Server::CallNewConnectionHandler(
        const std::shared_ptr<Connection> &connection) {
        if (_newConnectionHandler) _newConnectionHandler(connection);
    }

    void Server::CallDisconnectHandler(
        const std::shared_ptr<Connection> &connection) {
        if (_disconnectHandler) _disconnectHandler(connection);
    }

    void Server::CallPacketHandler(std::shared_ptr<Connection> connection,
                                   std::shared_ptr<Packet> packet) {
        if (_packetHandlers.find(packet->GetHeader()) !=
            _packetHandlers.end()) {
            _packetHandlers[packet->GetHeader()](connection, packet);
        }
    }

    std::shared_ptr<Application> Server::GetApplication() {
        return _application;
    }

    void Server::StartAccept() {
        CORE_LOGGING(trace) << "Waiting for incoming connection";

        auto connection =
            Connection::Create(_application->GetContext(), shared_from_this(),
                               _securityLevel, _defaultKeys);  // todo @ config

        _acceptor.async_accept(
            connection->GetSocket(),
            boost::bind(&Server::HandleAccept, this, connection,
                        boost::asio::placeholders::error));
    }

    void Server::HandleAccept(std::shared_ptr<Connection> connection,
                              const boost::system::error_code &err) {
        if (!err) {
            AssignConnection(connection);
            connection->Start();
        }

        // Wait for next connection
        StartAccept();
    }

    void Server::AssignConnection(std::shared_ptr<Connection> connection) {
        const std::lock_guard<std::mutex> lock(_connectionsMutex);

        auto id = _nextConnectionId++;
        connection->SetId(id);
        _connections[id] = std::move(connection);
    }

    std::shared_ptr<Connection> Server::GetConnection(uint64_t id) {
        const std::lock_guard<std::mutex> lock(_connectionsMutex);

        if (_connections.find(id) == _connections.end()) return nullptr;
        return _connections[id];
    }

    void Server::RemoveConnection(uint64_t id) {
        const std::lock_guard<std::mutex> lock(_connectionsMutex);

        assert(_connections.find(id) != _connections.end());
        _connections.erase(id);
    }

    void Server::SendToAll(const std::shared_ptr<Packet> &packet) {
        const std::lock_guard<std::mutex> lock(_connectionsMutex);

        for (auto &connection : _connections) {
            connection.second->Send(packet);
        }
    }

    void Server::SendTo(
        const std::shared_ptr<Packet> &packet,
        std::function<bool(std::shared_ptr<Connection> &)> filter) {
        const std::lock_guard<std::mutex> lock(_connectionsMutex);

        for (auto &connection : _connections) {
            if (filter(connection.second)) {
                connection.second->Send(packet);
            }
        }
    }
    void Server::SendToPhase(std::shared_ptr<Packet> packet, uint8_t phase) {
        SendTo(std::move(packet),
               [phase](std::shared_ptr<Connection> &connection) {
                   return connection->GetPhase() == phase;
               });
    }
}  // namespace core::networking
