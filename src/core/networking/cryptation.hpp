#pragma once

#include <cryptopp/dh.h>
#include <cryptopp/dh2.h>
#include <cryptopp/modes.h>

namespace core::networking {
    class CryptationBase {
       public:
        CryptationBase() : _initialized(false), _activated(false) {}
        virtual ~CryptationBase() {}

        bool IsReady() { return _activated; }

        bool IsInitialized() { return _initialized; }

        virtual bool Finalize() { return false; }
        virtual bool Initialize() { return false; }

        void Activate() { _activated = true; }

        void Reset() {
            _initialized = false;
            _activated = false;
        }

        virtual void Encrypt(uint8_t *dest, const uint8_t *src, size_t len) {}
        virtual void Decrypt(uint8_t *dest, const uint8_t *src, size_t len) {}

        void Encrypt(std::vector<uint8_t> &dest, std::vector<uint8_t> src) {
            Encrypt(dest.data(), src.data(), src.size());
        }

        void Decrypt(std::vector<uint8_t> &dest, std::vector<uint8_t> src) {
            Decrypt(dest.data(), src.data(), src.size());
        }

        virtual void AddData(size_t id, void *data, size_t length) {}

        virtual const void *GetData(size_t id) { return nullptr; }

       protected:
        bool _initialized, _activated;
    };

    enum KeyAgreementDataID : size_t {
        KEY_AGREEMENT_DATA_AGREED_VALUE,
        KEY_AGREEMENT_DATA_PUBLIC_STATIC_SERVER_KEY,
        KEY_AGREEMENT_DATA_PUBLIC_EPHEMERAL_SERVER_KEY,
        KEY_AGREEMENT_DATA_PUBLIC_STATIC_CLIENT_KEY,
        KEY_AGREEMENT_DATA_PUBLIC_EPHEMERAL_CLIENT_KEY,
        KEY_AGREEMENT_DATA_INVERT_CIPHER,
    };

    class KeyAgreementCryptation : public CryptationBase {
       public:
        KeyAgreementCryptation();
        virtual ~KeyAgreementCryptation();

        bool Finalize();
        bool Initialize();

        void Encrypt(uint8_t *dest, const uint8_t *src, size_t len);
        void Decrypt(uint8_t *dest, const uint8_t *src, size_t len);

        void AddData(size_t id, void *data, size_t length);
        const void *GetData(size_t id);

       private:
        CryptoPP::DH2 _udh;
        CryptoPP::DH _dh;
        CryptoPP::SecByteBlock _shared, _staticServerPrivKey,
            _staticServerPubKey, _ephemeralServerPrivKey,
            _ephemeralServerPubKey, _staticClientPubKey, _ephemeralClientPubKey;

        uint32_t _clientAgreedValueLength, _serverAgreedValueLength;

        CryptoPP::BlockCipher *DecideCipher(uint8_t selection);

        CryptoPP::CTR_Mode_ExternalCipher::Encryption _encrypt;
        CryptoPP::CTR_Mode_ExternalCipher::Decryption _decrypt;

        CryptoPP::BlockCipher *_cipher1, *_cipher2;

        bool _InvertCiphers;
    };

    enum XTEACryptationDataID : size_t {
        XTEA_CRYPTATION_START_KEY,
        XTEA_CRYPTATION_LOGIN_DECRYPT_KEY,
    };

    class XTEACryptation : public CryptationBase {
       public:
        XTEACryptation();
        virtual ~XTEACryptation();

        bool Finalize() { return true; }
        bool Initialize() { return true; }

        void Encrypt(uint8_t *dest, const uint8_t *src, size_t len);
        void Decrypt(uint8_t *dest, const uint8_t *src, size_t len);

        void AddData(size_t id, void *data, size_t length);
        const void *GetData(size_t id);

       private:
        void GenerateSpecialKey(std::vector<uint32_t> &key);
        void ComputeCryptationKey();

        uint32_t _cryptKey[4];
        uint32_t _decryptKey[4];
    };
}  // namespace core::networking