#include "connection.hpp"

#include <boost/bind.hpp>
#include <boost/exception/diagnostic_information.hpp>
#include <boost/format.hpp>
#include <chrono>
#include <thread>
#include <utility>
#include <vector>

#include "../application.hpp"
#include "../profiler.hpp"
#include "packet.hpp"
#include "packet_manager.hpp"
#include "reserved_headers.hpp"
#include "server.hpp"

namespace core::networking {
    Connection::Connection(boost::asio::io_context &context,
                           std::shared_ptr<Server> server,
                           uint8_t securityLevel, uint32_t *defaultKey)
        : _socket(context),
          _buffer(),
          _server(std::move(server)),
          _securityLevel(securityLevel),
          _handshaking(false),
          _handshake(1337),
          _phase(0),
          _id(0) {
        if (securityLevel == SECURITY_LEVEL_KEY_AGREEMENT) {
            _cryptation = std::make_unique<KeyAgreementCryptation>();
        } else if (securityLevel == SECURITY_LEVEL_XTEA) {
            _cryptation = std::make_unique<XTEACryptation>();
            _cryptation->AddData(XTEA_CRYPTATION_START_KEY, defaultKey, 16);
        }
    }

    Connection::~Connection() {
        CORE_LOGGING(trace) << "Destroying connection object";
    }

    void Connection::Start() {
        CORE_LOGGING(debug) << "New connection from "
                            << _socket.remote_endpoint().address().to_string();
        ReadNextPacket();

        if (_securityLevel >= SECURITY_LEVEL_NONE) {
            StartHandshake();
        } else {
            _server->CallNewConnectionHandler(
                shared_from_this());  // Skip handshake and switch to the next
                                      // phase
        }
    }

    void Connection::Close() {
        try {
            if (_socket.is_open()) {
                CORE_LOGGING(debug)
                    << "Closing connection to "
                    << _socket.remote_endpoint().address().to_string();
                _socket.close();
            }

            if (_id > 0) {
                _server->CallDisconnectHandler(shared_from_this());
                _server->RemoveConnection(_id);
                _id = 0;
            }
        } catch (...) {
            CORE_LOGGING(info) << "Failed to close connection";
        }
    }

    void Connection::Send(std::shared_ptr<Packet> packet) {
        CORE_LOGGING(trace) << "Sending packet " << packet->GetName() << " ("
                            << (uint32_t)packet->GetHeader() << ")";

        // todo: find solution without allocating and deallocating memory
        // constantly
        auto buffer = new std::vector<uint8_t>();
        auto data = packet->GetData();

        if (_cryptation && _cryptation->IsReady()) {
            buffer->reserve(data.size() + 1);
            buffer->resize(data.size() + 1);
            uint8_t header = packet->GetHeader();
            _cryptation->Encrypt(buffer->data(), &header, 1);
            _cryptation->Encrypt(buffer->data() + 1, data.data(), data.size());
        } else {
            buffer->push_back(packet->GetHeader());
            std::copy(data.begin(), data.end(), std::back_inserter(*buffer));
        }

        _socket.async_write_some(
            boost::asio::buffer(*buffer),
            boost::bind(&Connection::HandleWrite, shared_from_this(), buffer,
                        boost::asio::placeholders::error,
                        boost::asio::placeholders::bytes_transferred));
    }

    void Connection::SendAsReply(std::shared_ptr<Packet> request,
                                 std::shared_ptr<Packet> reply) {
        auto id = request->GetField<uint64_t>("__REFERENCE_ID");

        reply->SetField<uint64_t>("__REFERENCE_ID", id);
        reply->SetField<uint8_t>("__REFERENCE_TYPE", 1);

        Send(reply);
    }

    void Connection::SetPhase(uint8_t phase) {
        CORE_LOGGING(trace)
            << "Set phase to " << boost::format("0x%02x") % (int)phase;

        auto packet = _server->GetPacketManager()->CreatePacket(
            ReservedOutgoingHeaders::HEADER_OUT_PHASE, Direction::Outgoing);
        packet->SetField<uint8_t>("phase", phase);

        _phase = phase;

        Send(packet);
    }

    bool Connection::HasProperty(const std::string &property) const {
        return _properties.find(property) != _properties.end();
    }

    void Connection::SetProperty(const std::string &property,
                                 ConnectionProperty value) {
        _properties[property] = std::move(value);
    }

    ConnectionProperty Connection::GetProperty(
        const std::string &property) const {
        return _properties.at(property);
    }

    void Connection::HandleHandshake(std::shared_ptr<Packet> packet) {
        auto handshake = packet->GetField<uint32_t>("handshake");
        auto time = packet->GetField<uint32_t>("time");
        auto delta = packet->GetField<uint32_t>("delta");

        if (handshake != _handshake) {
            CORE_LOGGING(warning)
                << "Closing connection because of handshake mismatch";
            Close();
            return;
        }

        auto currentTime = _server->GetApplication()->GetCoreTime();

        auto diff = currentTime - (time + delta);
        if (diff >= 0 && diff <= 50) {
            CORE_LOGGING(trace) << "Time sync done, handshake done";

            if (_securityLevel >= SECURITY_LEVEL_KEY_AGREEMENT) {
                StartKeyAgreement();
            } else {
                _handshaking = false;
                _server->CallNewConnectionHandler(shared_from_this());

                if (_securityLevel == SECURITY_LEVEL_XTEA) {
                    // We need to make sure that XTEA cryptation is enabled
                    // AFTER the Phase is changed (aka NewConnection handler)
                    //  otherwise the client will not be able to read the data
                    //  correctly NOTE: this needs to be verified!
                    using namespace std::chrono_literals;
                    std::this_thread::sleep_for(
                        100ms);  // todo: this is currently a dirty trick
                                 // because we have no main thread to update the
                                 // server

                    CORE_LOGGING(trace) << "(XTEA) Cryptation enabled";
                    if (!_cryptation->Finalize()) {
                        Close();
                    }

                    _cryptation->Activate();
                }
            }
            return;
        }

        auto newDelta = (currentTime - time) / 2;
        if (newDelta < 0) {
            CORE_LOGGING(error) << "Failed to sync time";
            Close();
            return;
        }

        // todo max retries?
        SendHandshake(currentTime, newDelta);
    }

    std::shared_ptr<Server> Connection::GetServer() { return _server; }

    void Connection::StartHandshake() {
        _handshaking = true;
        SetPhase(Phases::PHASE_HANDSHAKE);
        SendHandshake(_server->GetApplication()->GetCoreTime(), 0);
    }

    void Connection::SendHandshake(uint32_t time, uint32_t delta) {
        auto packet = _server->GetPacketManager()->CreatePacket(
            ReservedOutgoingHeaders::HEADER_OUT_HANDSHAKE, Direction::Outgoing);
        packet->SetField<uint32_t>("handshake", _handshake);
        packet->SetField<uint32_t>("time", time);
        packet->SetField<uint32_t>("delta", 0);

        Send(packet);
    }

    void Connection::ReadNextPacket() {
        CORE_LOGGING(trace) << "Reading next packet";
        boost::asio::async_read(
            _socket, _buffer.prepare(1), boost::asio::transfer_exactly(1),
            boost::bind(&Connection::HandleReadHeader, shared_from_this(),
                        boost::asio::placeholders::error,
                        boost::asio::placeholders::bytes_transferred));
    }

    void Connection::HandleReadHeader(const boost::system::error_code &err,
                                      std::size_t transferred) {
        if (!err) {
            CORE_LOGGING(trace)
                << "(HEADER) Received " << transferred << " bytes";

            uint8_t header;

            _buffer.commit(transferred);
            std::istream stream(&_buffer);
            char buf[1];
            stream.read(buf, 1);

            if (_cryptation && _cryptation->IsReady()) {
                _cryptation->Decrypt(
                    &header, reinterpret_cast<const uint8_t *>(&buf[0]), 1);
            } else {
                header = buf[0];
            }

            _buffer.consume(1);

            CORE_LOGGING(trace)
                << "Received header " << boost::format("0x%02x") % (int)header;

            auto packet =
                _server->GetPacketManager()->CreatePacket(header, Incoming);
            if (!packet) {
                CORE_LOGGING(warning) << "Received unknown header "
                                      << boost::format("0x%02x") % (int)header;
                Close();
                return;
            }

            // Check if packet is dynamic size
            if (packet->IsDynamicSized()) {
                CORE_LOGGING(trace) << "Reading dynamic packet size";

                // We have to read our packet size first
                boost::asio::async_read(
                    _socket, _buffer.prepare(2),
                    boost::asio::transfer_exactly(2),
                    boost::bind(&Connection::HandleReadSize, shared_from_this(),
                                packet, boost::asio::placeholders::error,
                                boost::asio::placeholders::bytes_transferred));

                return;
            }

            auto sizeToRead = packet->GetSize();
            if (packet->HasSequence()) sizeToRead += 1;
            CORE_LOGGING(trace)
                << "Reading the next " << sizeToRead << " bytes";

            boost::asio::async_read(
                _socket, _buffer.prepare(sizeToRead),
                boost::asio::transfer_exactly(sizeToRead),
                boost::bind(&Connection::HandleReadData, shared_from_this(),
                            packet, boost::asio::placeholders::error,
                            boost::asio::placeholders::bytes_transferred));
        } else {
            if (err == boost::asio::error::eof) {
                CORE_LOGGING(debug)
                    << "Connection closed by remote "
                    << _socket.remote_endpoint().address().to_string();
                Close();
                return;
            }
            CORE_LOGGING(warning) << "Failed to read " << err.message();
            Close();
        }
    }

    void Connection::HandleReadSize(std::shared_ptr<Packet> packet,
                                    const boost::system::error_code &err,
                                    std::size_t transferred) {
        assert(transferred == 2);
        CORE_LOGGING(trace) << "(SIZE) Received " << transferred << " bytes";
        _buffer.commit(transferred);
        unsigned short size;

        if (_cryptation && _cryptation->IsReady()) {
            std::vector<uint8_t> buf(transferred);

            _cryptation->Decrypt(
                buf.data(),
                reinterpret_cast<const uint8_t *>(_buffer.data().data()),
                transferred);
            _buffer.consume(transferred);

            size = (buf[1] << 8) | buf[0];
        } else {
            boost::asio::buffer_copy(
                boost::asio::buffer(static_cast<void *>(&size), 2),
                _buffer.data());  // todo: clean solution
        }

        CORE_LOGGING(trace) << "Size of packet is " << size;

        auto sizeToRead =
            size - 1 - 2;  // already read header (1byte) and size (2bytes)
        if (packet->HasSequence()) {
            sizeToRead += 1;
        }

        CORE_LOGGING(trace) << "Reading the next " << sizeToRead << " bytes";
        boost::asio::async_read(
            _socket, _buffer.prepare(sizeToRead),
            boost::asio::transfer_exactly(sizeToRead),
            boost::bind(&Connection::HandleReadData, shared_from_this(), packet,
                        boost::asio::placeholders::error,
                        boost::asio::placeholders::bytes_transferred));
    }

    void Connection::HandleReadData(std::shared_ptr<Packet> packet,
                                    const boost::system::error_code &err,
                                    std::size_t transferred) {
        CORE_LOGGING(trace) << "(DATA) Received " << transferred << " bytes";
        _buffer.commit(transferred);

        if (_cryptation && _cryptation->IsReady()) {
            std::vector<uint8_t> buf(transferred);

            _cryptation->Decrypt(
                buf.data(),
                reinterpret_cast<const uint8_t *>(_buffer.data().data()),
                transferred);
            _buffer.consume(transferred);

            packet->CopyData(buf, packet->IsDynamicSized() ? 2 : 0);
        } else {
            packet->CopyData(_buffer);
        }

#ifdef NDEBUG
        try {
#endif
            {
                PROFILE_SCOPE("Handler for " + packet->GetName());
                _server->CallPacketHandler(shared_from_this(), packet);
            }
#ifdef NDEBUG
        } catch (...) {
            CORE_LOGGING(fatal)
                << boost::current_exception_diagnostic_information();
        }
#endif

        ReadNextPacket();
    }

    void Connection::HandleWrite(std::vector<uint8_t> *buffer,
                                 const boost::system::error_code &err,
                                 std::size_t transferred) {
        delete buffer;
        if (err) {
            CORE_LOGGING(warning)
                << "Failed to send data to "
                << _socket.remote_endpoint().address().to_string() << ": "
                << err.message();
        }
    }

    boost::asio::ip::tcp::socket &Connection::GetSocket() { return _socket; }

    std::shared_ptr<Connection> Connection::Create(
        boost::asio::io_context &context, std::shared_ptr<Server> server,
        uint8_t securityLevel, uint32_t *defaultXTEAKeys) {
        return std::make_shared<Connection>(context, server, securityLevel,
                                            defaultXTEAKeys);
    }

    void Connection::StartKeyAgreement() {
        CORE_LOGGING(trace) << "(KEYS) Agreement start!";

        if (!_cryptation->Initialize()) {
            CORE_LOGGING(trace) << "(KEYS) Cannot initialize!";
            Close();
            return;
        }

        const CryptoPP::SecByteBlock *staticKey =
            (const CryptoPP::SecByteBlock *)(_cryptation->GetData(
                KEY_AGREEMENT_DATA_PUBLIC_STATIC_SERVER_KEY));
        const CryptoPP::SecByteBlock *ephemeralKey =
            (const CryptoPP::SecByteBlock *)(_cryptation->GetData(
                KEY_AGREEMENT_DATA_PUBLIC_EPHEMERAL_SERVER_KEY));

        const uint32_t *agreedValue = (const uint32_t *)_cryptation->GetData(
            KEY_AGREEMENT_DATA_AGREED_VALUE);
        if (*agreedValue < 1 || staticKey->size() < 1 ||
            ephemeralKey->size() < 1 ||
            (ephemeralKey->size() + staticKey->size() > 256)) {
            CORE_LOGGING(trace) << "(KEYS) Invalid values!";
            Close();
            return;
        }

        char keys[256];
        memset(keys, 0, sizeof(keys));
        memcpy(keys, staticKey->data(), staticKey->size());
        memcpy(keys + staticKey->size(), ephemeralKey->data(),
               ephemeralKey->size());

        auto packet = _server->GetPacketManager()->CreatePacket(
            ReservedOutgoingHeaders::HEADER_OUT_KEY_AGREEMENT,
            Direction::Outgoing);
        packet->SetField<uint16_t>("valuelen",
                                   static_cast<uint16_t>(*agreedValue));
        packet->SetField<uint16_t>(
            "datalen",
            static_cast<uint16_t>(ephemeralKey->size() + staticKey->size()));
        packet->SetField("data", (uint8_t *)keys, sizeof(keys));

        Send(packet);
    }

    void Connection::HandleKeyAgreement(std::shared_ptr<Packet> packet) {
        PROFILE_FUNCTION();

        if (!_cryptation->IsInitialized()) {
            CORE_LOGGING(trace) << "(KEYS) Connection sent handle keys without "
                                   "letting the server initialize them";
            Close();
            return;
        }

        uint8_t *data = (uint8_t *)packet->GetField("data");

        {
            PROFILE_SCOPE("Generate keys");
            const CryptoPP::SecByteBlock *staticKey =
                (const CryptoPP::SecByteBlock *)_cryptation->GetData(
                    KEY_AGREEMENT_DATA_PUBLIC_STATIC_SERVER_KEY);
            const CryptoPP::SecByteBlock *ephemeralKey =
                (const CryptoPP::SecByteBlock *)_cryptation->GetData(
                    KEY_AGREEMENT_DATA_PUBLIC_EPHEMERAL_SERVER_KEY);

            uint16_t agreedValue = packet->GetField<uint16_t>("valuelen");
            _cryptation->AddData(KEY_AGREEMENT_DATA_AGREED_VALUE, &agreedValue,
                                 sizeof(agreedValue));

            _cryptation->AddData(KEY_AGREEMENT_DATA_PUBLIC_STATIC_CLIENT_KEY,
                                 data, staticKey->size());
            _cryptation->AddData(KEY_AGREEMENT_DATA_PUBLIC_EPHEMERAL_CLIENT_KEY,
                                 data + staticKey->size(),
                                 ephemeralKey->size());
        }

        if (!_cryptation->Finalize()) {
            CORE_LOGGING(trace) << "(KEYS) Cannot agree";
            Close();
            return;
        }

        Send(_server->GetPacketManager()->CreatePacket(
            ReservedOutgoingHeaders::HEADER_OUT_KEY_AGREEMENT_COMPLETED,
            Direction::Outgoing));

        CORE_LOGGING(trace) << "(KEYS) Agreement completed!";
        _handshaking = false;

        _cryptation->Activate();

        // We have to wait here before we are calling the new connection
        // handler, this is because the client is not able to handle a packet in
        // the same tcp packet after the key agreement is complete.
        using namespace std::chrono_literals;
        std::this_thread::sleep_for(
            100ms);  // todo: this is currently a dirty trick because we have no
                     // main thread to update the server
        _server->CallNewConnectionHandler(shared_from_this());
    }

    void Connection::ChangeXTEAKey(uint32_t *key) {
        _cryptation->AddData(XTEA_CRYPTATION_LOGIN_DECRYPT_KEY, key, 16);
    }
}  // namespace core::networking
