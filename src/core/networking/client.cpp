#include "client.hpp"

#include <boost/bind.hpp>
#include <boost/exception/diagnostic_information.hpp>
#include <boost/format.hpp>

#include "../logger.hpp"
#include "packet.hpp"

namespace core::networking {
    using boost::asio::ip::tcp;

    Client::Client(const std::string &hostname, unsigned short port,
                   boost::asio::io_service &service)
        : _socket(service), _buffer(), _random(std::random_device()()) {
        // Create packet manager
        _packetManager = std::make_shared<PacketManager>();

        // Resolve hostname
        tcp::resolver resolver(service);
        tcp::resolver::query q(hostname, std::to_string(port));

        tcp::resolver::iterator endpointIterator = resolver.resolve(q);
        tcp::resolver::iterator end;

        // Connect to possible endpoints
        boost::system::error_code error = boost::asio::error::host_not_found;
        while (error && endpointIterator != end) {
            auto endpoint = *endpointIterator++;
            CORE_LOGGING(info)
                << "[CLIENT] Try to connect to " << endpoint.host_name();

            _socket.close();
            _socket.connect(endpoint, error);
        }

        if (error)
            throw error;  // we failed to connect to all possible endpoints,
                          // throw exception

        CORE_LOGGING(info) << "[CLIENT] Connected to "
                           << _socket.remote_endpoint().address().to_string();
    }

    Client::~Client() {}

    std::shared_ptr<PacketManager> Client::GetPacketManager() const {
        return _packetManager;
    }

    void Client::Start() { ReadNextPacket(); }

    void Client::SendPacket(std::shared_ptr<Packet> packet) {
        auto buffer = new std::vector<uint8_t>();
        buffer->push_back(packet->GetHeader());
        auto data = packet->GetData();
        std::copy(data.begin(), data.end(), std::back_inserter(*buffer));

        CORE_LOGGING(trace) << "[CLIENT] Sending " << (int)packet->GetHeader();

        _socket.async_write_some(
            boost::asio::buffer(*buffer),
            boost::bind(&Client::HandleWrite, shared_from_this(), buffer,
                        boost::asio::placeholders::error,
                        boost::asio::placeholders::bytes_transferred));
    }

    void Client::SendPacket(std::shared_ptr<Packet> packet,
                            RequestHandler handler) {
        std::uniform_int_distribution<uint64_t> dist;
        uint64_t id;
        do {
            id = dist(_random);
        } while (_outstandingReplies.find(id) != _outstandingReplies.end());

        _outstandingReplies[id] = handler;

        CORE_LOGGING(trace) << "[CLIENT] Sending request with id " << id;

        packet->SetField<uint8_t>("__REFERENCE_TYPE", 0);
        packet->SetField<uint64_t>("__REFERENCE_ID", id);

        SendPacket(packet);
    }

    void Client::Close() {
        CORE_LOGGING(debug) << "[CLIENT] Closing connection to "
                            << _socket.remote_endpoint().address().to_string();
        _socket.close();
    }

    void Client::ReadNextPacket() {
        boost::asio::async_read(
            _socket, _buffer.prepare(1), boost::asio::transfer_exactly(1),
            boost::bind(&Client::HandleReadHeader, shared_from_this(),
                        boost::asio::placeholders::error,
                        boost::asio::placeholders::bytes_transferred));
    }

    void Client::HandleReadHeader(const boost::system::error_code &err,
                                  std::size_t transferred) {
        if (!err) {
            CORE_LOGGING(trace)
                << "[CLIENT] Header received " << transferred << " bytes";

            uint8_t header;

            _buffer.commit(transferred);
            std::istream stream(&_buffer);
            stream >> header;
            _buffer.consume(1);

            CORE_LOGGING(trace) << "[CLIENT] Received header "
                                << boost::format("0x%02x") % (int)header;

            auto packet = _packetManager->CreatePacket(header, Incoming);
            if (!packet) {
                CORE_LOGGING(warning) << "[CLIENT] Received unknown header "
                                      << boost::format("0x%02x") % (int)header;
                Close();
                return;
            }

            CORE_LOGGING(trace) << "[CLIENT] Reading the next "
                                << packet->GetSize() << " bytes";

            boost::asio::async_read(
                _socket, _buffer.prepare(packet->GetSize()),
                boost::asio::transfer_exactly(packet->GetSize()),
                boost::bind(&Client::HandleReadData, shared_from_this(), packet,
                            boost::asio::placeholders::error,
                            boost::asio::placeholders::bytes_transferred));
        } else {
            if (err == boost::asio::error::eof) {
                CORE_LOGGING(debug)
                    << "[CLIENT] Connection closed by remote "
                    << _socket.remote_endpoint().address().to_string();
                return;
            }

            CORE_LOGGING(warning)
                << "[CLIENT] Failed to read " << err.message();
            Close();
        }
    }

    void Client::HandleReadData(std::shared_ptr<Packet> packet,
                                const boost::system::error_code &err,
                                std::size_t transferred) {
        CORE_LOGGING(trace)
            << "[CLIENT] Data received " << transferred << " bytes";
        _buffer.commit(transferred);
        packet->CopyData(_buffer);

#ifndef _DEBUG
        try {
#endif
            if (packet->IsReply()) {
                auto id = packet->GetReferenceId();
                if (_outstandingReplies.find(id) != _outstandingReplies.end()) {
                    _outstandingReplies[id](packet, shared_from_this());
                    _outstandingReplies.erase(id);
                } else {
                    CORE_LOGGING(trace) << "[CLIENT] Ignoring reply for id "
                                        << id << " because it is unknown";
                }
            }

            //_server->CallPacketHandler(shared_from_this(), packet); // todo
#ifndef _DEBUG
        } catch (...) {
            CORE_LOGGING(fatal)
                << boost::current_exception_diagnostic_information();
        }
#endif

        ReadNextPacket();
    }

    void Client::HandleWrite(std::vector<uint8_t> *buffer,
                             const boost::system::error_code &err,
                             std::size_t transferred) {
        delete buffer;
        if (err) {
            CORE_LOGGING(warning)
                << "[CLIENT] Failed to send data to "
                << _socket.remote_endpoint().address().to_string() << ": "
                << err.message();
        } else {
            CORE_LOGGING(trace) << "[CLIENT] Send " << transferred << " bytes";
        }
    }
}  // namespace core::networking