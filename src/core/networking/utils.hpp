#pragma once

#include <string>

namespace core::networking {
    std::string GetLocalAddress();
    int32_t ConvertIP(const std::string &ip);
}  // namespace core::networking