#include "../logger.hpp"
#include "../utils/xtea.hpp"
#include "cryptation.hpp"
#include "utils.hpp"

namespace core::networking {

    void XTEACryptation::ComputeCryptationKey() {
        std::vector<uint32_t> randomKey;
        randomKey.resize(175);
        randomKey.reserve(175);

        GenerateSpecialKey(randomKey);

        return core::utils::XTEA::Encrypt(
            (const uint8_t *)_decryptKey, (uint8_t *)_cryptKey, 16,
            (const uint32_t *)(((char *)randomKey.data()) + 37), 32);
    }

    void XTEACryptation::GenerateSpecialKey(std::vector<uint32_t> &key) {
        unsigned int i = 0, n = 1491971513;

        do {
            n = (n ^ 0x80164043) - 702581315;
            key.push_back(n);
            i++;
        } while (i < (uint8_t)n);
    }

    XTEACryptation::XTEACryptation() {
        memset(_cryptKey, 0, sizeof(_cryptKey));
        memset(_decryptKey, 0, sizeof(_decryptKey));
    }

    XTEACryptation::~XTEACryptation() {}

    void XTEACryptation::Encrypt(uint8_t *dst, const uint8_t *src, size_t len) {
        core::utils::XTEA::Encrypt(src, dst, len, _cryptKey, 32);
    }

    void XTEACryptation::Decrypt(uint8_t *dst, const uint8_t *src, size_t len) {
        core::utils::XTEA::Decrypt(src, dst, len, _decryptKey, 32);
    }

    void XTEACryptation::AddData(size_t id, void *data, size_t length) {
        if (id == XTEA_CRYPTATION_START_KEY) {
            if (length != sizeof(_cryptKey)) {
                CORE_LOGGING(warning) << "Invalid XTEA start key";
                return;
            }

            memcpy(_cryptKey, data, length);
            memcpy(_decryptKey, data, length);
        } else if (id == XTEA_CRYPTATION_LOGIN_DECRYPT_KEY) {
            if (length != sizeof(_decryptKey)) {
                CORE_LOGGING(warning) << "Invalid login decryptation key size";
                return;
            }

            memcpy(_decryptKey, data, length);
            ComputeCryptationKey();
        }
    }

    const void *XTEACryptation::GetData(size_t) { return nullptr; }

}  // namespace core::networking