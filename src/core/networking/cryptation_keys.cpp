#include <cryptopp/dh.h>
#include <cryptopp/dh2.h>
#include <cryptopp/modes.h>
#include <cryptopp/nbtheory.h>
#include <cryptopp/osrng.h>
#include <utility>

#include "../logger.hpp"
#include "../profiler.hpp"
#include "cryptation.hpp"

// Algorithms
#include <cryptopp/blowfish.h>
#include <cryptopp/camellia.h>
#include <cryptopp/cast.h>
#include <cryptopp/des.h>
#include <cryptopp/idea.h>
#include <cryptopp/mars.h>
#include <cryptopp/rc5.h>
#include <cryptopp/rc6.h>
#include <cryptopp/seed.h>
#include <cryptopp/serpent.h>
#include <cryptopp/shacal2.h>
#include <cryptopp/tea.h>
#include <cryptopp/twofish.h>

namespace core::networking {
    KeyAgreementCryptation::KeyAgreementCryptation()
        : _udh(_dh),
          _cipher1(nullptr),
          _cipher2(nullptr),
          _clientAgreedValueLength(0),
          _serverAgreedValueLength(0),
          _InvertCiphers(false) {}

    KeyAgreementCryptation::~KeyAgreementCryptation() {
        if (_cipher1) {
            delete _cipher1;
            _cipher1 = nullptr;
        }

        if (_cipher2) {
            delete _cipher2;
            _cipher2 = nullptr;
        }
    }

    CryptoPP::BlockCipher *KeyAgreementCryptation::DecideCipher(
        uint8_t selection) {
        enum : uint8_t {
            CIPHER_RC6 = 1,
            CIPHER_MARS,
            CIPHER_TWOFISH,
            CIPHER_SERPENT,
            CIPHER_CAST256,
            CIPHER_IDEA,
            CIPHER_DESEDE2,
            CIPHER_CAMELLIA,
            CIPHER_SEED,
            CIPHER_RC5,
            CIPHER_BLOWFISH,
            CIPHER_TEA,
            CIPHER_SHACAL2,
            CIPHER_MAX
        };

        selection = selection % CIPHER_MAX;

        switch (selection) {
            default:
                break;

            case CIPHER_RC6:
                return new CryptoPP::RC6::Encryption();

            case CIPHER_MARS:
                return new CryptoPP::MARS::Encryption();

            case CIPHER_SERPENT:
                return new CryptoPP::Serpent::Encryption();

            case CIPHER_CAST256:
                return new CryptoPP::CAST256::Encryption();

            case CIPHER_IDEA:
                return new CryptoPP::IDEA::Encryption();

            case CIPHER_DESEDE2:
                return new CryptoPP::DES_EDE2::Encryption();

            case CIPHER_CAMELLIA:
                return new CryptoPP::Camellia::Encryption();

            case CIPHER_SEED:
                return new CryptoPP::SEED::Encryption();

            case CIPHER_RC5:
                return new CryptoPP::RC5::Encryption();

            case CIPHER_BLOWFISH:
                return new CryptoPP::Blowfish::Encryption();

            case CIPHER_TEA:
                return new CryptoPP::TEA::Encryption();

            case CIPHER_SHACAL2:
                return new CryptoPP::SHACAL2::Encryption();
        }

        return new CryptoPP::Twofish::Encryption();
    }

    bool KeyAgreementCryptation::Initialize() {
        PROFILE_FUNCTION();

        // RFC 5114, 1024-bit MODP Group with 160-bit Prime Order Subgroup
        // http://tools.ietf.org/html/rfc5114#section-2.1
        CryptoPP::Integer p(
            "0xB10B8F96A080E01DDE92DE5EAE5D54EC52C99FBCFB06A3C6"
            "9A6A9DCA52D23B616073E28675A23D189838EF1E2EE652C0"
            "13ECB4AEA906112324975C3CD49B83BFACCBDD7D90C4BD70"
            "98488E9C219A73724EFFD6FAE5644738FAA31A4FF55BCCC0"
            "A151AF5F0DC8B4BD45BF37DF365C1A65E68CFDA76D4DA708"
            "DF1FB2BC2E4A4371");

        CryptoPP::Integer g(
            "0xA4D1CBD5C3FD34126765A442EFB99905F8104DD258AC507F"
            "D6406CFF14266D31266FEA1E5C41564B777E690F5504F213"
            "160217B4B01B886A5E91547F9E2749F4D7FBD7D3B9A92EE1"
            "909D0D2263F80A76A6A24C087A091F531DBF0A0169B6A28A"
            "D662A4D18E73AFA32D779D5918D08BC8858F4DCEF97C2A24"
            "855E6EEB22B3B2E5");

        CryptoPP::Integer q("0xF518AA8781A8DF278ABA4E7D64B7CB9D49462353");

        CryptoPP::AutoSeededRandomPool rnd;

        // 1. Initialize and verify p, g, q
        _dh.AccessGroupParameters().Initialize(p, q, g);

        if (!_dh.GetGroupParameters().ValidateGroup(rnd, 3)) {
            return false;
        }

        CryptoPP::Integer v = ModularExponentiation(g, q, p);
        if (v != CryptoPP::Integer::One()) {
            return false;
        }

        // 2. Elevate to Unified DH
        CryptoPP::DH2 _udh(_dh);

        _shared.New(_udh.AgreedValueLength());
        _staticServerPrivKey.New(_udh.StaticPrivateKeyLength());
        _staticServerPubKey.New(_udh.StaticPublicKeyLength());
        _ephemeralServerPrivKey.New(_udh.EphemeralPrivateKeyLength());
        _ephemeralServerPubKey.New(_udh.EphemeralPublicKeyLength());

        _udh.GenerateEphemeralKeyPair(rnd, _ephemeralServerPrivKey,
                                      _ephemeralServerPubKey);
        _udh.GenerateStaticKeyPair(rnd, _staticServerPrivKey,
                                   _staticServerPubKey);

        _serverAgreedValueLength = _udh.AgreedValueLength();

        _initialized = true;
        return true;
    }

    bool KeyAgreementCryptation::Finalize() {
        PROFILE_FUNCTION();

        if (!_staticClientPubKey.size() || !_ephemeralClientPubKey.size() ||
            !_initialized) {
            return false;
        }

        if (_clientAgreedValueLength != _serverAgreedValueLength) {
            return false;
        }

        if (!_udh.Agree(_shared, _staticServerPrivKey, _ephemeralServerPrivKey,
                        _staticClientPubKey, _ephemeralClientPubKey)) {
            return false;
        }

        if (_shared.size() < 2) {
            return false;
        }

        // 1: Encrypt, 2 : Decrypt [Server], 1: Decrypt 2: Encrypt [Client]
        uint8_t cipherSelection1 = _shared.data()[0] % _shared.size(), 
            cipherSelection2 = _shared.data()[1] % _shared.size();

        _cipher1 = DecideCipher(_shared.data()[cipherSelection1]);
        _cipher2 = DecideCipher(_shared.data()[cipherSelection2]);

        size_t KeySize1 = _cipher1->DefaultKeyLength(),
               KeySize2 = _cipher2->DefaultKeyLength(),
               IVSize1 = _cipher1->BlockSize(),
               IVSize2 = _cipher2->BlockSize();

        if (_shared.size() < KeySize1 + KeySize2 + IVSize2 + IVSize1) {
            return false;
        }

        CryptoPP::SecByteBlock Key1(KeySize1), Key2(KeySize2),
            IV1(IVSize1), IV2(IVSize2);

        Key1.Assign(_shared.data(), KeySize1);
        Key2.Assign(_shared.data() + KeySize1, KeySize2);    

        size_t IVPos = _shared.size() - KeySize1;
        IV1.Assign(_shared.data() + IVPos, IVSize1);

        if (IVPos < IVSize2) {
            IVPos = 0;
        } else {
            IVPos -= IVSize2;
        }

        IV2.Assign(_shared.data() + IVPos, IVSize2);

        _cipher1->SetKey(Key1, KeySize1);
        _cipher2->SetKey(Key2, KeySize2);

        if (_InvertCiphers) {
            _decrypt.SetCipherWithIV(*_cipher1, IV1);
            _encrypt.SetCipherWithIV(*_cipher2, IV2);
        } else {
            _decrypt.SetCipherWithIV(*_cipher2, IV2);
            _encrypt.SetCipherWithIV(*_cipher1, IV1);
        }

        return true;
    }

    void KeyAgreementCryptation::Encrypt(uint8_t *dest, const uint8_t *src,
                                         size_t len) {
        if (!_activated) {
            return;
        }

        _encrypt.ProcessData(dest, src, len);
    }

    void KeyAgreementCryptation::Decrypt(uint8_t *dest, const uint8_t *src,
                                         size_t len) {
        if (!_activated) {
            return;
        }

        _decrypt.ProcessData(dest, src, len);
    }

    void KeyAgreementCryptation::AddData(size_t id, void *data, size_t length) {
        if (id == KEY_AGREEMENT_DATA_AGREED_VALUE) {
            if (length != 2) {
                CORE_LOGGING(warning) << "Key agreement length is not 2!";
                return;
            }

            _clientAgreedValueLength = *reinterpret_cast<uint16_t *>(data);
        } else if (id == KEY_AGREEMENT_DATA_PUBLIC_STATIC_CLIENT_KEY) {
            _staticClientPubKey.New(length);
            _staticClientPubKey.Assign(reinterpret_cast<uint8_t *>(data),
                                       length);
        } else if (id == KEY_AGREEMENT_DATA_PUBLIC_EPHEMERAL_CLIENT_KEY) {
            _ephemeralClientPubKey.New(length);
            _ephemeralClientPubKey.Assign(reinterpret_cast<uint8_t *>(data),
                                          length);
        } else if (id == KEY_AGREEMENT_DATA_INVERT_CIPHER) {
            if (length != 1) {
                CORE_LOGGING(warning) << "Invert shared cipher must be a bool type!";
                return;
            }

            _InvertCiphers = *reinterpret_cast<bool*>(data);
        }
    }

    const void *KeyAgreementCryptation::GetData(size_t id) {
        if (!_initialized) return nullptr;

        if (id == KEY_AGREEMENT_DATA_AGREED_VALUE) {
            return &_serverAgreedValueLength;
        } else if (id == KEY_AGREEMENT_DATA_PUBLIC_STATIC_SERVER_KEY) {
            return &_staticServerPubKey;
        } else if (id == KEY_AGREEMENT_DATA_PUBLIC_EPHEMERAL_SERVER_KEY) {
            return &_ephemeralServerPubKey;
        }

        return nullptr;
    }

}  // namespace core::networking
