#include "packet_manager.hpp"

#include <boost/format.hpp>

#include "../logger.hpp"
#include "packet.hpp"

namespace core::networking {
    PacketManager::PacketManager() {}

    PacketManager::~PacketManager() {}

    std::shared_ptr<PacketDefinition> PacketManager::RegisterPacket(
        const std::string &name, uint8_t header, bool incoming, bool outgoing) {
        if (!incoming && !outgoing) {
            CORE_LOGGING(warning)
                << "Tried to register packet " << header
                << " with incoming & outgoing traffic disabled";
            return nullptr;  // todo: throw exception
        }

        if (incoming) {
            if (_incomingPackets.find(header) != _incomingPackets.end()) {
                CORE_LOGGING(warning)
                    << "Tried to register packet " << header
                    << " for incoming which is already in use";
                return nullptr;  // todo: throw exception
            }
        }
        if (outgoing) {
            if (_outgoingPackets.find(header) != _outgoingPackets.end()) {
                CORE_LOGGING(warning)
                    << "Tried to register packet " << header
                    << " for outgoing which is already in use";
                return nullptr;  // todo: throw exception
            }
        }

        auto packet = std::make_shared<PacketDefinition>(name, header);

        CORE_LOGGING(trace) << "Registered packet " << name << " ("
                            << (boost::format("0x%02x") % (int)header) << ")";

        if (incoming) _incomingPackets[header] = packet;
        if (outgoing) _outgoingPackets[header] = packet;

        return packet;
    }

    std::shared_ptr<Packet> PacketManager::CreatePacket(uint8_t header,
                                                        Direction direction) {
        if (direction == Direction::Incoming) {
            if (_incomingPackets.find(header) != _incomingPackets.end()) {
                return std::make_shared<Packet>(_incomingPackets[header]);
            } else {
                CORE_LOGGING(warning) << "Failed to find packet "
                                      << (boost::format("0x%02x") % (int)header)
                                      << " in incoming packet types";
            }
        } else if (direction == Direction::Outgoing) {
            if (_outgoingPackets.find(header) != _outgoingPackets.end()) {
                return std::make_shared<Packet>(_outgoingPackets[header]);
            } else {
                CORE_LOGGING(warning) << "Failed to find packet "
                                      << (boost::format("0x%02x") % (int)header)
                                      << " in outgoing packet types";
            }
        }
        return nullptr;
    }
}  // namespace core::networking
