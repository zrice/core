#pragma once

#include <map>
#include <string>

#include "packet_definition.hpp"

namespace core::networking {
    class Packet;

    enum Direction { Incoming, Outgoing };

    class PacketManager {
       public:
        PacketManager();
        virtual ~PacketManager();

        std::shared_ptr<PacketDefinition> RegisterPacket(
            const std::string &name, uint8_t header, bool incoming,
            bool outgoing);

        std::shared_ptr<Packet> CreatePacket(uint8_t header,
                                             Direction direction);

       private:
        std::map<uint8_t, std::shared_ptr<PacketDefinition>> _incomingPackets;
        std::map<uint8_t, std::shared_ptr<PacketDefinition>> _outgoingPackets;
    };
}  // namespace core::networking
