#pragma once

#include <boost/asio.hpp>
#include <boost/variant.hpp>
#include <memory>

#include "../logger.hpp"
#include "cryptation.hpp"
#include "packet.hpp"

namespace game::environment {
    class Player;
}

namespace core::networking {
    class Server;

    typedef boost::variant<std::string, uint32_t, uint8_t> ConnectionProperty;

    class Connection : public std::enable_shared_from_this<Connection> {
       public:
        static std::shared_ptr<Connection> Create(
            boost::asio::io_context &context, std::shared_ptr<Server> server,
            uint8_t securityLevel, uint32_t *defaultXTEAKeys);

       public:
        Connection(boost::asio::io_context &context,
                   std::shared_ptr<Server> server, uint8_t securityLevel,
                   uint32_t *defaultKey);
        virtual ~Connection();

        void Start();
        void Close();
        void Send(std::shared_ptr<Packet> packet);
        void SendAsReply(std::shared_ptr<Packet> request,
                         std::shared_ptr<Packet> reply);

        void SetPhase(uint8_t phase);
        uint8_t GetPhase() { return _phase; }

        bool HasProperty(const std::string &property) const;
        void SetProperty(const std::string &property, ConnectionProperty value);
        ConnectionProperty GetProperty(const std::string &property) const;
        template <typename T>
        T GetProperty(const std::string &property) const {
            return boost::get<T>(GetProperty(property));
        }

        uint64_t GetId() const { return _id; }
        void SetId(uint64_t id) { _id = id; }

        std::shared_ptr<game::environment::Player> GetPlayer() const {
            return _player;
        }
        void SetPlayer(std::shared_ptr<game::environment::Player> player) {
            _player = std::move(player);
        }

        void HandleHandshake(std::shared_ptr<Packet> packet);
        void HandleKeyAgreement(std::shared_ptr<Packet> packet);

        std::shared_ptr<Server> GetServer();

        boost::asio::ip::tcp::socket &GetSocket();

        void ChangeXTEAKey(uint32_t *key);

        uint8_t GetSecurityLevel() { return _securityLevel; }

       private:
        void HandleReadHeader(const boost::system::error_code &err,
                              std::size_t transferred);
        void HandleReadSize(std::shared_ptr<Packet> packet,
                            const boost::system::error_code &err,
                            std::size_t transferred);
        void HandleReadData(std::shared_ptr<Packet> packet,
                            const boost::system::error_code &err,
                            std::size_t transferred);
        void HandleWrite(std::vector<uint8_t> *buffer,
                         const boost::system::error_code &err,
                         std::size_t transferred);
        void ReadNextPacket();

        void StartHandshake();
        void SendHandshake(uint32_t time, uint32_t delta);

        void StartKeyAgreement();

       private:
        boost::asio::ip::tcp::socket _socket;
        boost::asio::streambuf _buffer;
        std::shared_ptr<Server> _server;
        uint64_t _id;

        uint8_t _securityLevel;
        std::shared_ptr<game::environment::Player> _player;
        std::map<std::string, ConnectionProperty> _properties;

        bool _handshaking;
        uint32_t _handshake;

        uint8_t _phase;

        std::unique_ptr<CryptationBase> _cryptation;
    };
}  // namespace core::networking
