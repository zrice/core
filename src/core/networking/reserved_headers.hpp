#pragma once

namespace core::networking {
    enum ReservedIncomingHeaders : uint8_t {
        HEADER_INC_HANDSHAKE = 0xff,
        HEADER_INC_KEY_AGREEMENT = 0xfb,
    };

    enum ReservedOutgoingHeaders : uint8_t {
        HEADER_OUT_HANDSHAKE = 0xff,
        HEADER_OUT_PHASE = 0xfd,
        HEADER_OUT_KEY_AGREEMENT = 0xfb,
        HEADER_OUT_KEY_AGREEMENT_COMPLETED = 0xfa,
    };

    enum Phases : uint8_t {
        PHASE_HANDSHAKE = 1,
        PHASE_LOGIN = 2,
        PHASE_SELECT = 3,
        PHASE_LOADING = 4,
        PHASE_GAME = 5,
        PHASE_AUTH = 10,
    };

    enum SecurityLevel : uint8_t {
        SECURITY_LEVEL_NONE = 0,  // No handshake
        SECURITY_LEVEL_BASIC =
            1,                    // Basic handshake (No keys or Diffie-Hellman)
        SECURITY_LEVEL_XTEA = 2,  // Key/Pong keys
        SECURITY_LEVEL_KEY_AGREEMENT = 3,  // Diffie-Hellman Key agreement
    };
}  // namespace core::networking
