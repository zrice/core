#pragma once

#include <boost/asio.hpp>

#include "packet_definition.hpp"

namespace core::networking {
    class Packet {
       public:
        explicit Packet(std::shared_ptr<PacketDefinition> definition);
        virtual ~Packet();

        void CopyData(std::vector<uint8_t> &data, unsigned int offset = 0);
        void CopyData(boost::asio::streambuf &buffer);
        std::vector<uint8_t> GetData();
        unsigned int GetSize();
        uint8_t GetHeader();
        const std::string &GetName() const { return _definition->GetName(); }

        void *GetField(std::string name);

        template <typename T>
        T GetField(std::string name) {
            return *reinterpret_cast<T *>(GetField(name));
        }

        std::string GetString(std::string name);

        std::shared_ptr<Packet> GetRepeatedSubField(std::string name,
                                                    unsigned int index);
        std::shared_ptr<Packet> GetSubField(std::string name);

        void SetRepeatedField(std::string name, unsigned int index,
                              uint8_t *value, unsigned int length);
        void SetField(std::string name, uint8_t *value, unsigned int length);

        void SetString(std::string name, const char *value,
                       unsigned int length);
        void SetString(std::string name, std::string value);

        template <typename T>
        void SetField(std::string name, T value) {
            SetField(name, reinterpret_cast<uint8_t *>(&value), sizeof(T));
        }

        template <typename T>
        void SetRepeatedField(std::string name, unsigned int index, T value) {
            SetRepeatedField(name, index, reinterpret_cast<uint8_t *>(&value),
                             sizeof(T));
        }

        void SetDynamicString(const std::string &str);
        std::string GetDynamicString();
        void SetDynamicData(std::vector<uint8_t> data);

        bool IsReply();
        bool IsRequest();
        bool IsDynamicSized();
        bool HasSequence();
        uint64_t GetReferenceId();

       private:
        void InitializeSubPackets();
        void SyncInternalBuffer();

       private:
        std::shared_ptr<PacketDefinition> _definition;
        std::map<std::string, std::vector<std::shared_ptr<Packet>>> _subPackets;

        std::vector<uint8_t> _data;
        std::vector<uint8_t> _dynamicData;
        uint8_t _sequence;
    };
}  // namespace core::networking
