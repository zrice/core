#include "scripting_context.hpp"

#include <utility>

namespace core::scripting {
    ScriptingContextAbstract::ScriptingContextAbstract(
        std::shared_ptr<ScriptingEngineAbstract> engine)
        : _engine(std::move(engine)) {}

    ScriptingContextAbstract::~ScriptingContextAbstract() = default;
}  // namespace core::scripting
