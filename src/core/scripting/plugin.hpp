#pragma once

#include <boost/filesystem.hpp>
#include <boost/property_tree/ptree.hpp>

namespace core::scripting {
    class Scripting;

    class ScriptingEngineAbstract;

    class ScriptingContextAbstract;

    class Plugin : public std::enable_shared_from_this<Plugin> {
       public:
        Plugin(std::shared_ptr<Scripting> scripting,
               boost::filesystem::path folder);
        virtual ~Plugin();

        bool IsValid();

        void Load();

        const std::string &GetName();

       private:
        std::shared_ptr<Scripting> _scripting;
        boost::filesystem::path _basePath;
        boost::property_tree::ptree _metaData;

        std::shared_ptr<ScriptingEngineAbstract> _scriptEngine;
        std::shared_ptr<ScriptingContextAbstract> _context;

        std::string _mainFile;
        std::string _name;

        bool _valid;
    };
}  // namespace core::scripting
