#include "angelscript.hpp"

#ifdef ENABLE_ANGELSCRIPT

#include <boost/format.hpp>

#include "../../logger.hpp"
#include "context.hpp"
#include "proxy/mysql.hpp"
#include "script_std_string.hpp"
#include "scriptarray.hpp"

namespace core::scripting::angelscript {
    AngelScript::AngelScript()
        : ScriptingEngineAbstract("AngelScript"), _contextPool(), _poolMutex() {
        _engine = asCreateScriptEngine();

        if (_engine) {
            CORE_LOGGING(info)
                << "Loaded AngelScript v" << asGetLibraryVersion();

            // Register error callback
            _engine->SetMessageCallback(asMETHOD(AngelScript, MessageCallback),
                                        this, asCALL_THISCALL);

            // Enable string using std::string
            RegisterStdString(_engine);

            // Register array support
            RegisterScriptArray(_engine, true);

            // Register application interface
            RegisterApplicationInterface();
        } else {
            CORE_LOGGING(warning) << "Failed to load AngelScript";
        }
    }

    AngelScript::~AngelScript() {
        if (_engine) {
            _engine->ShutDownAndRelease();

            CORE_LOGGING(info) << "AngelScript shutdown";
        }
    }

    std::shared_ptr<ScriptBuilder> AngelScript::CreateScriptBuilder() {
        return std::make_shared<ScriptBuilder>();
    }

    asIScriptEngine *AngelScript::GetEngine() { return _engine; }

    std::string AngelScript::GetDefaultMainFile() { return "main.as"; }

    std::shared_ptr<scripting::ScriptingContextAbstract>
    AngelScript::CreatePluginContext(std::shared_ptr<Plugin> plugin) {
        auto context =
            std::make_shared<ScriptingContext>(shared_from_this(), plugin);

        return context;
    }

    asIScriptContext *AngelScript::GetContext() {
        std::lock_guard<std::mutex> lock(_poolMutex);

        asIScriptContext *ctx = nullptr;
        if (_contextPool.size()) {
            ctx = *_contextPool.rbegin();
            _contextPool.pop_back();
        } else {
            ctx = _engine->CreateContext();
        }

        return ctx;
    }

    void AngelScript::ReturnContext(asIScriptContext *ctx) {
        std::lock_guard<std::mutex> lock(_poolMutex);

        _contextPool.push_back(ctx);
        ctx->Unprepare();
    }

    void print(std::string &msg) { CORE_LOGGING(trace) << msg; }

    void info(std::string &msg) { CORE_LOGGING(info) << msg; }

    void debug(std::string &msg) { CORE_LOGGING(debug) << msg; }

    void error(std::string &msg) { CORE_LOGGING(error) << msg; }

    proxy::MySQL *test() { return new proxy::MySQL(nullptr); }

#define REGISTER_REF_CLASS(name)                                              \
    r = r = _engine->RegisterObjectType(#name, 0, asOBJ_REF);                 \
    assert(r >= 0);                                                           \
    r = _engine->RegisterObjectBehaviour(#name, asBEHAVE_ADDREF, "void f()",  \
                                         asMETHOD(proxy::name, AddRef),       \
                                         asCALL_THISCALL);                    \
    assert(r >= 0);                                                           \
    r = _engine->RegisterObjectBehaviour(#name, asBEHAVE_RELEASE, "void f()", \
                                         asMETHOD(proxy::name, ReleaseRef),   \
                                         asCALL_THISCALL);                    \
    assert(r >= 0)

#define GETTER_DECL(astype, methodname, ctype)                                \
    r = _engine->RegisterObjectMethod(                                        \
        "ResultSet", #astype " Get" #methodname "(const string& in) const",   \
        asMETHODPR(proxy::ResultSet, Get##methodname, (const std::string &),  \
                   ctype),                                                    \
        asCALL_THISCALL);                                                     \
    assert(r >= 0);                                                           \
    r = _engine->RegisterObjectMethod(                                        \
        "ResultSet", #astype " Get" #methodname "(uint32) const",             \
        asMETHODPR(proxy::ResultSet, Get##methodname, (unsigned int), ctype), \
        asCALL_THISCALL);                                                     \
    assert(r >= 0)

    void AngelScript::RegisterApplicationInterface() {
        int r;

        // Logging
        r = _engine->RegisterGlobalFunction("void Print(const string& in)",
                                            asFUNCTION(print), asCALL_CDECL);
        assert(r >= 0);
        r = _engine->RegisterGlobalFunction("void Info(const string& in)",
                                            asFUNCTION(info), asCALL_CDECL);
        assert(r >= 0);
        r = _engine->RegisterGlobalFunction("void Debug(const string& in)",
                                            asFUNCTION(debug), asCALL_CDECL);
        assert(r >= 0);
        r = _engine->RegisterGlobalFunction("void Error(const string& in)",
                                            asFUNCTION(error), asCALL_CDECL);
        assert(r >= 0);

        r = _engine->SetDefaultNamespace("core");
        assert(r >= 0);
        // Database
        REGISTER_REF_CLASS(MySQL);
        REGISTER_REF_CLASS(Statement);
        REGISTER_REF_CLASS(ResultSet);
        r = _engine->RegisterObjectMethod(
            "MySQL", "Statement@ CreateStatement(const string& in)",
            asMETHOD(proxy::MySQL, CreateStatement), asCALL_THISCALL);
        assert(r >= 0);
        r = _engine->RegisterObjectMethod("Statement", "ResultSet@ Execute()",
                                          asMETHOD(proxy::Statement, Execute),
                                          asCALL_THISCALL);
        assert(r >= 0);
        r = _engine->RegisterObjectMethod(
            "ResultSet", "uint GetColumnCount() const",
            asMETHOD(proxy::ResultSet, GetColumnCount), asCALL_THISCALL);
        assert(r >= 0);
        r = _engine->RegisterObjectMethod(
            "ResultSet", "uint GetColumnIndex(const string& in) const",
            asMETHOD(proxy::ResultSet, GetColumnIndex), asCALL_THISCALL);
        assert(r >= 0);
        r = _engine->RegisterObjectMethod(
            "ResultSet", "uint64 GetColumnSize(uint) const",
            asMETHOD(proxy::ResultSet, GetColumnSize), asCALL_THISCALL);
        assert(r >= 0);
        r = _engine->RegisterObjectMethod(
            "ResultSet", "uint64 GetRowCount() const",
            asMETHOD(proxy::ResultSet, GetRowCount), asCALL_THISCALL);
        assert(r >= 0);
        r = _engine->RegisterObjectMethod(
            "ResultSet", "uint64 GetRowIndex() const",
            asMETHOD(proxy::ResultSet, GetRowIndex), asCALL_THISCALL);
        assert(r >= 0);
        r = _engine->RegisterObjectMethod("ResultSet", "bool Next()",
                                          asMETHOD(proxy::ResultSet, Next),
                                          asCALL_THISCALL);
        assert(r >= 0);
        GETTER_DECL(string, String, std::string);
        GETTER_DECL(int8, Int8, int8_t);
        GETTER_DECL(uint8, Uint8, uint8_t);
        GETTER_DECL(int16, Int16, int16_t);
        GETTER_DECL(uint16, Uint16, uint16_t);
        GETTER_DECL(int32, Int32, int32_t);
        GETTER_DECL(uint32, Uint32, uint32_t);
        GETTER_DECL(int64, Int64, int64_t);
        GETTER_DECL(uint64, Uint64, uint64_t);
    }

    void AngelScript::MessageCallback(const asSMessageInfo *msg, void *param) {
        switch (msg->type) {
            case asMSGTYPE_ERROR:
                CORE_LOGGING(error)
                    << boost::format("AngelScript: %s (%d, %d) : %s") %
                           msg->section % msg->row % msg->col % msg->message;
                break;
            case asMSGTYPE_WARNING:
                CORE_LOGGING(warning)
                    << boost::format("AngelScript: %s (%d, %d) : %s") %
                           msg->section % msg->row % msg->col % msg->message;
                break;
            case asMSGTYPE_INFORMATION:
                CORE_LOGGING(info)
                    << boost::format("AngelScript: %s (%d, %d) : %s") %
                           msg->section % msg->row % msg->col % msg->message;
                break;
        }
    }
}  // namespace core::scripting::angelscript

#endif
