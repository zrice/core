#pragma once

#include "../../../logger.hpp"

namespace core::scripting::angelscript::proxy {
    class Ref {
       public:
        Ref() : _refCount(1) {}

        void AddRef() { _refCount++; }

        void ReleaseRef() {
            _refCount--;
            if (_refCount <= 0) {
                delete this;
            }
        }

       private:
        int _refCount;
    };
}  // namespace core::scripting::angelscript::proxy