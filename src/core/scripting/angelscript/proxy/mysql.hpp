#pragma once

#ifdef ENABLE_ANGELSCRIPT

#include <memory>
#include <utility>

#include "../../../mysql/mysql.hpp"
#include "ref.hpp"

#define PROXY_GETTER_DECL(ctype, name, target)   \
    ctype Get##name(const std::string &column) { \
        return _ptr->Get##target(column);        \
    }                                            \
    ctype Get##name(unsigned int index) { return _ptr->Get##target(index); }

namespace core::scripting::angelscript::proxy {
    class ResultSet : public Ref {
       public:
        explicit ResultSet(std::shared_ptr<core::mysql::ResultSet> &&result)
            : _ptr(std::move(result)) {}

        [[nodiscard]] unsigned int GetColumnCount() const {
            return _ptr->GetColumnCount();
        }

        [[nodiscard]] unsigned int GetColumnIndex(
            const std::string &name) const {
            return _ptr->GetColumnIndex(name);
        }

        [[nodiscard]] unsigned long GetColumnSize(unsigned int index) const {
            return _ptr->GetColumnSize(index);
        }

        [[nodiscard]] unsigned long GetRowCount() const {
            return _ptr->GetRowCount();
        }

        [[nodiscard]] unsigned long GetRowIndex() const {
            return _ptr->GetRowIndex();
        }

        bool Next() { return _ptr->Next(); }
        bool SetRowIndex(unsigned int index) {
            return _ptr->SetRowIndex(index);
        }

        PROXY_GETTER_DECL(std::string, String, String)
        PROXY_GETTER_DECL(int8_t, Int8, Signed8)
        PROXY_GETTER_DECL(uint8_t, Uint8, Unsigned8)
        PROXY_GETTER_DECL(int16_t, Int16, Signed16)
        PROXY_GETTER_DECL(uint16_t, Uint16, Unsigned16)
        PROXY_GETTER_DECL(int32_t, Int32, Signed32)
        PROXY_GETTER_DECL(uint32_t, Uint32, Unsigned32)
        PROXY_GETTER_DECL(int64_t, Int64, Signed64)
        PROXY_GETTER_DECL(uint64_t, Uint64, Unsigned64)

       private:
        std::shared_ptr<core::mysql::ResultSet> _ptr;
    };

    class Statement : public Ref {
       public:
        explicit Statement(core::mysql::Statement &&statement)
            : _ptr(std::move(statement)) {}

        ResultSet *Execute() { return new ResultSet(_ptr()); }

       private:
        core::mysql::Statement _ptr;
    };

    class MySQL : public Ref {
       public:
        explicit MySQL(std::shared_ptr<core::mysql::MySQL> &&mysql)
            : _ptr(std::move(mysql)) {}

        Statement *CreateStatement(std::string &statement) {
            return new Statement(_ptr->CreateStatement(statement));
        }

       private:
        std::shared_ptr<core::mysql::MySQL> _ptr;
    };
}  // namespace core::scripting::angelscript::proxy

#endif