#pragma once

#ifdef ENABLE_ANGELSCRIPT

#include "../plugin.hpp"
#include "../scripting_context.hpp"
#include "angelscript.hpp"
#include "script_builder.hpp"

namespace core::scripting::angelscript {
    class ScriptingContext : public scripting::ScriptingContextAbstract {
       public:
        ScriptingContext(std::shared_ptr<AngelScript> engine,
                         std::shared_ptr<Plugin> plugin);
        ~ScriptingContext() override;

        bool Load(boost::filesystem::path file) override;
        bool CallEntryPoint() override;

       private:
        std::shared_ptr<AngelScript> _engine;
        std::shared_ptr<Plugin> _plugin;
        std::shared_ptr<ScriptBuilder> _scriptBuilder;

        std::string _moduleName;
    };
}  // namespace core::scripting::angelscript

#endif
