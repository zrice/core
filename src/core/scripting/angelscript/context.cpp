#include "context.hpp"

#ifdef ENABLE_ANGELSCRIPT

#include "../../logger.hpp"

namespace core::scripting::angelscript {
    ScriptingContext::ScriptingContext(std::shared_ptr<AngelScript> engine,
                                       std::shared_ptr<Plugin> plugin)
        : ScriptingContextAbstract(engine),
          _engine(engine),
          _plugin(plugin),
          _scriptBuilder() {
        _scriptBuilder = engine->CreateScriptBuilder();

        _moduleName = "PLUGIN_" + _plugin->GetName();
    }

    ScriptingContext::~ScriptingContext() {}

    bool ScriptingContext::Load(boost::filesystem::path file) {
        auto engine = _engine->GetEngine();

        auto err = _scriptBuilder->StartNewModule(engine, _moduleName.c_str());
        if (err < 0) {
            // should only happen if no more memory is left
            CORE_LOGGING(error)
                << "Unexpected issue while creating angelscript context";
            return false;
        }

        auto mainFile = file.string();
        err = _scriptBuilder->AddSectionFromFile(mainFile.c_str());
        if (err < 0) {
            CORE_LOGGING(error) << "Failed to load main file " << file;
            return false;
        }

        err = _scriptBuilder->BuildModule();
        if (err < 0) {
            CORE_LOGGING(error) << "Failed to compile script " << file;
            return false;
        }

        return true;
    }

    bool ScriptingContext::CallEntryPoint() {
        auto module = _engine->GetEngine()->GetModule(_moduleName.c_str());
        auto mainFunction = module->GetFunctionByDecl("void main()");

        // Retrieve context
        auto ctx = _engine->GetContext();

        ctx->Prepare(mainFunction);

        auto err = ctx->Execute();
        if (err != asEXECUTION_FINISHED) {
            CORE_LOGGING(error)
                << "Failed to execute main function " << _plugin->GetName();
            _engine->ReturnContext(ctx);
            return false;
        }

        _engine->ReturnContext(ctx);
        return true;
    }
}  // namespace core::scripting::angelscript

#endif
