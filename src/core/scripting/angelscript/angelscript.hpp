#pragma once

#ifdef ENABLE_ANGELSCRIPT

#include <angelscript.h>
#include <vector>

#include "../scripting_engine.hpp"
#include "script_builder.hpp"

namespace core::scripting::angelscript {
    class AngelScript : public ScriptingEngineAbstract,
                        public std::enable_shared_from_this<AngelScript> {
       public:
        AngelScript();
        ~AngelScript() override;

        std::shared_ptr<ScriptBuilder> CreateScriptBuilder();
        asIScriptEngine *GetEngine();

        /**
         * Retrieve a context from the context pool.
         * Use ReturnContext after the context isn't needed anymore
         * @return A script context
         */
        asIScriptContext *GetContext();

        /**
         * Returns the context to the pool of available contexts
         */
        void ReturnContext(asIScriptContext *ctx);

        std::string GetDefaultMainFile() override;
        std::shared_ptr<ScriptingContextAbstract> CreatePluginContext(
            std::shared_ptr<Plugin> plugin) override;

       private:
        void MessageCallback(const asSMessageInfo *msg, void *param);

        void RegisterApplicationInterface();

       private:
        asIScriptEngine *_engine;
        std::vector<asIScriptContext *> _contextPool;
        std::mutex _poolMutex;
    };
}  // namespace core::scripting::angelscript

#endif
