#include "scripting_engine.hpp"

#include <utility>

namespace core::scripting {
    ScriptingEngineAbstract::ScriptingEngineAbstract(std::string name)
        : _name(std::move(name)) {}

    ScriptingEngineAbstract::~ScriptingEngineAbstract() = default;

    const std::string &ScriptingEngineAbstract::GetName() { return _name; }
}  // namespace core::scripting
