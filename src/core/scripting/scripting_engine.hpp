#pragma once

#include <string>

#include "scripting_context.hpp"

namespace core::scripting {
    class Plugin;

    class ScriptingEngineAbstract {
       public:
        explicit ScriptingEngineAbstract(std::string name);
        virtual ~ScriptingEngineAbstract();

        const std::string &GetName();
        virtual std::string GetDefaultMainFile() = 0;
        virtual std::shared_ptr<ScriptingContextAbstract> CreatePluginContext(
            std::shared_ptr<Plugin> plugin) = 0;

       private:
        std::string _name;
    };
}  // namespace core::scripting
