#include "scripting.hpp"

#ifdef ENABLE_ANGELSCRIPT

#include "angelscript/angelscript.hpp"

#endif

#include <boost/filesystem.hpp>

#include "../profiler.hpp"
#include "plugin.hpp"

namespace fs = boost::filesystem;

namespace core::scripting {
    Scripting::Scripting() : _engines(), _plugins() {
#ifdef ENABLE_ANGELSCRIPT
        _engines["AngelScript"] = std::make_shared<angelscript::AngelScript>();
#endif
    }

    Scripting::~Scripting() = default;

    void Scripting::LoadPlugins() {
        PROFILE_FUNCTION();
        // check if "plugins" is a folder, if not skip plugin loading
        if (!fs::is_directory("plugins")) return;

        for (auto &directory : fs::directory_iterator("plugins")) {
            if (fs::is_directory(directory)) {
                auto plugin = std::make_shared<Plugin>(shared_from_this(),
                                                       directory.path());
                plugin->Load();
                _plugins.push_back(plugin);
            }
        }
    }

    std::shared_ptr<ScriptingEngineAbstract> Scripting::GetScriptEngine(
        std::string name) {
        if (_engines.find(name) == _engines.end()) {
            return nullptr;
        }

        return _engines[name];
    }
}  // namespace core::scripting
