#include "plugin.hpp"

#include <boost/property_tree/json_parser.hpp>

#include "../logger.hpp"
#include "../profiler.hpp"
#include "scripting.hpp"
#include "scripting_engine.hpp"

namespace fs = boost::filesystem;
namespace pt = boost::property_tree;

namespace core::scripting {
    Plugin::Plugin(std::shared_ptr<Scripting> scripting, fs::path folder)
        : _scripting(scripting),
          _basePath(folder),
          _metaData(),
          _valid(false),
          _name(folder.filename().string()) {
        CORE_LOGGING(trace) << "Loading plugin for " << folder;
        auto &metaFile = folder.append("plugin.json");

        CORE_LOGGING(trace) << "Open plugin metadata " << metaFile;
        try {
            pt::read_json(metaFile.string(), _metaData);

            // Verify required fields
            auto type = _metaData.get_optional<std::string>("type");
            if (!type) {
                CORE_LOGGING(warning) << "Failed to load plugin " << folder
                                      << ": Missing type property";
                return;
            } else {
                // Check if specified script engine is available
                if (auto engine = _scripting->GetScriptEngine(type.get())) {
                    _scriptEngine = engine;
                    _mainFile = _scriptEngine->GetDefaultMainFile();

                    CORE_LOGGING(trace)
                        << "Setting plugin language to " << type.get();
                } else {
                    CORE_LOGGING(warning) << "Failed to load plugin " << folder
                                          << ": Invalid language";
                    return;
                }
            }

            // Load optional fields
            auto name = _metaData.get_optional<std::string>("name");
            if (name) {
                _name = name.get();
                CORE_LOGGING(trace) << "Setting plugin name to " << _name;
            }

            auto mainFile = _metaData.get_optional<std::string>("main");
            if (mainFile) {
                _mainFile = mainFile.get();
                CORE_LOGGING(trace)
                    << "Setting plugin main file to " << _mainFile;
            }

            _valid = true;
        } catch (const pt::json_parser::json_parser_error &e) {
            CORE_LOGGING(warning)
                << "Plugin not valid, please verify " << metaFile;
            CORE_LOGGING(warning) << e.message();
        }
    }

    Plugin::~Plugin() {}

    void Plugin::Load() {
        PROFILE_FUNCTION();
        if (!_scriptEngine) return;

        // Create plugin context
        _context = _scriptEngine->CreatePluginContext(shared_from_this());

        auto mainFilePath = _basePath.append(_mainFile);

        if (_context->Load(mainFilePath)) {
            CORE_LOGGING(trace) << "Loaded plugin " << GetName();

            // Call main function
            {
                PROFILE_SCOPE(_name.c_str());
                _context->CallEntryPoint();
            }
        } else {
            _valid = false;
        }
    }

    bool Plugin::IsValid() { return _valid; }

    const std::string &Plugin::GetName() { return _name; }
}  // namespace core::scripting
