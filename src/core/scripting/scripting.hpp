#pragma once

#include <map>
#include <memory>
#include <vector>

#include "scripting_engine.hpp"

namespace core::scripting {
    class Plugin;

    class Scripting : public std::enable_shared_from_this<Scripting> {
       public:
        Scripting();
        virtual ~Scripting();

        void LoadPlugins();

        std::shared_ptr<ScriptingEngineAbstract> GetScriptEngine(
            std::string name);

       private:
        std::map<std::string, std::shared_ptr<ScriptingEngineAbstract>>
            _engines;
        std::vector<std::shared_ptr<Plugin>> _plugins;
    };
}  // namespace core::scripting
