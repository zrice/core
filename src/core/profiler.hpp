// Based on https://gist.github.com/TheCherno/31f135eea6ee729ab5f26a6908eb3a5e
// from TheCherno

#pragma once

#include <algorithm>
#include <chrono>
#include <fstream>
#include <mutex>
#include <string>
#include <thread>

#ifdef PROFILING
#define START_SESSION(name, filename) \
    core::Instrumentor::Get().BeginSession(name, filename)
#define END_SESSION() core::Instrumentor::Get().EndSession()

#define PROFILE_SCOPE(name) core::InstrumentationTimer timer##__LINE__(name)
#ifdef _MSC_VER
#define PROFILE_FUNCTION() PROFILE_SCOPE(__FUNCSIG__)
#else
#define PROFILE_FUNCTION() PROFILE_SCOPE(__PRETTY_FUNCTION__)
#endif
#else
#define START_SESSION(name, filename)
#define END_SESSION()

#define PROFILE_SCOPE(name)
#define PROFILE_FUNCTION()
#endif

namespace core {
    struct ProfileResult {
        std::string name;
        long long start, end;
        uint32_t threadID;
    };

    struct InstrumentationSession {
        std::string name;
    };

    class Instrumentor {
       private:
        InstrumentationSession *_currentSession;
        std::ofstream _outputStream;
        std::mutex _mutex;
        int _profileCount;

       public:
        Instrumentor();

        void BeginSession(const std::string &name,
                          const std::string &filepath = "results.json");
        void EndSession();
        void WriteProfile(const ProfileResult &result);
        void WriteHeader();
        void WriteFooter();

        static Instrumentor &Get();
    };

    class InstrumentationTimer {
       public:
        InstrumentationTimer(std::string name);
        ~InstrumentationTimer();

        void Stop();

       private:
        std::string _name;
        std::chrono::time_point<std::chrono::high_resolution_clock>
            _startTimepoint;
        bool _stopped;
    };
}  // namespace core