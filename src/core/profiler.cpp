#include "profiler.hpp"

namespace core {
    Instrumentor::Instrumentor() : _currentSession(nullptr), _profileCount(0) {}

    void Instrumentor::BeginSession(const std::string &name,
                                    const std::string &filepath) {
        _outputStream.open(filepath);
        WriteHeader();
        _currentSession = new InstrumentationSession{name};
    }

    void Instrumentor::EndSession() {
        WriteFooter();
        _outputStream.close();
        delete _currentSession;
        _currentSession = nullptr;
        _profileCount = 0;
    }

    void Instrumentor::WriteProfile(const ProfileResult &result) {
        std::lock_guard<std::mutex> lock(_mutex);

        if (_profileCount++ > 0) _outputStream << ",";

        std::string name = result.name;
        std::replace(name.begin(), name.end(), '"', '\'');

        _outputStream << "{";
        _outputStream << "\"cat\":\"function\",";
        _outputStream << "\"dur\":" << (result.end - result.start) << ',';
        _outputStream << "\"name\":\"" << name << "\",";
        _outputStream << "\"ph\":\"X\",";
        _outputStream << "\"pid\":0,";
        _outputStream << "\"tid\":" << result.threadID << ",";
        _outputStream << "\"ts\":" << result.start;
        _outputStream << "}";

        _outputStream.flush();
    }

    void Instrumentor::WriteHeader() {
        _outputStream << "{\"otherData\": {},\"traceEvents\":[";
        _outputStream.flush();
    }

    void Instrumentor::WriteFooter() {
        _outputStream << "]}";
        _outputStream.flush();
    }

    Instrumentor &Instrumentor::Get() {
        static Instrumentor instance;
        return instance;
    }

    InstrumentationTimer::InstrumentationTimer(std::string name)
        : _name(name), _stopped(false) {
        _startTimepoint = std::chrono::high_resolution_clock::now();
    }

    InstrumentationTimer::~InstrumentationTimer() {
        if (!_stopped) Stop();
    }

    void InstrumentationTimer::Stop() {
        auto endTimepoint = std::chrono::high_resolution_clock::now();

        long long start =
            std::chrono::time_point_cast<std::chrono::microseconds>(
                _startTimepoint)
                .time_since_epoch()
                .count();
        long long end = std::chrono::time_point_cast<std::chrono::microseconds>(
                            endTimepoint)
                            .time_since_epoch()
                            .count();

        uint32_t threadID =
            std::hash<std::thread::id>{}(std::this_thread::get_id());
        Instrumentor::Get().WriteProfile({_name, start, end, threadID});

        _stopped = true;
    }
}  // namespace core