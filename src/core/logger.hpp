#pragma once

#include <boost/log/trivial.hpp>
#include <boost/log/utility/manipulators/add_value.hpp>
#include <string>

#define CORE_LOGGING(lvl)                                             \
    BOOST_LOG_TRIVIAL(lvl) << boost::log::add_value("Line", __LINE__) \
                           << boost::log::add_value(                  \
                                  "File", core::path_to_filename(__FILE__))

namespace core {
    std::string path_to_filename(std::string path);
}
