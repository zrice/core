#include "player_cache.hpp"

#include <random>

#include "../logger.hpp"
#include "../mysql/migrations/_defaults.hpp"

namespace core::cache {
    PlayerCache::PlayerCache(std::shared_ptr<Redis> redis,
                             std::shared_ptr<mysql::MySQL> database)
        : _redis(std::move(redis)),
          _database(std::move(database)),
          _randomDevice(),
          _random(_randomDevice()) {}

    PlayerCache::~PlayerCache() = default;

    Player PlayerCache::GetPlayer(uint32_t playerId) {
        Player player;

        auto key = "player:" + std::to_string(playerId);
        auto exists =
            _redis->Execute<int64_t>(bredis::single_command_t{"exists", key});
        if (exists == 0) {
            // Player isn't cache, populate the cache
            CORE_LOGGING(trace) << "Player not cached";

            auto stmt = _database->CreateStatement(
                "SELECT `players`.*, `player_data`.* FROM `" GAME_DATABASE
                "`.`players` LEFT JOIN `" GAME_DATABASE
                "`.`player_data` ON `players`.`id` = `player_data`.`player_id` "
                "WHERE `players`.`id`=%1%");
            stmt << playerId;

            // Execute statement
            auto result = stmt();

            if (result->Next()) {
                auto name = result->GetString("name");
                auto classId = result->GetUnsigned8("class");
                auto skillGroup = result->GetUnsigned8("skill_group");
                auto x = result->GetUnsigned32("x");
                auto y = result->GetUnsigned32("y");
                auto hp = result->GetUnsigned32("hp");
                auto mp = result->GetUnsigned32("mp");
                auto stamina = result->GetUnsigned16("stamina");
                auto st = result->GetUnsigned16("st");
                auto ht = result->GetUnsigned16("ht");
                auto dx = result->GetUnsigned16("dx");
                auto iq = result->GetUnsigned16("iq");
                auto level = result->GetUnsigned16("level");
                auto exp = result->GetUnsigned64("exp");
                auto gold = result->GetUnsigned64("gold");
                auto playtime = result->GetUnsigned64("playtime");

                // Write result into cache
                _redis->Execute(bredis::command_container_t{
                    bredis::single_command_t{"hset", key, "id",
                                             std::to_string(playerId)},
                    bredis::single_command_t{"hset", key, "name", name},
                    bredis::single_command_t{"hset", key, "class",
                                             std::to_string(classId)},
                    bredis::single_command_t{"hset", key, "skillGroup",
                                             std::to_string(skillGroup)},
                    bredis::single_command_t{"hset", key, "x",
                                             std::to_string(x)},
                    bredis::single_command_t{"hset", key, "y",
                                             std::to_string(y)},
                    bredis::single_command_t{"hset", key, "hp",
                                             std::to_string(hp)},
                    bredis::single_command_t{"hset", key, "mp",
                                             std::to_string(mp)},
                    bredis::single_command_t{"hset", key, "stamina",
                                             std::to_string(stamina)},
                    bredis::single_command_t{"hset", key, "st",
                                             std::to_string(st)},
                    bredis::single_command_t{"hset", key, "ht",
                                             std::to_string(ht)},
                    bredis::single_command_t{"hset", key, "dx",
                                             std::to_string(dx)},
                    bredis::single_command_t{"hset", key, "iq",
                                             std::to_string(iq)},
                    bredis::single_command_t{"hset", key, "level",
                                             std::to_string(level)},
                    bredis::single_command_t{"hset", key, "exp",
                                             std::to_string(exp)},
                    bredis::single_command_t{"hset", key, "gold",
                                             std::to_string(gold)},
                    bredis::single_command_t{"hset", key, "playtime",
                                             std::to_string(playtime)}});
            } else {
                // Player id is invalid, this should never happen if the
                // database is correct
                CORE_LOGGING(error) << "Player not found!";
                return player;
            }
        }

        // Read all keys from cache
        auto result =
            _redis->ExecuteArray(bredis::single_command_t{"hgetall", key});
        for (auto i = 0; i < result.elements.size(); i += 2) {
            auto fieldName = boost::get<string_t>(result.elements[i]);
            auto value = boost::get<string_t>(result.elements[i + 1]);

            if (fieldName.str == "id") {
                player.id = boost::lexical_cast<uint32_t>(value.str);
            } else if (fieldName.str == "name") {
                player.name = value.str;
            } else if (fieldName.str == "class") {
                player.playerClass = static_cast<uint8_t>(
                    boost::lexical_cast<uint32_t>(value.str));
            } else if (fieldName.str == "skillGroup") {
                player.skillGroup = static_cast<uint8_t>(
                    boost::lexical_cast<uint32_t>(value.str));
            } else if (fieldName.str == "x") {
                player.posX = boost::lexical_cast<int32_t>(value.str);
            } else if (fieldName.str == "y") {
                player.posY = boost::lexical_cast<int32_t>(value.str);
            } else if (fieldName.str == "hp") {
                player.hp = boost::lexical_cast<uint32_t>(value.str);
            } else if (fieldName.str == "mp") {
                player.mp = boost::lexical_cast<uint32_t>(value.str);
            } else if (fieldName.str == "stamina") {
                player.stamina = boost::lexical_cast<uint32_t>(value.str);
            } else if (fieldName.str == "st") {
                player.st = static_cast<uint8_t>(
                    boost::lexical_cast<uint32_t>(value.str));
            } else if (fieldName.str == "ht") {
                player.ht = static_cast<uint8_t>(
                    boost::lexical_cast<uint32_t>(value.str));
            } else if (fieldName.str == "dx") {
                player.dx = static_cast<uint8_t>(
                    boost::lexical_cast<uint32_t>(value.str));
            } else if (fieldName.str == "iq") {
                player.iq = static_cast<uint8_t>(
                    boost::lexical_cast<uint32_t>(value.str));
            } else if (fieldName.str == "level") {
                player.level = static_cast<uint8_t>(
                    boost::lexical_cast<uint32_t>(value.str));
            } else if (fieldName.str == "exp") {
                player.exp = boost::lexical_cast<uint32_t>(value.str);
            } else if (fieldName.str == "gold") {
                player.gold = boost::lexical_cast<uint32_t>(value.str);
            } else if (fieldName.str == "playtime") {
                player.playtime = boost::lexical_cast<uint32_t>(value.str);
            } else {
                CORE_LOGGING(trace)
                    << "Unknown cache field for player " << fieldName.str
                    << " (value: " << value.str << ")";
            }
        }

        return player;
    }

    void PlayerCache::AddPlayer(uint32_t accountId, uint32_t playerId) {
        auto key = "players:" + std::to_string(accountId);
        CORE_LOGGING(trace)
            << "Adding player " << playerId << " to " << accountId;

        _redis->Execute(
            bredis::single_command_t{"lpush", key, std::to_string(playerId)});
    }

    std::vector<uint32_t> PlayerCache::GetPlayers(uint32_t accountId) {
        auto key = "players:" + std::to_string(accountId);
        CORE_LOGGING(trace) << "Reading player cache for account " << accountId;

        std::vector<uint32_t> ret;

        // Check if key exists
        auto res = _redis->Execute(bredis::single_command_t{"exists", key});
        if (auto exists = boost::get<int64_t>(&res)) {
            if (*exists == 1) {
                // Key exists so we can read from the cache here
                CORE_LOGGING(trace) << "Cache exists";

                for (auto i = 0; i < 4; i++) {
                    res = _redis->Execute(bredis::single_command_t{
                        "lindex", key, std::to_string(i)});
                    auto str = boost::get<string_t>(&res);
                    if (!str) {
                        CORE_LOGGING(trace) << "No entry in position " << i;
                        continue;
                    }

                    auto playerId = boost::lexical_cast<uint32_t>(str->str);

                    CORE_LOGGING(trace)
                        << "Position " << i << " Player: " << playerId;
                    ret.push_back(playerId);
                }

                return ret;
            }
        }

        // We don't have any cache key, we'll load from the database and
        // populate the cache
        auto stmt =
            _database->CreateStatement("SELECT * FROM `" GAME_DATABASE
                                       "`.`players` WHERE `account_id` = %1%");
        stmt << accountId;
        auto result = stmt();
        while (result->Next()) {
            auto playerId = result->GetUnsigned32("id");
            ret.push_back(playerId);

            _redis->Execute(bredis::single_command_t{"lpush", key,
                                                     std::to_string(playerId)});
        }

        return ret;
    }

    uint32_t PlayerCache::GenerateLoginKey(uint32_t accountId) {
        std::uniform_int_distribution<uint32_t> dist;

        bool unique = false;
        uint32_t id;
        while (!unique) {
            CORE_LOGGING(trace) << "Generating login key";

            auto random = dist(_random);
            auto key = "login:" + std::to_string(random);
            auto exists = _redis->Execute<int64_t>(
                bredis::single_command_t{"exists", key});

            unique = exists == 0;
            CORE_LOGGING(trace) << "Key " << random << " is unique? " << unique;
            id = random;

            if (unique) {
                _redis->Execute(bredis::single_command_t{
                    "set", key, std::to_string(accountId)});
            }
        }

        return id;
    }

    uint32_t PlayerCache::CheckLoginKey(uint32_t loginKey) {
        auto key = "login:" + std::to_string(loginKey);
        auto exists =
            _redis->Execute<int64_t>(bredis::single_command_t{"exists", key});

        if (exists != 1) return 0;

        auto accountId =
            _redis->Execute<string_t>(bredis::single_command_t{"get", key});
        return boost::lexical_cast<uint32_t>(accountId.str);
    }

    void PlayerCache::SetLoginKeyTTL(uint32_t loginKey) {
        auto key = "login:" + std::to_string(loginKey);
        _redis->Execute(bredis::single_command_t{
            "expire", key, "30"});  // 30 seconds life time of login key
    }

    void PlayerCache::RemoveLoginKeyTTL(uint32_t loginKey) {
        auto key = "login:" + std::to_string(loginKey);
        _redis->Execute(bredis::single_command_t{"persist", key});
    }

    void PlayerCache::SavePlayer(const Player &player) {
        auto key = "player:" + std::to_string(player.id);
        _redis->Execute(bredis::command_container_t{
            bredis::single_command_t{"hset", key, "name", player.name},
            bredis::single_command_t{"hset", key, "class",
                                     std::to_string(player.playerClass)},
            bredis::single_command_t{"hset", key, "skillGroup",
                                     std::to_string(player.skillGroup)},
            bredis::single_command_t{"hset", key, "x",
                                     std::to_string(player.posX)},
            bredis::single_command_t{"hset", key, "y",
                                     std::to_string(player.posY)},
            bredis::single_command_t{"hset", key, "hp",
                                     std::to_string(player.hp)},
            bredis::single_command_t{"hset", key, "mp",
                                     std::to_string(player.mp)},
            bredis::single_command_t{"hset", key, "stamina",
                                     std::to_string(player.stamina)},
            bredis::single_command_t{"hset", key, "st",
                                     std::to_string(player.st)},
            bredis::single_command_t{"hset", key, "ht",
                                     std::to_string(player.ht)},
            bredis::single_command_t{"hset", key, "dx",
                                     std::to_string(player.dx)},
            bredis::single_command_t{"hset", key, "iq",
                                     std::to_string(player.iq)},
            bredis::single_command_t{"hset", key, "level",
                                     std::to_string(player.level)},
            bredis::single_command_t{"hset", key, "exp",
                                     std::to_string(player.exp)},
            bredis::single_command_t{"hset", key, "gold",
                                     std::to_string(player.gold)},
            bredis::single_command_t{"hset", key, "playtime",
                                     std::to_string(player.playtime)},
        });
    }
}  // namespace core::cache