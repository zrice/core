#pragma once

#include <boost/asio.hpp>
#include <bredis/Connection.hpp>
#include <bredis/Extract.hpp>
#include <memory>

#include "../logger.hpp"

namespace core::cache {
    using string_t = typename bredis::extracts::string_t;
    using nil_t = typename bredis::extracts::nil_t;
    using array_wrapper_t = typename bredis::extracts::array_wrapper_t;
    using Iterator =
        typename bredis::to_iterator<boost::asio::streambuf>::iterator_t;

    class Redis {
       public:
        Redis(std::shared_ptr<bredis::Connection<boost::asio::ip::tcp::socket>>
                  connection);

        bredis::extracts::array_holder_t ExecuteArray(
            const bredis::command_wrapper_t &command) {
            auto res = Execute(command);
            return boost::get<bredis::extracts::array_holder_t>(res);
        }

        bredis::extracts::extraction_result_t Execute(
            const bredis::command_wrapper_t &command) {
            boost::asio::streambuf buffer;
            _connection->write(command);

            auto resultMarkers = _connection->read(buffer);
            auto extract = boost::apply_visitor(bredis::extractor<Iterator>(),
                                                resultMarkers.result);
            buffer.consume(resultMarkers.consumed);

            return extract;
        }

        template <typename Result>
        Result Execute(const bredis::command_wrapper_t &command) {
            auto extract = Execute(command);

            try {
                return boost::get<Result>(extract);
            } catch (const std::exception &e) {
                CORE_LOGGING(error)
                    << "Failed to extract result, type is " << extract.which();
                throw e;
            }
        }

       private:
        std::shared_ptr<bredis::Connection<boost::asio::ip::tcp::socket>>
            _connection;
    };
}  // namespace core::cache