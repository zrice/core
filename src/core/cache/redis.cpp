#include "redis.hpp"

#include <utility>

namespace core::cache {
    Redis::Redis(
        std::shared_ptr<bredis::Connection<boost::asio::ip::tcp::socket>>
            connection)
        : _connection(std::move(connection)) {}
}  // namespace core::cache