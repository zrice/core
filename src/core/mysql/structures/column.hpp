#pragma once

#include <string>

#include "type.hpp"

namespace core::mysql::structures {
    class Column {
       public:
        Column(std::string name, Type type);

        [[nodiscard]] const std::string &GetName() const;
        [[nodiscard]] const std::string &GetDefaultValue() const;
        [[nodiscard]] const Type &GetType() const;

        Column &Default(const std::string &defaultValue);

       private:
        std::string _name;
        Type _type;

        std::string _defaultValue;
    };
}  // namespace core::mysql::structures