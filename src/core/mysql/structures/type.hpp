#pragma once

#include <string>

namespace core::mysql::structures {
    class Type {
       public:
        explicit Type(std::string type, unsigned long length = 0);

        [[nodiscard]] std::string GetType() const;
        [[nodiscard]] unsigned long GetLength() const;
        [[nodiscard]] bool IsNullable() const;
        [[nodiscard]] bool IsPrimaryKey() const;
        [[nodiscard]] bool IsAutoIncrement() const;

        Type &NotNull();
        Type &PrimaryKey();
        Type &AutoIncrement();

       private:
        std::string _type;
        unsigned long _length;

        bool _nullable;
        bool _primaryKey;
        bool _autoIncrement;
    };
}  // namespace core::mysql::structures