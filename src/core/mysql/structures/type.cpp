#include "type.hpp"

#include <utility>

namespace core::mysql::structures {
    Type::Type(std::string type, unsigned long length)
        : _type(std::move(type)),
          _length(length),
          _nullable(true),
          _primaryKey(false),
          _autoIncrement(false) {}

    std::string Type::GetType() const { return _type; }

    unsigned long Type::GetLength() const { return _length; }

    bool Type::IsNullable() const { return _nullable; }

    bool Type::IsPrimaryKey() const { return _primaryKey; }

    bool Type::IsAutoIncrement() const { return _autoIncrement; }

    Type &Type::NotNull() {
        _nullable = false;
        return *this;
    }

    Type &Type::PrimaryKey() {
        _primaryKey = true;
        return *this;
    }

    Type &Type::AutoIncrement() {
        _autoIncrement = true;
        return *this;
    }
}  // namespace core::mysql::structures
