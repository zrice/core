#include "column.hpp"

#include <utility>

namespace core::mysql::structures {
    Column::Column(std::string name, Type type)
        : _name(std::move(name)), _type(std::move(type)) {}

    const std::string &Column::GetName() const { return _name; }

    const std::string &Column::GetDefaultValue() const { return _defaultValue; }

    const Type &Column::GetType() const { return _type; }

    Column &Column::Default(const std::string &defaultValue) {
        _defaultValue = defaultValue;
        return *this;
    }
}  // namespace core::mysql::structures