#pragma once

#include <string>
#include <vector>

#include "column.hpp"

namespace core::mysql::structures {
    class Table {
       public:
        class Builder {
           public:
            explicit Builder(std::string database, std::string name);
            Builder &operator<<(const Column &column);
            explicit operator Table();

           private:
            std::string _database;
            std::string _name;
            std::vector<Column> _columns;
        };

        Table(std::string database, std::string name,
              std::vector<Column> columns);

        [[nodiscard]] const std::string &GetDatabase() const;
        [[nodiscard]] const std::string &GetName() const;
        const std::vector<Column> &GetColumns();

       private:
        std::string _database;
        std::string _name;
        std::vector<Column> _columns;
    };

    inline Table::Builder::operator Table() {
        return Table(_database, _name, _columns);
    }
}  // namespace core::mysql::structures