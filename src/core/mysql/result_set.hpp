#pragma once

#include <map>
#include <memory>
#include <mysql.h>
#include <string>

// inspired by mariadb++ (https://github.com/viaduck/mariadbpp)

#define MAKE_GETTER_SIG_STR(name, type, fq) \
    type fq Get##name(const std::string &name) const
#define MAKE_GETTER_SIG_INT(name, type, fq) \
    type fq Get##name(unsigned int index) const

#define MAKE_GETTER_DECL(name, type)   \
    MAKE_GETTER_SIG_STR(name, type, ); \
    MAKE_GETTER_SIG_INT(name, type, )

#define MAKE_GETTER(name, type, virtualType)       \
    MAKE_GETTER_SIG_STR(name, type, ResultSet::) { \
        return Get##name(GetColumnIndex(name));    \
    }                                              \
    MAKE_GETTER_SIG_INT(name, type, ResultSet::) { \
        CheckResultExists();                       \
        CheckType(index, virtualType);             \
                                                   \
        if (index >= GetColumnCount())             \
            throw std::out_of_range("Column index out of range");

namespace core::mysql {
    enum Types {
        null = 0,
        string,
        unsigned8,
        signed8,
        unsigned16,
        signed16,
        unsigned32,
        signed32,
        unsigned64,
        signed64,
        float32,
        double64,
        date,
        datetime,
        boolean
    };

    class MySQL;

    struct MySQLException : public std::exception {
        MySQLException(const std::string &error) : _error(error) {}

        std::string _error;

        const char *what() const noexcept override { return _error.c_str(); }
    };

    class ResultSet {
        friend class MySQL;

       public:
        explicit ResultSet(std::shared_ptr<MySQL> mysql);
        virtual ~ResultSet();

        [[nodiscard]] unsigned int GetColumnCount() const;
        [[nodiscard]] unsigned int GetColumnIndex(
            const std::string &name) const;
        [[nodiscard]] unsigned long GetColumnSize(unsigned int index) const;
        [[nodiscard]] unsigned long GetRowCount() const;
        [[nodiscard]] unsigned long GetRowIndex() const;
        [[nodiscard]] Types GetType(unsigned int columnIndex) const;

        bool Next();
        bool SetRowIndex(unsigned int index);

        MAKE_GETTER_DECL(String, std::string);
        MAKE_GETTER_DECL(Signed8, int8_t);
        MAKE_GETTER_DECL(Unsigned8, uint8_t);
        MAKE_GETTER_DECL(Signed16, int16_t);
        MAKE_GETTER_DECL(Unsigned16, uint16_t);
        MAKE_GETTER_DECL(Signed32, int32_t);
        MAKE_GETTER_DECL(Unsigned32, uint32_t);
        MAKE_GETTER_DECL(Signed64, int64_t);
        MAKE_GETTER_DECL(Unsigned64, uint64_t);
        MAKE_GETTER_DECL(Float, float);
        MAKE_GETTER_DECL(Double, double);
        MAKE_GETTER_DECL(IsNull, bool);
        MAKE_GETTER_DECL(Boolean, bool);
        // MAKE_GETTER_DECL(Date, bool);

       private:
        void CheckType(unsigned int columnIndex, Types requestedType) const;
        void CheckResultExists() const;

       private:
        MYSQL_RES *_result;
        MYSQL_FIELD *_fields;
        MYSQL_ROW _row;
        long unsigned int *_lengths;
        std::map<std::string, unsigned int> _mapFields;

        unsigned int _columnCount;
        bool _hasResult;
    };
}  // namespace core::mysql