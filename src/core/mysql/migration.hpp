#pragma once

#include <string>
#include <vector>

#include "actions/action.hpp"
#include "mysql.hpp"

namespace core::mysql {
    class Migration {
       public:
        explicit Migration(const std::string &name);
        virtual ~Migration();

        [[nodiscard]] const std::string &GetName() const;

        void Add(actions::Action *action);

        bool Execute(std::shared_ptr<MySQL> mysql);

       private:
        std::string _name;
        std::vector<actions::Action *> _actions;
    };
}  // namespace core::mysql