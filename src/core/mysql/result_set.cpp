#include "result_set.hpp"

#include <boost/lexical_cast.hpp>
#include <boost/numeric/conversion/cast.hpp>

#include "../logger.hpp"
#include "mysql.hpp"

namespace core::mysql {
    ResultSet::ResultSet(std::shared_ptr<MySQL> mysql)
        : _result(mysql_store_result(mysql->_mysql)),
          _columnCount(0),
          _mapFields() {
        if (_result) {
            _columnCount = mysql_num_fields(_result);
            _fields = mysql_fetch_fields(_result);

            for (auto i = 0; i < _columnCount; i++) {
                _mapFields[_fields[i].name] = i;
            }
        } else {
            CORE_LOGGING(warning)
                << "Failed to store result: " << mysql->GetError();
            throw MySQLException{mysql->GetError()};
        };
    }

    ResultSet::~ResultSet() {
        if (_result) mysql_free_result(_result);
    }

    unsigned int ResultSet::GetColumnCount() const { return _columnCount; }

    unsigned int ResultSet::GetColumnIndex(const std::string &name) const {
        auto i = _mapFields.find(name);
        if (i == _mapFields.end())
            throw std::out_of_range("No column named " + name);

        return i->second;
    }

    unsigned long ResultSet::GetColumnSize(unsigned int index) const {
        if (index >= _columnCount)
            throw std::out_of_range("Column index out of range");

        return _lengths[index];
    }

    unsigned long ResultSet::GetRowCount() const {
        return mysql_num_rows(_result);
    }

    unsigned long ResultSet::GetRowIndex() const {
        return (unsigned long)mysql_row_tell(_result);
    }

    Types ResultSet::GetType(unsigned int columnIndex) const {
        if (columnIndex >= _columnCount)
            throw std::out_of_range("Column index out of range");

        bool isUnsigned =
            (_fields[columnIndex].flags & UNSIGNED_FLAG) == UNSIGNED_FLAG;

        switch (_fields[columnIndex].type) {
            case MYSQL_TYPE_NULL:
                return Types::null;
            case MYSQL_TYPE_TINY:
                return isUnsigned ? Types::unsigned8 : Types::signed8;
            case MYSQL_TYPE_SHORT:
                return isUnsigned ? Types::unsigned16 : Types::signed16;
            case MYSQL_TYPE_INT24:
            case MYSQL_TYPE_LONG:
                return isUnsigned ? Types::unsigned32 : Types::signed32;
            case MYSQL_TYPE_LONGLONG:
                return isUnsigned ? Types::unsigned64 : Types::signed64;
            case MYSQL_TYPE_FLOAT:
                return Types::float32;
            case MYSQL_TYPE_DOUBLE:
                return Types::double64;
            case MYSQL_TYPE_BIT:
                return Types::boolean;
            default:
            case MYSQL_TYPE_VARCHAR:
            case MYSQL_TYPE_VAR_STRING:
            case MYSQL_TYPE_STRING:
                return Types::string;
        }
    }

    bool ResultSet::Next() {
        if (!_result) return (_hasResult = false);

        _row = mysql_fetch_row(_result);
        _lengths = mysql_fetch_lengths(_result);

        return (_hasResult = _row != nullptr);
    }

    bool ResultSet::SetRowIndex(unsigned int index) {
        mysql_data_seek(_result, index);
        return Next();
    }

    void ResultSet::CheckType(unsigned int columnIndex,
                              Types requestedType) const {
        auto actual = GetType(columnIndex);
        bool match = false;

        switch (requestedType) {
            case Types::float32:
            case Types::double64:
            case Types::date:
            case Types::datetime:
                match = requestedType == actual;
                break;
            case Types::unsigned8:
            case Types::signed8:
                match = actual == Types::signed8 || actual == Types::unsigned8;
                break;
            case Types::unsigned16:
            case Types::signed16:
                match =
                    actual == Types::signed16 || actual == Types::unsigned16;
                break;
            case Types::unsigned32:
            case Types::signed32:
                match =
                    actual == Types::signed32 || actual == Types::unsigned32;
                break;
            case Types::unsigned64:
            case Types::signed64:
                match =
                    actual == Types::signed64 || actual == Types::unsigned64;
                break;
            case Types::boolean:
                match = actual == Types::signed8 || actual == Types::boolean;
                break;
            case Types::string:
                match = actual == Types::string;
                break;
            default:
                match = false;
        }

        if (!match) {
            std::invalid_argument("Type mismatch");
        }
    }

    void ResultSet::CheckResultExists() const {
        if (!_hasResult) throw std::invalid_argument("No result");
    }

    MAKE_GETTER(String, std::string, Types::string)
    return std::string(_row[index], GetColumnSize(index));
}  // namespace core::mysql

MAKE_GETTER(Signed8, int8_t, Types::signed8)
return boost::numeric_cast<int8_t>(boost::lexical_cast<int>(GetString(index)));
}

MAKE_GETTER(Unsigned8, uint8_t, Types::unsigned8)
return boost::numeric_cast<uint8_t>(boost::lexical_cast<int>(GetString(index)));
}

MAKE_GETTER(Signed16, int16_t, Types::signed16)
return boost::numeric_cast<int16_t>(boost::lexical_cast<int>(GetString(index)));
}

MAKE_GETTER(Unsigned16, uint16_t, Types::unsigned16)
return boost::numeric_cast<uint16_t>(
    boost::lexical_cast<int>(GetString(index)));
}

MAKE_GETTER(Signed32, int32_t, Types::signed32)
return boost::numeric_cast<int32_t>(boost::lexical_cast<int>(GetString(index)));
}

MAKE_GETTER(Unsigned32, uint32_t, Types::unsigned32)
return boost::numeric_cast<uint32_t>(
    boost::lexical_cast<int>(GetString(index)));
}

MAKE_GETTER(Signed64, int64_t, Types::signed64)
return boost::numeric_cast<int64_t>(
    boost::lexical_cast<long>(GetString(index)));
}

MAKE_GETTER(Unsigned64, uint64_t, Types::unsigned64)
return boost::numeric_cast<uint64_t>(
    boost::lexical_cast<long>(GetString(index)));
}

MAKE_GETTER(Float, float, Types::float32)
return boost::numeric_cast<float>(
    boost::lexical_cast<double>(GetString(index)));
}

MAKE_GETTER(Double, double, Types::double64)
return boost::lexical_cast<double>(GetString(index));
}

MAKE_GETTER(IsNull, bool, Types::null)
return !_row[index];
}

MAKE_GETTER(Boolean, bool, Types::boolean)
return boost::numeric_cast<bool>(boost::lexical_cast<int>(GetString(index)));
}

/*MAKE_GETTER_DECL(Signed8, int8_t);
            MAKE_GETTER_DECL(Unsigned8, uint8_t);
            MAKE_GETTER_DECL(Signed16, int16_t);
            MAKE_GETTER_DECL(Unsigned16, uint16_t);
            MAKE_GETTER_DECL(Signed32, int32_t);
            MAKE_GETTER_DECL(Unsigned32, uint32_t);
            MAKE_GETTER_DECL(Signed64, int64_t);
            MAKE_GETTER_DECL(Unsigned64, uint64_t);
            MAKE_GETTER_DECL(Float, float);
            MAKE_GETTER_DECL(Double, double);
            MAKE_GETTER_DECL(IsNull, bool);
            MAKE_GETTER_DECL(Boolean, bool);*/
}  // namespace core::mysql