#pragma once

#include <boost/format.hpp>
#include <memory>
#include <string>

#include "result_set.hpp"

namespace core::mysql {
    class MySQL;

    class Statement {
       public:
        Statement(std::shared_ptr<MySQL> mysql, const std::string &statement);

        std::string EscapeString(const std::string &str);

        friend Statement &operator<<(Statement &stmt, const std::string &value);
        friend Statement &operator<<(Statement &stmt, int value);

        std::shared_ptr<ResultSet> operator()();

        std::string GetStatement();

       private:
        std::string _statement;
        boost::format _formatter;
        std::shared_ptr<MySQL> _mysql;
    };
}  // namespace core::mysql