#pragma once

#include <memory>
#include <vector>

#include "migration.hpp"
#include "mysql.hpp"

namespace core::mysql {
    class MigrationManager {
       public:
        MigrationManager();
        virtual ~MigrationManager();

        bool Migrate(std::shared_ptr<MySQL> mysql);
        void AddMigration(Migration *name);

        void LoadMigrations();

       private:
        std::vector<Migration *> _migrations;
    };
}  // namespace core::mysql