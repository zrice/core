#include "../actions/create_table.hpp"
#include "../structures/column.hpp"
#include "../structures/table.hpp"
#include "_defaults.hpp"
#include "migrations.h"

namespace core {
    namespace mysql {
        namespace migrations {
            M20181027_230100_CreatePlayerQuickSlots::
                M20181027_230100_CreatePlayerQuickSlots()
                : Migration("M20181027_230100_CreatePlayerQuickSlots") {
                using namespace structures;
                using namespace actions;

                Table::Builder player_quick_slot(GAME_DATABASE,
                                                 "player_quick_slot");
                player_quick_slot
                    << Column("player_id",
                              Type("int", 11).NotNull().PrimaryKey())
                    << Column("slot_id",
                              Type("smallint", 4).NotNull().PrimaryKey())
                    << Column("type", Type("tinyint", 3).NotNull())
                    << Column("position", Type("smallint", 4).NotNull());
                Add(new CreateTable(player_quick_slot.operator Table()));
            }
        }  // namespace migrations
    }      // namespace mysql
}  // namespace core
