#include "../actions/create_table.hpp"
#include "../structures/column.hpp"
#include "../structures/table.hpp"
#include "_defaults.hpp"
#include "migrations.h"

namespace core {
    namespace mysql {
        namespace migrations {
            M20181027_224500_CreatePlayerData::
                M20181027_224500_CreatePlayerData()
                : Migration("M20181027_224500_CreatePlayerData") {
                using namespace structures;
                using namespace actions;

                Table::Builder player_data(GAME_DATABASE, "player_data");
                player_data
                    << Column("player_id",
                              Type("int", 11).NotNull().PrimaryKey())  // id
                    << Column("x", Type("int", 8).NotNull())
                    << Column("y", Type("int", 8).NotNull())
                    << Column("map_index", Type("smallint", 4).NotNull())
                    << Column("hp", Type("int", 6).NotNull())  // health points
                    << Column("mp", Type("int", 6).NotNull())  // mana points
                    << Column("stamina",
                              Type("smallint", 4).NotNull())  // stamina points
                    << Column("st", Type("smallint", 3).NotNull())
                    << Column("ht", Type("smallint", 3).NotNull())
                    << Column("dx", Type("smallint", 3).NotNull())
                    << Column("iq", Type("smallint", 3).NotNull())
                    << Column("level", Type("smallint", 3).NotNull())
                    << Column("exp", Type("smallint", 3).NotNull())
                    << Column("gold", Type("bigint", 20).NotNull())  // 19 + '-'
                    << Column("rank_points", Type("int", 6).NotNull())
                    << Column("playtime",
                              Type("int", 6).NotNull())  // in minutes
                    << Column("last_play", Type("datetime").NotNull())
                    << Column("stat_point", Type("smallint", 4).NotNull())
                    << Column("skill_point", Type("smallint", 3).NotNull());
                Add(new CreateTable(player_data.operator Table()));
            }
        }  // namespace migrations
    }      // namespace mysql
}  // namespace core
