#include "../actions/create_table.hpp"
#include "../structures/column.hpp"
#include "../structures/table.hpp"
#include "_defaults.hpp"
#include "migrations.h"

namespace core {
    namespace mysql {
        namespace migrations {
            M20181013_234200_CreateAccounts::M20181013_234200_CreateAccounts()
                : Migration("M20181013_234200_CreateAccounts") {
                using namespace structures;
                using namespace actions;

                Table::Builder accounts(ACCOUNT_DATABASE, "accounts");
                accounts << Column("id", Type("int", 11)
                                             .NotNull()
                                             .PrimaryKey()
                                             .AutoIncrement())
                         << Column("status", Type("tinytext")).Default("NULL")
                         << Column("username", Type("varchar", 40).NotNull())
                         << Column("password", Type("varchar", 60).NotNull())
                         << Column("email", Type("varchar", 100).NotNull())
                         << Column("registration_date",
                                   Type("datetime").NotNull())
                                .Default("CURRENT_TIMESTAMP")
                         << Column("last_modified", Type("datetime").NotNull())
                                .Default("CURRENT_TIMESTAMP");
                Add(new CreateTable(accounts.operator Table()));
            }
        }  // namespace migrations
    }      // namespace mysql
}  // namespace core
