#include "../actions/create_table.hpp"
#include "../structures/column.hpp"
#include "../structures/table.hpp"
#include "_defaults.hpp"
#include "migrations.h"

namespace core {
    namespace mysql {
        namespace migrations {
            M20181027_225900_CreatePlayerSkills::
                M20181027_225900_CreatePlayerSkills()
                : Migration("M20181027_225900_CreatePlayerSkills") {
                using namespace structures;
                using namespace actions;

                Table::Builder player_skills(GAME_DATABASE, "player_skills");
                player_skills
                    << Column("player_id",
                              Type("int", 11).NotNull().PrimaryKey())
                    << Column("skill_id",
                              Type("smallint", 3).NotNull().PrimaryKey())
                    << Column("level", Type("tinyint", 2).NotNull());
                Add(new CreateTable(player_skills.operator Table()));
            }
        }  // namespace migrations
    }      // namespace mysql
}  // namespace core
