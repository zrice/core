#include "../actions/create_table.hpp"
#include "../structures/column.hpp"
#include "../structures/table.hpp"
#include "_defaults.hpp"
#include "migrations.h"

namespace core {
    namespace mysql {
        namespace migrations {
            M20181027_225700_CreatePlayerEquipment::
                M20181027_225700_CreatePlayerEquipment()
                : Migration("M20181027_225700_CreatePlayerEquipment") {
                using namespace structures;
                using namespace actions;

                Table::Builder player_equipment(GAME_DATABASE,
                                                "player_equipment");
                player_equipment
                    << Column("player_id",
                              Type("int", 11).NotNull().PrimaryKey())  // id
                    << Column("position", Type("smallint", 3).NotNull())
                    << Column("item_id", Type("int", 11).NotNull());
                Add(new CreateTable(player_equipment.operator Table()));
            }
        }  // namespace migrations
    }      // namespace mysql
}  // namespace core
