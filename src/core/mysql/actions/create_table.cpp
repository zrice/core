#include "create_table.hpp"

#include <algorithm>
#include <sstream>
#include <utility>

#include "../../logger.hpp"

namespace core::mysql::actions {
    CreateTable::CreateTable(const structures::Table &table) : _table(table) {}

    bool IsPrimaryKey(const structures::Column &column) {
        return column.GetType().IsPrimaryKey();
    }

    std::string CreateTable::ToSQL() {
        std::stringstream sql;

        sql << "CREATE TABLE `" << _table.GetDatabase() << "`.`"
            << _table.GetName() << "` (";

        auto columns = _table.GetColumns();

        auto first = true;
        for (auto &column : columns) {
            if (first) {
                first = false;
            } else {
                sql << ", ";
            }

            auto type = column.GetType();

            sql << "`" << column.GetName() << "` " << type.GetType();
            if (type.GetLength() > 0) {
                sql << "(" << type.GetLength() << ") ";
            } else {
                sql << " ";
            }

            if (!type.IsNullable()) {
                sql << "NOT NULL ";
            }

            if (type.IsAutoIncrement()) {
                sql << "AUTO_INCREMENT ";
            }

            if (column.GetDefaultValue() != "") {
                sql << "DEFAULT " << column.GetDefaultValue();
            }
        }

        std::stringstream primaryKeys;
        auto it = std::find_if(columns.begin(), columns.end(), IsPrimaryKey);
        first = true;
        while (it != columns.end()) {
            if (first) {
                first = false;
            } else {
                primaryKeys << ", ";
            }

            primaryKeys << "`" << it->GetName() << "`";

            it = std::find_if(++it, columns.end(), IsPrimaryKey);
        }

        if (!first) {
            sql << ", PRIMARY KEY(" << primaryKeys.str() << ")";
        }

        sql << ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;";  // todo: consider
                                                            // allow to change
                                                            // default charset

        return sql.str();
    }
}  // namespace core::mysql::actions