#pragma once

#include "../structures/table.hpp"
#include "action.hpp"

namespace core::mysql::actions {
    class CreateTable : public Action {
       public:
        explicit CreateTable(const structures::Table &table);

        std::string ToSQL() override;

       private:
        structures::Table _table;
    };
}  // namespace core::mysql::actions