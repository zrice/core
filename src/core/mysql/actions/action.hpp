#pragma once

#include <string>

namespace core::mysql::actions {
    class Action {
       public:
        virtual ~Action() = default;

        virtual std::string ToSQL() = 0;
    };
}  // namespace core::mysql::actions
