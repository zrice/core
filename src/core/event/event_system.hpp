#pragma once

#include <map>
#include <string>
#include <vector>

#define EVENT(name, ...) core::event::EventSystem::CallHook(name, __VA_ARGS__)

namespace core::event {
    class EventSystem {
       public:
        static void AddHook(std::string name, void *function);

        template <typename... Args>
        static void CallHook(std::string name, Args... agrs) {
            if (_events.find(name) == _events.end()) return;

            typedef void (*func)(Args...);
            auto events = _events[name];

            for (auto event : events) {
                auto evt = (func)event;
                evt(args...);
            }
        }

       private:
        static std::map<std::string, std::vector<void *>> _events;
    };
}  // namespace core::event