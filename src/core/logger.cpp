#include "logger.hpp"

namespace core {
    std::string path_to_filename(std::string path) {
        return path.substr(path.find_last_of("/\\") + 1);
        // return path.substr(0, path.find_last_of("."));
    }
}  // namespace core
