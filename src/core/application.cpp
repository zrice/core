#include "application.hpp"

#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/log/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/sinks/text_file_backend.hpp>
#include <boost/log/sources/record_ostream.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <csignal>
#include <cstdlib>

#include "mysql/migration_manager.hpp"
#include "profiler.hpp"

namespace logging = boost::log;
namespace keywords = boost::log::keywords;
namespace sinks = boost::log::sinks;
namespace pt = boost::property_tree;
namespace expr = boost::log::expressions;

namespace core {
    Application::Application() : _context(), _networkThread{} {
        START_SESSION("Startup", "profiling_startup.json");
        _bootTime = boost::posix_time::microsec_clock::local_time();

        CORE_LOGGING(info) << "Initializing core";

        InitializeLogging();
        LoadConfiguration();
        InitializeDatabase();
        InitializeCache();
        InitializeScripting();
    }

    void Application::InitializeLogging() {
        PROFILE_FUNCTION();
        CORE_LOGGING(debug) << "Initialize logging";

        // Register severity formatting variable
        logging::register_simple_formatter_factory<
            logging::trivial::severity_level, char>("Severity");

        // Register common used attributes for formatting
        logging::add_common_attributes();

        // Setup sinks
        auto fileSink = logging::add_file_log(
            keywords::file_name = "server_%Y%m%d.%N.log",
            keywords::rotation_size = 10 * 1024 * 1024,  // rotate after 10mb
            keywords::time_based_rotation =
                sinks::file::rotation_at_time_point(0, 0, 0)  // or at midnight
        );
        fileSink->set_formatter(
            expr::stream << "["
                         << expr::format_date_time<boost::posix_time::ptime>(
                                "TimeStamp", "%Y-%m-%d %H:%M:%S")
                         << "] " << std::setw(30)
                         << expr::attr<std::string>("File") << ":" << std::left
                         << std::setw(4) << expr::attr<int>("Line")
                         << std::right << " [" << std::setw(7)
                         << logging::trivial::severity << "] "
                         << expr::smessage);

        auto consoleSink = logging::add_console_log(
            std::cout, logging::keywords::auto_flush = true);
        consoleSink->set_formatter(
            expr::stream << "["
                         << expr::format_date_time<boost::posix_time::ptime>(
                                "TimeStamp", "%Y-%m-%d %H:%M:%S")
                         << "] " << std::setw(30)
                         << expr::attr<std::string>("File") << ":" << std::left
                         << std::setw(4) << expr::attr<int>("Line")
                         << std::right << " [" << std::setw(7)
                         << logging::trivial::severity << "] "
                         << expr::smessage);

        // Apply severity filter
#ifndef NDEBUG
        logging::core::get()->set_filter(logging::trivial::severity >=
                                         logging::trivial::trace);
#else
        logging::core::get()->set_filter(logging::trivial::severity >=
                                         logging::trivial::info);
#endif

        CORE_LOGGING(debug) << "Logging initialized";
    }

    bool Application::VerifyConfiguration() {
        if (_configuration.get<unsigned short>("port", 0) == 0) {
            CORE_LOGGING(trace) << "Missing port configuration";
            return false;
        }

        return true;
    }

    void Application::LoadConfiguration() {
        PROFILE_FUNCTION();
        CORE_LOGGING(info) << "Loading configuration";

        try {
            pt::read_json("config.json", _configuration);
            CORE_LOGGING(info) << "Configuration loaded";

            if (!VerifyConfiguration()) {
                std::exit(EXIT_FAILURE);
            }

            _type = _configuration.get<std::string>("type", "");
            CORE_LOGGING(trace) << "Type = " << _type;
        } catch (const pt::json_parser::json_parser_error &e) {
            CORE_LOGGING(fatal) << "Failed to load configuration";
            CORE_LOGGING(fatal)
                << e.filename() << ":" << e.line() << " " << e.message();

            std::exit(EXIT_FAILURE);
        }
    }

    void Application::InitializeDatabase() {
        PROFILE_FUNCTION();
        auto ip = _configuration.get<std::string>("database.ip", "127.0.0.1");
        auto port = _configuration.get<unsigned int>("database.port", 3306);
        auto username =
            _configuration.get<std::string>("database.user", "root");
        auto password =
            _configuration.get<std::string>("database.password", "");

        _mysql = std::make_shared<mysql::MySQL>(ip, username, password, port);
    }

    void Application::InitializeCache() {
        PROFILE_FUNCTION();
        CORE_LOGGING(info) << "Connecting to redis...";

        // Creating redis connection
        boost::asio::ip::tcp::endpoint redisEndpoint(
            boost::asio::ip::address::from_string(
                _configuration.get<std::string>("redis.ip", "127.0.0.1")),
            _configuration.get<unsigned short>("redis.port", 6379));

        boost::asio::ip::tcp::socket socket(_context, redisEndpoint.protocol());
        try {
            socket.connect(redisEndpoint);
        } catch (...) {
            CORE_LOGGING(error) << "Failed to connect to redis";
            exit(1);
            return;
        }

        auto connection =
            std::make_shared<bredis::Connection<boost::asio::ip::tcp::socket>>(
                std::move(socket));
        _redis = std::make_shared<cache::Redis>(connection);
    }

    void Application::InitializeScripting() {
        PROFILE_FUNCTION();
        _scripting = std::make_shared<scripting::Scripting>();
    }

    volatile sig_atomic_t stop;

    void signal_handler(int signal) {
        CORE_LOGGING(trace) << "Signal handled";
        stop = signal;
    }

    void Application::Start(std::shared_ptr<ApplicationAbstract> application) {
        _application = std::move(application);

        if (_application) {
            CORE_LOGGING(trace) << "Starting application";
            _application->Start();
        }

        _scripting->LoadPlugins();

        END_SESSION();

        START_SESSION("Update", "profiling_update.json");

        std::signal(SIGINT, signal_handler);
        std::signal(SIGTERM, signal_handler);

        StartNetworkThread();
        UpdateLoop();

        END_SESSION();
    }

    const uint32_t UPDATE_TICK_RATE = 10;  // every 10ms one update call

    void Application::StartNetworkThread() {
        _networkThread = std::thread{[this]() { _context.run(); }};
    }

    void Application::UpdateLoop() {
        auto lastTime = GetCoreTime();
        uint32_t accumulator = 0;

        while (!stop) {
            {
                PROFILE_SCOPE("Tick");
                auto current = GetCoreTime();
                auto elapsedTime = current - lastTime;
                lastTime = current;

                accumulator += elapsedTime;
                while (accumulator >= UPDATE_TICK_RATE) {
                    _application->Update(UPDATE_TICK_RATE);
                    accumulator -= UPDATE_TICK_RATE;
                }
            }

            std::this_thread::sleep_for(
                std::chrono::milliseconds(UPDATE_TICK_RATE / 2));
        }

        // Request asio to stop the event processing loop
        _context.stop();
        // Wait for network thread to end
        _networkThread.join();
    }

    const boost::property_tree::ptree &Application::GetConfiguration() const {
        return _configuration;
    }

    std::shared_ptr<mysql::MySQL> Application::GetMySQL() const {
        return _mysql;
    }

    std::shared_ptr<cache::Redis> Application::GetRedis() const {
        return _redis;
    }

    boost::asio::io_context &Application::GetContext() { return _context; }

    uint32_t Application::GetCoreTime() {
        auto now = boost::posix_time::microsec_clock::local_time();
        auto diff = now - _bootTime;

        return diff.total_milliseconds();
    }

    const std::string &Application::GetType() { return _type; }

    std::shared_ptr<scripting::ScriptingEngineAbstract>
    Application::GetScriptEngine(const std::string &name) {
        return _scripting->GetScriptEngine(name);
    }

}  // namespace core
