#pragma once

#include <cassert>

namespace core::utils {
    template <typename T>
    class Grid {
       public:
        Grid(uint64_t width, uint64_t height) : _width(width), _height(height) {
            _data = new T[width * height];
        }

        virtual ~Grid() { delete[] _data; }

        uint64_t GetWidth() { return _width; }
        uint64_t GetHeight() { return _height; }

        /// Resize the grid and clear all data
        void Resize(uint64_t width, uint64_t height) {
            delete[] _data;
            _data = new T[width * height];
            _width = width;
            _height = height;
        }

        T Get(uint64_t x, uint64_t y) {
            assert(x < _width);
            assert(y < _height);

            return _data[y * _width + x];
        }

        void Set(uint64_t x, uint64_t y, T value) {
            assert(x < _width);
            assert(y < _height);

            _data[y * _width + x] = value;
        }

       private:
        T *_data;
        uint64_t _width;
        uint64_t _height;
    };
}  // namespace core::utils