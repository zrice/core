#pragma once

#include <cmath>

namespace core::utils {
    class Math {
       public:
        static float Distance(float x1, float y1, float x2, float y2) {
            auto a = x1 - x2;
            auto b = y1 - y2;

            return sqrt(a * a + b * b);
        }
    };
}  // namespace core::utils