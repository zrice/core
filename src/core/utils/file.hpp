#pragma once

#include <algorithm>
#include <fstream>
#include <iterator>
#include <string>
#include <vector>

namespace core::utils {
    std::vector<uint8_t> ReadFile(const std::string &file) {
        std::ifstream ifstream(file, std::ios::in | std::ios::binary);
        return std::vector<uint8_t>(std::istreambuf_iterator<char>{ifstream},
                                    {});
    }
}  // namespace core::utils