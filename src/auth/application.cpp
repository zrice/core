#include "application.hpp"

#include <libbcrypt.h>
#include <utility>

#include "../core/mysql/migrations/_defaults.hpp"
#include "../core/networking/reserved_headers.hpp"
#include "headers.hpp"
#include "packets/general.hpp"

namespace auth {
    Application::Application(
        std::shared_ptr<core::Application> core_application)
        : _coreApplication(std::move(core_application)) {
        _playerCache = std::make_shared<core::cache::PlayerCache>(
            _coreApplication->GetRedis(), _coreApplication->GetMySQL());
    }

    void Application::Start() {
        auto conf = _coreApplication->GetConfiguration();

        // Create server
        try {
            _server = std::make_shared<core::networking::Server>(
                _coreApplication, conf.get<std::string>("host", "*"),
                conf.get<unsigned short>("port"),
                conf.get<uint8_t>(
                    "securityLevel",
                    core::networking::SECURITY_LEVEL_KEY_AGREEMENT),
                reinterpret_cast<const uint32_t *>(
                    conf.get<std::string>("pong", "testtesttesttest").c_str()));
        } catch (const boost::system::system_error &err) {
            CORE_LOGGING(fatal) << "Failed to create server " << err.what();
            return;
        }

        // Register packets
        RegisterPackets();

        CORE_LOGGING(info) << "Starting auth server";
        _server->Run();
    }

    void Application::Update(uint32_t elapsedTime) {}

    void Application::RegisterPackets() {
        auto pm = _server->GetPacketManager();

        packets::RegisterGeneral(pm);

        _server->RegisterHandler(
            HeaderIncomingPackets::HEADER_INC_LOGIN,
            std::bind(&Application::HandleLogin, this, std::placeholders::_1,
                      std::placeholders::_2));

        _server->SetNewConnectionHandler(std::bind(
            &Application::OnNewConnection, this, std::placeholders::_1));
    }

    void Application::OnNewConnection(
        std::shared_ptr<core::networking::Connection> connection) {
        connection->SetPhase(
            core::networking::Phases::PHASE_AUTH);  // todo: move to handler
                                                    // done handshake
    }

    void Application::HandleLogin(
        std::shared_ptr<core::networking::Connection> connection,
        std::shared_ptr<core::networking::Packet> packet) {
        auto username = packet->GetString("username");
        auto password = packet->GetString("password");

        auto stmt = _coreApplication->GetMySQL()->CreateStatement(
            "SELECT * FROM `" ACCOUNT_DATABASE
            "`.`accounts` WHERE `username`=%1%");
        stmt << username;

        auto res = stmt();

        bool status = false;
        std::string err = "UNKNOWN";
        uint32_t accountId;

        if (!res->Next()) {
            // invalid username
            // hash username with bcrypt to prevent time attacks
            bcrypt_checkpw(
                password.c_str(),
                "$2y$12$u4XpE.xw9MiF5LF.fzO9zucwdIlIcE5ETn85JGnUCyTXB4TTyxL6O");
            err = "WRONGPWD";
        } else {
            // user found
            auto hash = res->GetString("password");
            accountId = res->GetUnsigned32("id");
            status = bcrypt_checkpw(password.c_str(), hash.c_str()) == 0;
            if (!status) {
                err = "WRONGPWD";
            } else {
                err = res->GetString("status");
                if (err != "OK") status = false;
            }
        }

        CORE_LOGGING(trace)
            << "Auth request: Status : " << status << " ErrorCode: " << err;

        if (!status) {
            // login failed
            auto response = _server->GetPacketManager()->CreatePacket(
                HeaderOutgoingPackets::HEADER_OUT_LOGIN_FAILED,
                core::networking::Outgoing);
            response->SetString("status", err);
            connection->Send(response);
        } else {
            // Request login key from cache
            auto loginKey = _playerCache->GenerateLoginKey(accountId);
            _playerCache->SetLoginKeyTTL(loginKey);
            auto response = _server->GetPacketManager()->CreatePacket(
                HeaderOutgoingPackets::HEADER_OUT_LOGIN_SUCCESS,
                core::networking::Outgoing);
            response->SetField<uint32_t>("key", loginKey);
            response->SetField<uint8_t>("result", true);
            connection->Send(response);
        }
    }

}  // namespace auth
