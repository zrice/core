#pragma once

#include <memory>

#include "../core/application.hpp"
#include "../core/application_abstract.hpp"
#include "../core/cache/player_cache.hpp"
#include "../core/networking/client.hpp"
#include "../core/networking/server.hpp"

namespace auth {
    class Application : public core::ApplicationAbstract,
                        public std::enable_shared_from_this<Application> {
       public:
        Application(std::shared_ptr<core::Application> core_application);
        void Start() override;
        void Update(uint32_t elapsedTime) override;

       private:
        void RegisterPackets();

        void HandleLogin(
            std::shared_ptr<core::networking::Connection> connection,
            std::shared_ptr<core::networking::Packet> packet);
        void OnNewConnection(
            std::shared_ptr<core::networking::Connection> connection);

       private:
        std::shared_ptr<core::Application> _coreApplication;
        std::shared_ptr<core::networking::Server> _server;

        std::shared_ptr<core::cache::PlayerCache> _playerCache;
    };
}  // namespace auth
