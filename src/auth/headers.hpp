#pragma once

namespace auth {
    enum HeaderIncomingPackets : uint8_t {
        HEADER_INC_LOGIN = 0x6f,
    };

    enum HeaderOutgoingPackets : uint8_t {
        HEADER_OUT_LOGIN_FAILED = 0x07,
        HEADER_OUT_LOGIN_SUCCESS = 0x96
    };
}  // namespace auth
