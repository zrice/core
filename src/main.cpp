#include <iostream>
#include <memory>

#include "auth/application.hpp"
#include "core/application.hpp"
#include "game/application.hpp"

#ifndef SPLIT_CORES
int main() {
    auto core = std::make_shared<core::Application>();
    std::shared_ptr<core::ApplicationAbstract> app;

    if (core->GetType() == "auth") {
        app = std::make_shared<auth::Application>(core);
    } else if (core->GetType() == "game") {
        app = std::make_shared<game::Application>(core);
    } else {
        CORE_LOGGING(fatal) << "Unknown core type '" << core->GetType() << "'";
        CORE_LOGGING(fatal) << "Please use one of the following types: "
                            << "'auth', 'game'";
        return 1;
    }

    core->Start(app);

#ifdef _DEBUG
    // Wait for key to exit process when debug executable is compiled
    std::cout << "Press any key to exit the application." << std::endl;
    getchar();
#endif

    return 0;
}
#endif