# Build the project with enabled tests
FROM registry.gitlab.com/quantum-core/images/debian:0.9
COPY . /build/
RUN mkdir /build/build && \
    cd /build/build && \
    cmake .. -DCMAKE_BUILD_TYPE=RELEASE -DBUILD_TESTS=ON -DVCPKG_TARGET_TRIPLET=x64-linux -DCMAKE_TOOLCHAIN_FILE=/opt/vcpkg/scripts/buildsystems/vcpkg.cmake && \
    make compile_packets && \
    cmake .. && \
    make -j4

# Execute tests in a new container
FROM debian:10-slim
COPY --from=0 /build/build/m2server_tests /usr/local/bin/m2server_tests
RUN /usr/local/bin/m2server_tests

# Build final image only containing the server excecutable
FROM debian:10-slim
COPY --from=0 /build/build/m2server /usr/local/bin/m2server
ENTRYPOINT [ "/usr/local/bin/m2server" ]
