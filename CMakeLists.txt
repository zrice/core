cmake_minimum_required(VERSION 3.2)
project(m2server)
set(CMAKE_CXX_STANDARD 17)

option(BUILD_TESTS "Should tests be compiled or not" OFF)
option(SPLIT_CORES "Should a seperate build target be generated for every application" OFF)
option(PROFILING "Enable embedded profiling capabilites" OFF)

# Add additional cmake modules
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/modules/")

# Look for header and source files in src/
file(GLOB_RECURSE CORE_SOURCE_FILES "src/core/*.cpp")
file(GLOB_RECURSE CORE_HEADER_FILES "src/core/*.hpp")
file(GLOB_RECURSE AUTH_SOURCE_FILES "src/auth/*.cpp")
file(GLOB_RECURSE AUTH_HEADER_FILES "src/auth/*.hpp")
file(GLOB_RECURSE GAME_SOURCE_FILES "src/game/*.cpp")
file(GLOB_RECURSE GAME_HEADER_FILES "src/game/*.hpp")
file(GLOB_RECURSE TEST_FILES "tests/*.cpp")

# Remove main.cpp files
list(FILTER AUTH_SOURCE_FILES EXCLUDE REGEX "src/auth/main.cpp$")
list(FILTER GAME_SOURCE_FILES EXCLUDE REGEX "src/game/main.cpp$")

add_library(core_lib STATIC ${CORE_SOURCE_FILES} ${CORE_HEADER_FILES})
add_library(auth_lib STATIC ${AUTH_SOURCE_FILES} ${AUTH_HEADER_FILES})
add_library(game_lib STATIC ${GAME_SOURCE_FILES} ${GAME_HEADER_FILES})

target_link_libraries(auth_lib core_lib)
target_link_libraries(game_lib core_lib)

# Add target to compile packets into source files
file(GLOB_RECURSE PACKET_FILES "src/*.packets")
add_custom_target(compile_packets COMMAND pc ${PACKET_FILES} WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})

if(SPLIT_CORES)
	add_definitions(-DSPLIT_CORES)

	add_executable(auth src/auth/main.cpp)
	target_link_libraries(auth auth_lib)

	add_executable(game src/game/main.cpp)
	target_link_libraries(game game_lib)
else()
	# Create executable and add our global entry point to it
	add_executable(m2server src/main.cpp)
	# Finally link it to our core library
	target_link_libraries(m2server auth_lib game_lib)
endif()

if(PROFILING)
	add_definitions(-DPROFILING)
endif()


if(MSVC)
	add_definitions(-D_WIN32_WINNT=0x0600)
endif()

# Search for boost
find_package(Boost REQUIRED COMPONENTS log system filesystem)
target_link_libraries(core_lib PRIVATE ${Boost_LIBRARIES})
include_directories(${Boost_INCLUDE_DIRS})
#add_definitions(-DBOOST_ASIO_ENABLE_HANDLER_TRACKING)

# If the system is unix we need pthread
if(UNIX)
	target_link_libraries(core_lib PRIVATE pthread)
endif(UNIX)

# Search for angelscript
find_package(AngelScript CONFIG)
if(AngelScript_FOUND)
	message(STATUS "AngelScript found, enable scripting")
	target_link_libraries(core_lib PRIVATE Angelscript::angelscript)
	add_definitions(-DENABLE_ANGELSCRIPT)
else()
	message(STATUS "AngelScript not found")
endif()	

# Search for mariadb
find_package(MariaDBClient REQUIRED)
if(MariaDBClient_FOUND)
	include_directories(${MariaDBClient_INCLUDE_DIRS})
	target_link_libraries(core_lib PUBLIC ${MariaDBClient_LIBRARIES})

	if(MSVC)
		target_link_libraries(core_lib PUBLIC shlwapi)
	endif()
endif()

if(UNIX)
	# MariaDB required OpenSSL on unix and libdl
	find_package(Iconv REQUIRED)
	target_link_libraries(core_lib PRIVATE ${Iconv_LIBRARY} dl)
endif(UNIX)

# Search for crypto++
find_package(CryptoPP REQUIRED)
if (CryptoPP_FOUND)
	target_link_libraries(core_lib PUBLIC cryptopp-static)
endif()

# Find LZO library
find_path(LZO_INCLUDE_DIR lzo/lzo1.h)
find_library(LZO_LIBRARY lzo2)
include_directories(${LZO_INCLUDE_DIR})
link_libraries(${LZO_LIBRARY})

# Add bcrypt library
add_subdirectory(lib)
target_link_libraries(core_lib PUBLIC libbcrypt)
target_link_libraries(core_lib PUBLIC bredis)

if(BUILD_TESTS)
	# Enable unit testing
	enable_testing()
	find_package(GTest CONFIG REQUIRED)
	add_executable(m2server_tests ${TEST_FILES})
	target_link_libraries(m2server_tests PRIVATE GTest::gtest GTest::gtest_main core_lib auth_lib game_lib)
	target_include_directories(m2server_tests PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/src)
	add_test(AllTests m2server_tests)
endif()

if(SPLIT_CORES)
	install(TARGETS game auth RUNTIME DESTINATION bin)
else()
	install(TARGETS m2server RUNTIME DESTINATION bin)
endif()